unit uFUserLogin;
interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit,  DB,
   Vcl.Menus, cxMaskEdit, cxDropDownEdit,
  dxGDIPlusClasses, cxImage, cxTextEdit, Vcl.StdCtrls, cxButtons, cxLabel

  {dxSkinsCore,           dxSkinsDefaultPainters,
  dxSkinBasic, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinOffice2019Black, dxSkinOffice2019Colorful,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinValentine, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue
  };
type
  TFUserLogin = class(TForm)
    cxLabel1: TcxLabel;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    edPassword: TcxTextEdit;
    cxImage1: TcxImage;
    cxLabel2: TcxLabel;
    edUserName: TcxTextEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure edUserNamePropertiesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    TryCount: integer;
    OldIdleEnabledValue: Boolean;
  public
    { Public declarations }
    MaxTryCount: integer;
    UserName: string;
    UserID: integer;
  end;
var
  FUserLogin: TFUserLogin;
implementation
uses uSetup, uMainForm;
{$R *.dfm}

procedure TFUserLogin.btnOKClick(Sender: TObject);
var
  Idx: integer;
begin
  Inc (TryCount);
  Idx := MainForm.UserList.IndexOf(edUserName.Text);
  if Idx = -1 then
  begin
    MessageDlg ('Invalid user name', mtInformation, [mbOK], 0);
    exit;
  end;
  if edPassword.Text = MainForm.UserList.Items [Idx].Password then
  begin
    UserID      := MainForm.UserList.Items [Idx].UserID;
    UserName    := MainForm.UserList.Items [Idx].UserName;
    ModalResult := mrOk
  end
  else if TryCount <= MaxTryCount then
  begin
    MessageDlg('Invalid password', mtInformation, [mbOK], 0);
    edPassword.SetFocus;
  end
  else
  begin
    MessageDlg('Invalid password. You have exceeded the number of attempts', mtError, [mbOK], 0);
    ModalResult := mrCancel;
  end;
end;
procedure TFUserLogin.edUserNamePropertiesChange(Sender: TObject);
begin
  btnOK.Enabled := (edUserName.Text <> '') and (edPassword.Text <> '')
end;
procedure TFUserLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MainForm.IdleTimer.Enabled := OldIdleEnabledValue;
end;

procedure TFUserLogin.FormCreate(Sender: TObject);
begin
  MaxTryCount := 3;
end;
procedure TFUserLogin.FormShow(Sender: TObject);
begin
  OldIdleEnabledValue := MainForm.IdleTimer.Enabled;
  MainForm.IdleTimer.Enabled := false;
  TryCount        := 0;
  edUserName.Text := '';
  edPassword.Text := '';
  UserName        := '';
  UserID          := -1;
  btnOK.Enabled   := False;
  edUserName.SetFocus;
end;
end.
