unit uProcesses;

interface
uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, WinApi.ShellApi,
  Vcl.ComCtrls;

type
  TWinList = class (TStringList)
  public
    function GetWinListByThread (ThreadID: Cardinal): boolean;
  end;

function ProcessStillActive (proc_id: Cardinal): boolean;
procedure LaunchApp (AppName, Params: string; ParentHandle: cardinal; WaitMS: integer; HideFrames: boolean; var Proc_ID, Thread_ID: cardinal; var AppWnd: DWord);
procedure ResizeApp (AppWnd: Cardinal; Width, Height: integer);
procedure KillWindow (var AppWnd: cardinal);
function GetWindowClass(const nHandle: HWnd): string;
function GetFocusedWindowFromParent(ParentWnd:HWnd):HWnd;
function GetWindowTitle (Wnd: Cardinal): string;
function IsWindow (Wnd: Cardinal): boolean;
function GetZOrder(Wnd: HWND): Integer;

implementation

var
  WindowList: TList;

function GetWindow (Handle: Cardinal; LParam: longint): bool; stdcall;
begin
  result := true;
  WindowList.Add (Pointer (Handle));
end;

function GetHandles(ThreadID: Cardinal): Cardinal;
var
  i:    integer;
  hnd:  Cardinal;
  cpid: DWord;
begin
  result     := 0;
  WindowList := TList.Create;
  EnumWindows (@GetWindow, 0);
  for i := 0 to WindowList.Count - 1 do
  begin
    hnd := HWND (WindowList [i]);
    GetWindowThreadProcessID (hnd, @cpid);
    if ThreadID = CPID then
    begin
        Result := hnd;
        Exit;
    end;
  end;
  WindowList.Free;
end;

function ExecApplication(AppName, Params: string; WaitMS: integer; var proc_id, thread_id: Cardinal): Cardinal;
var
  StartInfo:  TStartupInfo;
  ProcInfo:   TProcessInformation;
  process_id: Cardinal;
  CmdLine:    string;
  ms, n:      Cardinal;
  ProcessHandle: THandle;
begin
  FillChar(StartInfo, SizeOf(StartInfo), 0);
  CmdLine               := '"' + AppName + '" ' + Params;
  StartInfo.cb          := SizeOf(StartInfo);
  StartInfo.dwFlags     := STARTF_USESHOWWINDOW;
  StartInfo.wShowWindow := SW_NORMAL; // SW_HIDE; // SW_SHOWMINIMIZED; //  SW_HIDE;

  if AppName <> '' then
    CreateProcess(nil, PChar(CmdLine), nil, nil, False, 0, nil, nil, StartInfo, ProcInfo);

  Sleep (WaitMS);
  process_id := ProcInfo.dwProcessId;
  proc_id    := ProcInfo.hProcess;
  thread_id  := ProcInfo.dwThreadId;
  ms         := 50;
  n          := 0;
  repeat
    Sleep (ms);
    Inc (n);
    result := GetHandles(process_id);
  until (result <> 0) and (IsWindow (result) or ((ms * n) >= WaitMS));

  ProcessHandle := OpenProcess(PROCESS_ALL_ACCESS, False, Process_id);
  if ProcessHandle > 0 then
  try
    ShowWindow (ProcessHandle, SW_HIDE);
  finally
    CloseHandle(ProcessHandle);
  end;

  // CloseHandle (ProcInfo.hProcess);
  CloseHandle (ProcInfo.hThread);
end;


function ProcessStillActive (proc_id: Cardinal): boolean;
var
  StatusCode: Cardinal;
begin
  result := False;
  if (proc_id > 0) and GetExitCodeProcess (proc_id, StatusCode) then
    result := StatusCode = STILL_ACTIVE;
end;

procedure LaunchApp (AppName, Params: string; ParentHandle: cardinal; WaitMS: integer; HideFrames: boolean; var Proc_ID, Thread_ID: cardinal; var AppWnd: DWord);
var
  CurrStyle: NativeInt;
begin
  AppWnd := ExecApplication (AppName, Params, WaitMS, Proc_ID, Thread_ID);
  if AppWnd <> 0 then
  begin
    WinApi.Windows.SetParent(AppWnd, ParentHandle);

    if HideFrames then
    begin
      //Get current style
      CurrStyle := WinApi.Windows.GetWindowLong(AppWnd, GWL_STYLE);

      //Remove title bar, system menu, maximize and minimize buttons
      CurrStyle := CurrStyle and not WS_CAPTION
                             and not WS_SYSMENU
                             and not WS_THICKFRAME
                             and not WS_MINIMIZE
                             and not WS_MAXIMIZEBOX;

      //Apply new style
      WinApi.Windows.SetWindowLong(AppWnd, GWL_Style, CurrStyle);
    end;

    //Get current app style
    CurrStyle := WinApi.Windows.GetWindowLong(AppWnd, GWL_EXSTYLE);

    //Remove from task bar
    CurrStyle := CurrStyle or WS_EX_TOOLWINDOW;

    //Apply new application style
    WinApi.Windows.SetWindowLong(AppWnd, GWL_EXSTYLE, CurrStyle);

    ShowWindow(AppWnd, SW_SHOWMAXIMIZED);
    ShowWindow(AppWnd, SW_SHOWMAXIMIZED);
  end;
end;

procedure ResizeApp (AppWnd: Cardinal; Width, Height: integer);
begin
  if IsWindow(AppWnd) then
    SetWindowPos(AppWnd, 0, 0, 0, Width, Height, SWP_ASYNCWINDOWPOS);
end;

procedure KillWindow (var AppWnd: cardinal);
begin
  if (AppWnd <> 0) then
  begin
    PostMessage(AppWnd, WM_Close, 0, 0);
    AppWnd := 0;
  end;
end;

function GetFocusedWindowFromParent(ParentWnd:HWnd):HWnd;
var
  OtherThread, Buffer: DWord;
  idCurrThread: DWord;
begin
  OtherThread  := GetWindowThreadProcessID(ParentWnd, @Buffer);
  idCurrThread := GetCurrentThreadID;
  if AttachThreadInput(idCurrThread, OtherThread, true) then
  begin
    result := GetFocus;
    AttachThreadInput(idCurrThread, OtherThread, false);
  end
  else
    result := GetFocus;
end;

function GetWindowClass(const nHandle: HWnd): string;
var
  szClassName: array[0..255] of char;
begin
  GetClassName (nHandle, szClassName, 255);
  result := szClassName;
end;

function GetWindowTitle (Wnd: Cardinal): string;
var
  Len: integer;
begin
  result := '';
  Len    := GetWindowTextLength(Wnd);

  if Len > 0 then
  begin
    SetLength (result, Len);
    GetWindowText(Wnd, PChar (result), Length (result) + 1);
  end;
end;

function IsWindow (Wnd: Cardinal): boolean;
var
  WinInfo: TWindowInfo;

  function HasdwStyle (Style: Cardinal): boolean;
  begin
    result := (WinInfo.dwStyle and Style) = Style;
  end;
begin
  result := False;
  FillChar(WinInfo, Sizeof(WinInfo), 0);
  WinInfo.cbSize := Sizeof(WinInfo);
  GetWindowInfo(Wnd, WinInfo);

  result := HasdwStyle(WS_CAPTION) or HasdwStyle(WS_SYSMENU) or HasdwStyle(WS_POPUPWINDOW) or HasdwStyle(WS_POPUP) or
            HasdwStyle(WS_MINIMIZEBOX) or HasdwStyle(WS_SIZEBOX) or HasdwStyle(WS_TILEDWINDOW) or
            HasdwStyle(WS_SIZEBOX) or HasdwStyle(WS_BORDER);
end;

function GetZOrder(Wnd: HWND): Integer;
var
  h: HWND;
begin
  Result := 0;
  h := Wnd;
  while h <> 0 do
  begin
    Inc(Result);
    h := Winapi.Windows.GetWindow(h, GW_HWNDPREV);
  end;
end;

procedure myEnumWindowCallback(hwnd:HWND; lParam: TWinList); stdcall;
var
  s: string;
begin
  if not IsWindow (hwnd) or not IsWindowVisible(hwnd) then Exit;

  lParam.AddObject(IntToHex (hwnd, 8), Pointer (hwnd));
end;

function TWinList.GetWinListByThread (ThreadID: Cardinal): boolean;
begin
  Clear;

  result := EnumThreadWindows(ThreadID, @myEnumWindowCallback, Integer(Self));
end;

end.
