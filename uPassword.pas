unit uPassword;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
 {$IFDEF UseSkins}
  dxSkinsCore,
  dxSkinBasic, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinOffice2019Black, dxSkinOffice2019Colorful,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue,
  {$ENDIF}
  cxTextEdit, cxLabel, Vcl.ExtCtrls;

type
  TFramePassword = class(TFrame)
    labPreviousPassword: TcxLabel;
    edPreviousPassword: TcxTextEdit;
    cxLabel3: TcxLabel;
    edPasswordValidation: TcxTextEdit;
    labErrorMsg: TcxLabel;
    labPassword: TcxLabel;
    edPassword: TcxTextEdit;
    pnlOldPassword: TPanel;
    Panel2: TPanel;
    lblIncorrectPassword: TcxLabel;
    procedure edPasswordValidationKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    function Validate(aOldPassword: String; var aNewPassword: String): Boolean;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.dfm}
{ TFrame1 }

constructor TFramePassword.Create(AOwner: TComponent);
begin
  inherited;
  labErrorMsg.Visible := false;
  lblIncorrectPassword.Visible := false;
end;

procedure TFramePassword.edPasswordValidationKeyPress(Sender: TObject; var Key: Char);
begin
  labErrorMsg.Visible := false;
  lblIncorrectPassword.Visible := false;
end;

function TFramePassword.Validate(aOldPassword: String;
  var aNewPassword: String): Boolean;
begin
  Result := false;
  if edPassword.Text <> edPasswordValidation.Text then
  begin
    labErrorMsg.Visible := true;
    exit;
  end;
  if edPreviousPassword.Text <> aOldPassword then
  begin
    lblIncorrectPassword.Visible := true;
    exit;
  end;

  aNewPassword := edPassword.text;
  Result := true;
end;

end.
