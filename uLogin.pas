unit uLogin;
interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit,  cxTextEdit, cxMaskEdit, cxLabel, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, dxGDIPlusClasses, cxImage
{
  dxSkinsCore,
  dxSkinsDefaultPainters,
  dxSkinBasic, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkroom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinOffice2019Black, dxSkinOffice2019Colorful, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringtime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue
  }
  ;
type
  TFLogin = class(TForm)
    cxLabel1: TcxLabel;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    edPassword: TcxTextEdit;
    cxImage1: TcxImage;

    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure edPasswordPropertiesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    MaxTry:   integer;
    TryCount: integer;
    OldIdleEnabledValue: Boolean;
    { Private declarations }
  public
    { Public declarations }
    FAdminPassword:     string;
    FUserPassword:      string;
    FApplyUserPassword: boolean;
  end;
var
  FLogin: TFLogin;
implementation
uses uMainForm;
{$R *.dfm}
procedure TFLogin.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;
procedure TFLogin.btnOKClick(Sender: TObject);
begin
  if (FApplyUserPassword and (FUserPassword <> '') and (FUserPassword = edPassword.Text)) or
      (FAdminPassword = edPassword.Text) then
    ModalResult := mrOK
  else
  begin
    Inc (TryCount);
    if TryCount >= MaxTry then
    begin
      MessageDlg ('Number of attempts exceeded', mtError, [mbOK], 0);
      ModalResult := mrCancel;
    end
    else
    begin
      MessageDlg ('Invalid password', mtWarning, [mbOK], 0);
      edPassword.Text := '';
      edPassword.SetFocus;
    end;
  end;
end;
procedure TFLogin.edPasswordPropertiesChange(Sender: TObject);
begin
  btnOK.Enabled := edPassword.Text <> '';
end;
procedure TFLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 MainForm.IdleTimer.Enabled := OldIdleEnabledValue;
end;

procedure TFLogin.FormCreate(Sender: TObject);
begin
  FApplyUserPassword := True;
end;
procedure TFLogin.FormShow(Sender: TObject);
begin
  OldIdleEnabledValue := MainForm.IdleTimer.Enabled;
  MainForm.IdleTimer.Enabled := false;
  edPassword.Text := '';
  btnOK.Enabled   := False;
  MaxTry          := 3;
  TryCount        := 0;
end;
end.
