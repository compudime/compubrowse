unit uTypes;

interface

uses
  System.Classes, System.StrUtils;

type
  TNullableBoolean = (nbNull, nbFalse, nbTrue);
  TTabType = (ttNone, ttURL, ttProgram);

  TCommandInfo = record
    UrlID: String;
    Caption: String;
    Mode: Integer;
    exe: String;
    Params: string;
    function ToIniString: String;
    procedure FromString(aStr: String);
    function ProString: String;
//    class operator Assign(var Dest: TCommandInfo;
//      const [ref] Src: TCommandInfo);
  end;

implementation

uses
  System.SysUtils;
{ TTabCommand }

//class operator TCommandInfo.Assign(var Dest: TCommandInfo;
//  const [ref] Src: TCommandInfo);
//begin
//  try
//    Dest.UrlID := Src.UrlID;
//    Dest.Caption := Src.Caption;
//    Dest.exe := Src.exe;
//    Dest.Params := Src.Params;
//    Dest.Mode := Src.Mode;
//  except
//    Dest.UrlID := '';
//    Dest.Caption := '';
//    Dest.exe := '';
//    Dest.Params := '';
//    Dest.Mode := '';
//  end;
//end;

procedure TCommandInfo.FromString(aStr: String);
var
  Strings: tstringlist;
begin
  Strings := tstringlist.Create;
  try
    try
//      self.UrlID := tguid.NewGuid.ToString;

      Strings.Clear;
      Strings.Delimiter := '|';
      Strings.StrictDelimiter := True;
      Strings.DelimitedText := aStr;

      self.Caption := Strings[0];
      self.exe := Strings[1];
      self.Params := Strings[2];
      self.Mode := Strings[3].ToInteger;
    except
//      self.Caption := '';
//      self.exe := '';
//      self.Params := '';
//      self.Mode := '';

      // raise Exception.Create('Can read command form ini file');
    end;
  finally
    Strings.Free;
  end;
end;

function TCommandInfo.ToIniString: String;
var
  StrLst: TStringList;
begin
  StrLst := TStringList.Create;
  try
  try
  StrLst.Delimiter := '|';
  StrLst.Add(Caption);
  StrLst.Add(exe);
  StrLst.Add(Params);
  StrLst.Add(Mode.ToString);

  result := StrLst.DelimitedText;
  except
    result := '|||';
  end;
  finally
    StrLst.Free;
  end;

end;

function TCommandInfo.ProString: String;
begin
  result := format('%s %s', [exe, Params]);
end;

end.
