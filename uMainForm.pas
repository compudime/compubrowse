unit uMainForm;
// {$define Encrypt}   //remeber to un remark save to ini
{ $define Useskins }
// {$define TestForegroundWindow}

{$I cef.inc}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, System.Threading,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.StdCtrls,
  uCEFChromium, uCEFWindowParent, uCEFInterfaces, uCEFApplication, uCEFTypes,
  uCEFConstants,

  uCEFSentinel,  dxBarBuiltInMenu, TypInfo,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxPC, IniFiles,
  uLogin, Data.DB, dxmdaset, System.Actions, Vcl.ActnList, Vcl.Menus,

{$IFDEF Encrypt}
  DCPcrypt2, DCPblockciphers, DCPrijndael, DCPSha256,
{$ENDIF}
  cxButtons, System.IOUtils, uProcesses, uCEFChromiumCore, Math, System.UITypes,
  cxClasses,  cxContainer, cxEdit, dxGDIPlusClasses, cxImage,
  System.ImageList, Vcl.ImgList, System.DateUtils, uMemoryIniStructure,
  dxStatusBar, uTabParameters, ShellAPI, System.Generics.Collections,
  uFUserLogin, uSetup,
  uAddEditUser, uTypes,

 {$IFDEF UseSkins}
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinsForm,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinBasic, dxSkinOffice2019Black,
  dxSkinOffice2019Colorful, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
 {$ENDIF}

  Vcl.AppEvnts, dxSkinsCore, dxSkinBasic, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinOffice2019Black, dxSkinOffice2019Colorful, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringtime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinTheBezier,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinsForm, uResetPasswordDlg;

const
  CEFBROWSER_DESTROYWNDPARENT = WM_APP + $100;
  CEFBROWSER_DESTROYTAB = WM_APP + $101;
  CEFBROWSER_INITIALIZED = WM_APP + $102;
  CEFBROWSER_CHECKTAGGEDTABS = WM_APP + $103;
  CEFBROWSER_UPDATEDOWNLOAD = WM_APP + $104;

const
  ICO_Black = 0;
  ICO_Yellow = 1;
  ICO_Green = 2;
  ICO_Red = 3;
  ICO_Refresh = 4;

const
  AdminPassword = '243999';
  strEncryptKey = 'U8F0DFASD1394882';

type
  TDownloadState = (dsNone, dsInProgress, dsTerminated, dsCanceled);
  PDownloadInfo = ^RDownloadInfo;

  RDownloadInfo = record
    ID: Cardinal;
    PageIndex: integer;
    SuggestedName: string;
    FullPath: string;
    State: TDownloadState;
    Pct: integer;
    Btn: TcxButton;
    DropDown: TcxButton;
  end;

  ApWinInfo = ^RecWinInfo;

  RecWinInfo = record
    Handle: Cardinal;
    dwStyle: NativeInt;
    dwExStyle: NativeInt;
    Title: string;
    ClassName: string;
    ZOrder: integer;
  end;

  ApCardinal = ^Cardinal;

  TBrowserTabSheet = class(TcxTabSheet)
  private
    FKind: TTabType;
    FURLProgram: string;
    FParams: string;
    FWaitMS: integer;
    FHideFrames: boolean;
    FLoadAtStartup: boolean;
    FAutoKill: boolean;
    FAllowOpenDialog: TNullableBoolean;
    FDirectory: string;
    FTabUser: string;
    FTabPassword: string;
    FProcID: Cardinal;
    FThreadID: Cardinal;
    FAppWnd: DWord;
    FActiveWnd: DWord;
    FChromium: TChromium;
    FIsLoaded: boolean;
    FParentCEFWin: TCEFWindowParent;
    FBtnPlay: TcxButton;
    FTimer: TTimer;
    FQueue: TList;
    FWinList: TWinList;
    procedure SetKind(Value: TTabType);
    procedure SetURLProgram(Value: string);
    procedure SetParams(Value: string);
    procedure SetWaitMS(Value: integer);
    procedure SetHideFrames(Value: boolean);
    procedure SetLoadAtStartup(Value: boolean);
    procedure SetAutoKill(Value: boolean);
    procedure SetAllowOpenDialog(Value: TNullableBoolean);
    procedure SetDirectory(Value: string);
    procedure SetTabUser(Value: string);
    procedure SetTabPassword(Value: string);
    procedure SetIsLoaded(Value: boolean);
    procedure SetAppWnd(Value: DWord);
    procedure SetActiveWnd(Value: DWord);
    procedure SetProcID(Value: Cardinal);
    procedure SetThreadID(Value: Cardinal);
    procedure SetChromium(Value: TChromium);
    procedure SetParentCEFWin(Value: TCEFWindowParent);
    procedure SetBtnPlay(Value: TcxButton);
  private
    FCommandList: TList<TCommandInfo>;
    procedure ResizeApp(Width, Height: integer);
    procedure BtnPlayClick(Sender: TObject);
    procedure RunningTimer(Sender: TObject);
    procedure ClearQueue;
    function GetWindowList: TList;
    procedure UpdateQueue;
    function ReplaceParams(s: string): string;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    procedure CreateBtnPlay;
    function ProgramStillRunning: boolean;
    procedure CloseApp;
    procedure RunApp;
    procedure RelocateBtnPlay;
    property Kind: TTabType read FKind write SetKind;
    property URLProgram: string read FURLProgram write SetURLProgram;
    property Params: string read FParams write SetParams;
    property WaitMS: integer read FWaitMS write SetWaitMS;
    property HideFrames: boolean read FHideFrames write SetHideFrames;
    property LoadAtStartup: boolean read FLoadAtStartup write SetLoadAtStartup;
    property AutoKill: boolean read FAutoKill write SetAutoKill;
    property AllowOpenDialog: TNullableBoolean read FAllowOpenDialog
      write SetAllowOpenDialog;
    property CommandList: TList<TCommandInfo> read FCommandList
      write FCommandList;

    property Directory: string read FDirectory write SetDirectory;
    property TabUser: string read FTabUser write SetTabUser;
    property TabPassword: string read FTabPassword write SetTabPassword;
    property IsLoaded: boolean read FIsLoaded write SetIsLoaded;
    property ProcID: Cardinal read FProcID write SetProcID;
    property ThreadID: Cardinal read FThreadID write SetThreadID;
    property AppWnd: DWord read FAppWnd write SetAppWnd;
    property ActiveWnd: DWord read FActiveWnd write SetActiveWnd;
    property Chromium: TChromium read FChromium write SetChromium;
    property ParentCEFWin: TCEFWindowParent read FParentCEFWin
      write SetParentCEFWin;

    property BtnPlay: TcxButton read FBtnPlay write SetBtnPlay;

  end;

  TMainForm = class(TForm)
    ButtonPnl: TPanel;
    NavButtonPnl: TPanel;
    ConfigPnl: TPanel;
    URLEditPnl: TPanel;
    URLCbx: TComboBox;
    PageControlApp: TcxPageControl;
    ActionList1: TActionList;
    actSetup: TAction;
    SetupBtn: TcxButton;
    AddTabBtn: TcxButton;
    RemoveTabBtn: TcxButton;
    BackBtn: TcxButton;
    ForwardBtn: TcxButton;
    ReloadBtn: TcxButton;
    StopBtn: TcxButton;
    GoBtn: TcxButton;
    TimerValidateExpirationDate: TTimer;
    dxSkinController1: TdxSkinController;
    cxButton1: TcxButton;
    ImageList1: TImageList;
    actRefreshPage: TAction;
    popTab: TPopupMenu;
    actRefreshPage1: TMenuItem;
    actCloseApp: TAction;
    Closeapp1: TMenuItem;
    Memo1: TMemo;
    actTabProperties: TAction;
    abproperties1: TMenuItem;
    pnlDownloads: TPanel;
    btnClose: TcxButton;
    TimerUpdateDownload: TTimer;
    pmDownload: TPopupMenu;
    actDownloadOpen: TAction;
    actDownloadShowInFolder: TAction;
    actDownloadOpen1: TMenuItem;
    Showinfolder1: TMenuItem;
    actChangePassword: TAction;
    N1: TMenuItem;
    Changepassword1: TMenuItem;

    AppEvents: TApplicationEvents;
    IdleTimer: TTimer;
    procedure FormDestroy(Sender: TObject);
    procedure AddTabBtnClick(Sender: TObject);
    procedure RemoveTabBtnClick(Sender: TObject);

    procedure PageControlAppChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var canclose: boolean);
    procedure BackBtnClick(Sender: TObject);
    procedure ForwardBtnClick(Sender: TObject);
    procedure ReloadBtnClick(Sender: TObject);
    procedure StopBtnClick(Sender: TObject);
    procedure GoBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actSetupExecute(Sender: TObject);
    procedure TimerValidateExpirationDateTimer(Sender: TObject);
    procedure actRefreshPageExecute(Sender: TObject);
    procedure PageControlAppTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click
      (Sender: TObject);

    procedure actCloseAppExecute(Sender: TObject);
    procedure PageControlAppContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: boolean);
    procedure PageControlAppPageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: boolean);
    procedure actTabPropertiesExecute(Sender: TObject);
    procedure TimerUpdateDownloadTimer(Sender: TObject);
    procedure actDownloadOpenExecute(Sender: TObject);
    procedure actDownloadShowInFolderExecute(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure actChangePasswordExecute(Sender: TObject);
    procedure popTabPopup(Sender: TObject);
    procedure PopTabCommandOnClick(Sender: TObject);
    procedure AppEventsMessage(var Msg: tagMSG; var Handled: boolean);
    procedure IdleTimerTimer(Sender: TObject);

  protected
    FClosingTab: boolean;
    FCanClose: boolean;
    FClosing: boolean;
    FFirstTime: boolean;
    FLoading: boolean;
    FSetupMode: boolean;
    FChangingTab: boolean;
    FLastDownload: PDownloadInfo;
    LstDownload: TList;
    FUserList: TUserCollection;

    FIdleCounter: integer;
    FLastInput: Cardinal;
    FIdleTime: integer;

    procedure Chromium_OnAfterCreated(Sender: TObject;
      const browser: ICefBrowser);
    procedure Chromium_OnAddressChange(Sender: TObject;
      const browser: ICefBrowser; const frame: ICefFrame; const url: ustring);

    procedure Chromium_OnTitleChange(Sender: TObject;
      const browser: ICefBrowser; const Title: ustring);
    procedure Chromium_OnClose(Sender: TObject; const browser: ICefBrowser;
      var aAction: TCefCloseBrowserAction);
    procedure Chromium_OnBeforeClose(Sender: TObject;
      const browser: ICefBrowser);

    procedure Chromium_OnBeforePopup(Sender: TObject;
      const browser: ICefBrowser; const frame: ICefFrame;
      const targetUrl, targetFrameName: ustring;
      targetDisposition: TCefWindowOpenDisposition; userGesture: boolean;
      const popupFeatures: TCefPopupFeatures; var windowInfo: TCefWindowInfo;
      var client: ICefClient; var settings: TCefBrowserSettings;
      var extra_info: ICefDictionaryValue; var noJavascriptAccess: boolean;
      var Result: boolean);
    procedure Chromium_LoadEnd(Sender: TObject; const browser: ICefBrowser;
      const frame: ICefFrame; httpStatusCode: integer);
    procedure Chromium_LoadError(Sender: TObject; const browser: ICefBrowser;
      const frame: ICefFrame; errorCode: integer;
      const errorText, failedUrl: ustring);
    procedure Chromium_BeforeDownlad(Sender: TObject;
      const browser: ICefBrowser; const downloadItem: ICefDownloadItem;
      const SuggestedName: ustring; const callback: ICefBeforeDownloadCallback);
    procedure Chromium_DownloadUpdated(Sender: TObject;
      const browser: ICefBrowser; const downloadItem: ICefDownloadItem;
      const callback: ICefDownloadItemCallback);

    procedure BrowserCreatedMsg(var aMessage: TMessage);
      message CEF_AFTERCREATED;
    procedure BrowserDestroyWindowParentMsg(var aMessage: TMessage);
      message CEFBROWSER_DESTROYWNDPARENT;
    procedure BrowserDestroyTabMsg(var aMessage: TMessage);
      message CEFBROWSER_DESTROYTAB;

    procedure BrowserCheckTaggedTabsMsg(var aMessage: TMessage);
      message CEFBROWSER_CHECKTAGGEDTABS;
    procedure BrowserUpdateDownload(var aMessage: TMessage);
      message CEFBROWSER_UPDATEDOWNLOAD;
    procedure CEFInitializedMsg(var aMessage: TMessage);
      message CEFBROWSER_INITIALIZED;

    procedure WMMove(var aMessage: TWMMove); message WM_MOVE;
    procedure WMMoving(var aMessage: TMessage); message WM_MOVING;
    procedure WMEnterMenuLoop(var aMessage: TMessage); message WM_ENTERMENULOOP;
    procedure WMExitMenuLoop(var aMessage: TMessage); message WM_EXITMENULOOP;

    function FindByTabAndID(TabIndex, ID: integer): PDownloadInfo;
    function FindByButton(Btn: TcxButton): PDownloadInfo;
    procedure ClearDownloadList;
    function LimitedString(s: string; Fnt: TFont; Size: integer): string;
    procedure DownloadOnClick(Sender: TObject);
    procedure DownloadOnDropDown(Sender: TObject);
    procedure GetAbsolutePos(Ctrl: TControl; var X, Y: integer);
    procedure CheckDownloadPanelVisible;

    function AllTabSheetsAreTagged: boolean;
    procedure CloseAllBrowsers;
    function GetPageIndex(const aSender: TObject;
      var aPageIndex: integer): boolean;
    procedure NotifyMoveOrResizeStarted;

    procedure AddNewTabURL(TabCaption, url, Directory, TabUser,
      TabPassword: string; LoadAtStartup: boolean;
      AllowOpenDialog: TNullableBoolean { ; bm: TBookmark };
      aCommandList: TList<TCommandInfo>);

    procedure AddNewTabProgram(TabCaption, ProgramName, Params, TabUser,
      TabPassword: string; WaitMS: integer; LoadAtStartup, HideFrames,
      AutoKill: boolean { ; bm: TBookmark }; aCommandList: TList<TCommandInfo>);

    function CountWebTabs: integer;
    function CountWebTabsWithChrommium: integer;
    function CountAppTabs: integer;
    function CountAppTabsRunning: integer;
    procedure CloseTab(aTab: TBrowserTabSheet);

    procedure GoBack;
    procedure GoForward;
    procedure Reload;
    procedure Stop;
    procedure GoURL(url: string);
    function AreClosedAllBrowsers: boolean;
    procedure ValidateExpirationDate;
  public
    FUserID: integer;
    FUserIdx: integer;
    FUserName: string;

    procedure GenerateTabs;
    function ReplaceParams(s, TabUser, TabPassword: string;
      Params: TParamCollection): string; // overload;

    procedure ClearEnvironment;
    procedure UpdateEnvironment;
    function GetIniFileName: string;
    procedure LoadIniFile;
    procedure SaveIniFile;
    procedure DefaultSetup;
    function ProgramExpired: boolean;
    property Loading: boolean read FLoading;
    property SetupMode: boolean read FSetupMode;
    property UserID: integer read FUserID write FUserID;
    property UserList: TUserCollection read FUserList write FUserList;

  end;


  TMenuItemCmd = class(TMenuItem)
  private
    FCommandInfo: TCommandInfo;
    procedure RunCmd;
  public
    procedure Execute;
    property CommandInfo: TCommandInfo read FCommandInfo write FCommandInfo;
  end;

var
  MainForm: TMainForm;

procedure CreateGlobalCEFApp;
function Str2Date(s: WideString): TDate;

implementation

{$R *.dfm}

procedure GlobalCEFApp_OnContextInitialized;
begin
  if (MainForm <> nil) and MainForm.HandleAllocated then
    PostMessage(MainForm.Handle, CEFBROWSER_INITIALIZED, 0, 0);
end;

procedure CreateGlobalCEFApp;
begin
  GlobalCEFApp := TCefApplication.Create;
  GlobalCEFApp.OnContextInitialized := GlobalCEFApp_OnContextInitialized;
end;

function LPad(s: WideString; Len: integer; c: char): WideString;
begin
  Result := s;

  while Length(Result) < Len do
    Result := c + Result;

end;

constructor TBrowserTabSheet.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FKind := ttNone;
  FURLProgram := '';
  FParams := '';
  FWaitMS := 500;
  FHideFrames := True;
  FLoadAtStartup := False;
  FAutoKill := False;
  FAllowOpenDialog := nbNull;
  FDirectory := '';
  FTabUser := '';
  FTabPassword := '';
  FProcID := 0;
  FThreadID := 0;
  FAppWnd := 0;
  FChromium := nil;
  FIsLoaded := False;
  FParentCEFWin := nil;
  FBtnPlay := nil;
  FTimer := TTimer.Create(Self);
  FTimer.Enabled := False;
  FTimer.Interval := 100;
  FTimer.OnTimer := RunningTimer;
  FQueue := TList.Create;
  FWinList := TWinList.Create;

end;

destructor TBrowserTabSheet.Destroy;
begin
  if Assigned(FBtnPlay) then
    FBtnPlay.Free;

  FTimer.Free;

  ClearQueue;
  FQueue.Free;
  FWinList.Free;

  inherited Destroy;
end;

procedure TBrowserTabSheet.RelocateBtnPlay;
begin
  if Assigned(FBtnPlay) then
  begin
    FBtnPlay.Left := (ClientWidth - FBtnPlay.Width) div 2;
    FBtnPlay.Top := (ClientHeight - FBtnPlay.Height) div 2;
  end;
end;

procedure TBrowserTabSheet.CreateBtnPlay;
begin
  FBtnPlay := TcxButton.Create(Self);
  FBtnPlay.Parent := Self;
  FBtnPlay.Caption := '';
  FBtnPlay.Width := 140;
  FBtnPlay.Height := 140;
  FBtnPlay.LookAndFeel.SkinName := 'UserSkin';
  FBtnPlay.OnClick := BtnPlayClick;
  RelocateBtnPlay;
end;

procedure TBrowserTabSheet.Resize;
begin
  inherited Resize;

  if (Kind = ttProgram) and IsLoaded and ProgramStillRunning then
    ResizeApp(ClientWidth, ClientHeight);

  RelocateBtnPlay;
end;

function TBrowserTabSheet.ProgramStillRunning: boolean;
var
  Title: string;
  WinInfo: TWindowInfo;
  Current: Cardinal;
  L: TList;

{$REGION 'dwStyle and dwExStyle'}
  function dwStyleToStr(dwStyle: Cardinal): string;
  begin
    Result := '[';

    if (dwStyle and WS_BORDER) = WS_BORDER then
      Result := Result + 'WS_BORDER, ';

    if (dwStyle and WS_CAPTION) = WS_CAPTION then
      Result := Result + 'WS_CAPTION, ';

    if (dwStyle and WS_CHILD) = WS_CHILD then
      Result := Result + 'WS_CHILD, ';

    if (dwStyle and WS_CHILDWINDOW) = WS_CHILDWINDOW then
      Result := Result + 'WS_CHILDWINDOW, ';

    if (dwStyle and WS_CLIPCHILDREN) = WS_CLIPCHILDREN then
      Result := Result + 'WS_CLIPCHILDREN, ';

    if (dwStyle and WS_CLIPSIBLINGS) = WS_CLIPSIBLINGS then
      Result := Result + 'WS_CLIPSIBLINGS, ';

    if (dwStyle and WS_DISABLED) = WS_DISABLED then
      Result := Result + 'WS_DISABLED, ';

    if (dwStyle and WS_HSCROLL) = WS_HSCROLL then
      Result := Result + 'WS_HSCROLL, ';

    if (dwStyle and WS_ICONIC) = WS_ICONIC then
      Result := Result + 'WS_ICONIC, ';

    if (dwStyle and WS_MAXIMIZE) = WS_MAXIMIZE then
      Result := Result + 'WS_MAXIMIZE, ';

    if (dwStyle and WS_MAXIMIZEBOX) = WS_MAXIMIZEBOX then
      Result := Result + 'WS_MAXIMIZEBOX, ';

    if (dwStyle and WS_MINIMIZE) = WS_MINIMIZE then
      Result := Result + 'WS_MINIMIZE, ';

    if (dwStyle and WS_MINIMIZEBOX) = WS_MINIMIZEBOX then
      Result := Result + 'WS_MINIMIZEBOX, ';

    if (dwStyle and WS_OVERLAPPED) = WS_OVERLAPPED then
      Result := Result + 'WS_OVERLAPPED, ';

    if (dwStyle and WS_OVERLAPPEDWINDOW) = WS_OVERLAPPEDWINDOW then
      Result := Result + 'WS_OVERLAPPEDWINDOW, ';

    if (dwStyle and WS_POPUP) = WS_POPUP then
      Result := Result + 'WS_POPUP, ';

    if (dwStyle and WS_POPUPWINDOW) = WS_POPUPWINDOW then
      Result := Result + 'WS_POPUPWINDOW, ';

    if (dwStyle and WS_SIZEBOX) = WS_SIZEBOX then
      Result := Result + 'WS_SIZEBOX, ';

    if (dwStyle and WS_SYSMENU) = WS_SYSMENU then
      Result := Result + 'WS_SYSMENU, ';

    if (dwStyle and WS_TABSTOP) = WS_TABSTOP then
      Result := Result + 'WS_TABSTOP, ';

    if (dwStyle and WS_THICKFRAME) = WS_THICKFRAME then
      Result := Result + 'WS_THICKFRAME, ';

    if (dwStyle and WS_TILED) = WS_TILED then
      Result := Result + 'WS_TILED, ';

    if (dwStyle and WS_TILEDWINDOW) = WS_TILEDWINDOW then
      Result := Result + 'WS_TILEDWINDOW, ';

    if (dwStyle and WS_VISIBLE) = WS_VISIBLE then
      Result := Result + 'WS_VISIBLE, ';

    if (dwStyle and WS_VSCROLL) = WS_VSCROLL then
      Result := Result + 'WS_VSCROLL, ';

    if Result <> '' then
      Delete(Result, Length(Result) - 1, 2);

    Result := Result + ']';

  end;

  function dwExStyleToStr(dwExStyle: Cardinal): string;
  begin
    Result := '[';

    if (dwExStyle and WS_EX_ACCEPTFILES) = WS_EX_ACCEPTFILES then
      Result := Result + 'WS_EX_ACCEPTFILES, ';

    if (dwExStyle and WS_EX_APPWINDOW) = WS_EX_APPWINDOW then
      Result := Result + 'WS_EX_APPWINDOW, ';

    if (dwExStyle and WS_EX_CLIENTEDGE) = WS_EX_CLIENTEDGE then
      Result := Result + 'WS_EX_CLIENTEDGE, ';

    if (dwExStyle and WS_EX_COMPOSITED) = WS_EX_COMPOSITED then
      Result := Result + 'WS_EX_COMPOSITED, ';

    if (dwExStyle and WS_EX_CONTEXTHELP) = WS_EX_CONTEXTHELP then
      Result := Result + 'WS_EX_CONTEXTHELP, ';

    if (dwExStyle and WS_EX_CONTROLPARENT) = WS_EX_CONTROLPARENT then
      Result := Result + 'WS_EX_CONTROLPARENT, ';

    if (dwExStyle and WS_EX_LAYERED) = WS_EX_LAYERED then
      Result := Result + 'WS_EX_LAYERED, ';

    if (dwExStyle and WS_EX_LAYOUTRTL) = WS_EX_LAYOUTRTL then
      Result := Result + 'WS_EX_LAYOUTRTL, ';

    if (dwExStyle and WS_EX_LEFT) = WS_EX_LEFT then
      Result := Result + 'WS_EX_LEFT, ';

    if (dwExStyle and WS_EX_LEFTSCROLLBAR) = WS_EX_LEFTSCROLLBAR then
      Result := Result + 'WS_EX_LEFTSCROLLBAR, ';

    if (dwExStyle and WS_EX_LTRREADING) = WS_EX_LTRREADING then
      Result := Result + 'WS_EX_LTRREADING, ';

    if (dwExStyle and WS_EX_MDICHILD) = WS_EX_MDICHILD then
      Result := Result + 'WS_EX_MDICHILD, ';

    if (dwExStyle and WS_EX_NOACTIVATE) = WS_EX_NOACTIVATE then
      Result := Result + 'WS_EX_NOACTIVATE, ';

    if (dwExStyle and WS_EX_NOINHERITLAYOUT) = WS_EX_NOINHERITLAYOUT then
      Result := Result + 'WS_EX_NOINHERITLAYOUT, ';

    if (dwExStyle and WS_EX_NOPARENTNOTIFY) = WS_EX_NOPARENTNOTIFY then
      Result := Result + 'WS_EX_NOPARENTNOTIFY, ';

    if (dwExStyle and WS_EX_OVERLAPPEDWINDOW) = WS_EX_OVERLAPPEDWINDOW then
      Result := Result + 'WS_EX_OVERLAPPEDWINDOW, ';

    if (dwExStyle and WS_EX_PALETTEWINDOW) = WS_EX_PALETTEWINDOW then
      Result := Result + 'WS_EX_PALETTEWINDOW, ';

    if (dwExStyle and WS_EX_RIGHT) = WS_EX_RIGHT then
      Result := Result + 'WS_EX_RIGHT, ';

    if (dwExStyle and WS_EX_RIGHTSCROLLBAR) = WS_EX_RIGHTSCROLLBAR then
      Result := Result + 'WS_EX_RIGHTSCROLLBAR, ';

    if (dwExStyle and WS_EX_RTLREADING) = WS_EX_RTLREADING then
      Result := Result + 'WS_EX_RTLREADING, ';

    if (dwExStyle and WS_EX_STATICEDGE) = WS_EX_STATICEDGE then
      Result := Result + 'WS_EX_STATICEDGE, ';

    if (dwExStyle and WS_EX_TOOLWINDOW) = WS_EX_TOOLWINDOW then
      Result := Result + 'WS_EX_TOOLWINDOW, ';

    if (dwExStyle and WS_EX_TOPMOST) = WS_EX_TOPMOST then
      Result := Result + 'WS_EX_TOPMOST, ';

    if (dwExStyle and WS_EX_TRANSPARENT) = WS_EX_TRANSPARENT then
      Result := Result + 'WS_EX_TRANSPARENT, ';

    if (dwExStyle and WS_EX_WINDOWEDGE) = WS_EX_WINDOWEDGE then
      Result := Result + 'WS_EX_WINDOWEDGE, ';

    if Result <> '' then
      Delete(Result, Length(Result) - 1, 2);

    Result := Result + ']';

  end;
{$ENDREGION}

var
  ParentWnd, Temp: Cardinal;
  n: integer;
begin
  Result := (Kind = ttProgram) and ProcessStillActive(ProcID);

  if Result and not MainForm.FChangingTab and not MainForm.FClosing then
  begin
    if PageControl.ActivePage = Self then
    begin
      Current := GetFocusedWindowFromParent(AppWnd);
      Temp := Current;
      n := 0;
      while (n < 5) and (Temp <> AppWnd) and not IsWindow(Temp) do
      begin
        Inc(n);
        Temp := GetParent(Temp);
      end;

      if IsWindow(Temp) then
        Current := Temp;

      L := GetWindowList;

      if L.Count > 0 then
      begin
        if (Current <> 0) then
        begin
          Winapi.Windows.SetFocus(Current);
          UpdateQueue;

{$IFDEF TestForegroundWindow}
          FActiveWnd := Current;
          Title := GetWindowTitle(FActiveWnd);

          FillChar(WinInfo, Sizeof(WinInfo), 0);
          WinInfo.cbSize := Sizeof(WinInfo);
          GetWindowInfo(ActiveWnd, WinInfo);

          { MainForm.Memo1.Lines.Clear;
            MainForm.Memo1.Lines.Add('[Focused Control]');
            MainForm.Memo1.Lines.Add('Title: ' +  Title + '; ClassName: ' + GetWindowClass(ActiveWnd));
            MainForm.Memo1.Lines.Add('dwStyle: ' + dwStyleToStr(WinInfo.dwStyle));
            MainForm.Memo1.Lines.Add('dwExStyle: ' + dwExStyleToStr(WinInfo.dwStyle)); }
{$ENDIF}
        end;
      end
      else
      begin
        ClearQueue;
      end;
      L.Free;
    end;
  end
  else
    FActiveWnd := 0;
end;

procedure TBrowserTabSheet.CloseApp;
begin
  try
    if (Kind = ttProgram) and IsLoaded and ProgramStillRunning then
      KillWindow(FAppWnd);
  except
    on e: Exception do
      showmessage(e.Message);

  end;
end;

procedure TBrowserTabSheet.ResizeApp(Width, Height: integer);
begin
  uProcesses.ResizeApp(AppWnd, Width, Height);
end;

procedure TBrowserTabSheet.BtnPlayClick(Sender: TObject);
var
  s: string;
begin
  if (Kind = ttURL) then
  begin
    ParentCEFWin.Visible := True;
    LoadAtStartup := True;
    s := ReplaceParams(URLProgram);
    Chromium.LoadURL(s);
  end
  else if (Kind = ttProgram) and not IsLoaded and not ProgramStillRunning then
    RunApp;
end;

procedure TBrowserTabSheet.RunningTimer(Sender: TObject);
begin
  IsLoaded := ProgramStillRunning;
  FTimer.Enabled := IsLoaded;
  BtnPlay.Visible := not IsLoaded;

  if IsLoaded then
    ImageIndex := ICO_Green
  else
    ImageIndex := ICO_Black;
end;

procedure TBrowserTabSheet.ClearQueue;
  procedure ClearList(L: TList);
  var
    ApWin: ApWinInfo;
  begin
    while L.Count > 0 do
    begin
      ApWin := ApWinInfo(L[0]);
      L.Delete(0);
      Dispose(ApWin);
    end;
  end;

var
  L: TList;
begin
  while FQueue.Count > 0 do
  begin
    L := TList(FQueue[0]);
    FQueue[0] := nil;
    FQueue.Delete(0);

    if Assigned(L) then
    begin
      ClearList(L);
      L.Free;
    end;
  end;
end;

function CompareZOrder(Item1, Item2: Pointer): integer;
begin
  if ApWinInfo(Item1)^.ZOrder > ApWinInfo(Item2)^.ZOrder then
    Result := 1
  else if ApWinInfo(Item1)^.ZOrder < ApWinInfo(Item2)^.ZOrder then
    Result := -1
  else
    Result := 0;
end;

function TBrowserTabSheet.GetWindowList: TList;
var
  ApWin: ApWinInfo;
  i: integer;
  Wnd: Cardinal;
  WinInfo: TWindowInfo;
  Title, StrClassName: string;
  OK: boolean;
begin
  Result := TList.Create;
  FWinList.GetWinListByThread(FThreadID);

  if FWinList.Count > 0 then
  begin
    for i := 0 to FWinList.Count - 1 do
    begin
      Title := '';
      StrClassName := '';
      Wnd := Cardinal(FWinList.Objects[i]);
      OK := False;
      if Wnd <> AppWnd then
        try
          Title := GetWindowTitle(Wnd);
          StrClassName := GetWindowClass(Wnd);
          OK := True;
        except

        end;

      if OK and (StrClassName <> 'TApplication') then
      begin
        FillChar(WinInfo, Sizeof(WinInfo), 0);
        WinInfo.cbSize := Sizeof(WinInfo);
        GetWindowInfo(Wnd, WinInfo);

        New(ApWin);
        ApWin^.Handle := Wnd;
        ApWin^.dwStyle := WinInfo.dwStyle;
        ApWin^.dwExStyle := WinInfo.dwExStyle;
        ApWin^.Title := Title;
        ApWin^.ClassName := StrClassName;
        ApWin^.ZOrder := GetZOrder(Wnd);
        Result.Add(ApWin);
      end;
    end;
  end;
  Result.Sort(@CompareZOrder);
end;

procedure TBrowserTabSheet.UpdateQueue;
var
  ApWin: ApWinInfo;
  i: integer;
  L: TList;
begin
  L := GetWindowList;
  if L.Count > 0 then
    FQueue.Add(L);

  if FQueue.Count > 5 then
  begin
    L := TList(FQueue[0]);
    FQueue[0] := nil;
    FQueue.Delete(0);

    for i := 0 to L.Count - 1 do
    begin
      ApWin := ApWinInfo(L[i]);
      L[i] := nil;
      Dispose(ApWin);
    end;
    L.Clear;
    L.Free;
  end;
end;

function TBrowserTabSheet.ReplaceParams(s: string): string;
begin
  Result := MainForm.ReplaceParams(s, TabUser, TabPassword,
    MainForm.FUserList[MainForm.FUserIdx].Params);
end;

procedure TBrowserTabSheet.RunApp;
var
  s: string;
begin
  s := ReplaceParams(Params);
  LaunchApp(URLProgram, s, Self.Handle, WaitMS, HideFrames, FProcID,
    FThreadID, FAppWnd);

  IsLoaded := ProgramStillRunning;
  BtnPlay.Visible := not IsLoaded;
  FTimer.Enabled := IsLoaded;
  ResizeApp(ClientWidth, ClientHeight);

  if IsLoaded then
    ImageIndex := ICO_Green
  else
    ImageIndex := ICO_Red;

{$IFDEF TestForegroundWindow}
  MainForm.Memo1.Lines.Add(Format('%s, ProcID [%s], ThreadID [%s], AppWnd []',
    [URLProgram, IntToHex(FProcID, 8), IntToHex(FThreadID, 8),
    IntToHex(FAppWnd)]));
{$ENDIF}
end;

procedure TBrowserTabSheet.SetKind(Value: TTabType);
begin
  if FKind <> Value then
  begin
    FKind := Value;
  end;
end;

procedure TBrowserTabSheet.SetURLProgram(Value: string);
begin
  if Value <> FURLProgram then
  begin
    FURLProgram := Value;
  end;
end;

procedure TBrowserTabSheet.SetParams(Value: string);
begin
  if FParams <> Value then
  begin
    FParams := Value;
  end;
end;

procedure TBrowserTabSheet.SetWaitMS(Value: integer);
begin
  if FWaitMS <> Value then
  begin
    FWaitMS := Value;
  end;
end;

procedure TBrowserTabSheet.SetHideFrames(Value: boolean);
begin
  if FHideFrames <> Value then
  begin
    FHideFrames := Value;
  end;
end;

procedure TBrowserTabSheet.SetLoadAtStartup(Value: boolean);
begin
  if FLoadAtStartup <> Value then
  begin
    FLoadAtStartup := Value;
  end;
end;

procedure TBrowserTabSheet.SetAutoKill(Value: boolean);
begin
  if FAutoKill <> Value then
  begin
    FAutoKill := Value;
  end;
end;

procedure TBrowserTabSheet.SetAllowOpenDialog(Value: TNullableBoolean);
begin
  if FAllowOpenDialog <> Value then
  begin
    FAllowOpenDialog := Value;
  end;
end;

procedure TBrowserTabSheet.SetDirectory(Value: string);
begin
  if FDirectory <> Value then
  begin
    FDirectory := Value;
  end;
end;

procedure TBrowserTabSheet.SetTabUser(Value: string);
begin
  if FTabUser <> Value then
  begin
    FTabUser := Value;
  end;
end;

procedure TBrowserTabSheet.SetTabPassword(Value: string);
begin
  if FTabPassword <> Value then
  begin
    FTabPassword := Value;
  end;
end;

procedure TBrowserTabSheet.SetIsLoaded(Value: boolean);
begin
  if FIsLoaded <> Value then
  begin
    FIsLoaded := Value;
  end;
end;

procedure TBrowserTabSheet.SetProcID(Value: Cardinal);
begin
  if FProcID <> Value then
  begin
    FProcID := Value;
  end;
end;

procedure TBrowserTabSheet.SetThreadID(Value: Cardinal);
begin
  if FThreadID <> Value then
  begin
    FThreadID := Value;
  end;
end;

procedure TBrowserTabSheet.SetAppWnd(Value: DWord);
begin
  if FAppWnd <> Value then
  begin
    FAppWnd := Value;
  end;
end;

procedure TBrowserTabSheet.SetActiveWnd(Value: DWord);
begin
  if FActiveWnd <> Value then
  begin
    FActiveWnd := Value;
  end;
end;

procedure TBrowserTabSheet.SetChromium(Value: TChromium);
begin
  if Value <> FChromium then
  begin
    FChromium := Value;
  end;
end;

procedure TBrowserTabSheet.SetParentCEFWin(Value: TCEFWindowParent);
begin
  if FParentCEFWin <> Value then
  begin
    FParentCEFWin := Value;
  end;
end;

procedure TBrowserTabSheet.SetBtnPlay(Value: TcxButton);
begin
  if FBtnPlay <> Value then
  begin
    FBtnPlay := Value;
  end;
end;

procedure TMainForm.AddTabBtnClick(Sender: TObject);
begin
  AddNewTabURL(URLCbx.Text, 'New Tab', '', '', '', True,
    nbTrue { , nil } , nil);
end;

procedure TMainForm.actChangePasswordExecute(Sender: TObject);
var
  PasswordDlg: TResetPasswordDlg;
begin
{  FAddEditUser.UserAction := uaChangePassword;
  FAddEditUser.edUserName.Text := FUserName;
  FAddEditUser.PreviousPassword := FUserList.Items[FUserIdx].Password;
  if FAddEditUser.ShowModal = mrOK then
  begin
    FUserList.Items[FUserIdx].Password := FAddEditUser.edPassword.Text;

    SaveIniFile;
    MessageDlg('Password updated', mtInformation, [mbOK], 0);
  end;}

  PasswordDlg := TResetPasswordDlg.Create(Self);
  PasswordDlg.OldPassword := FUserList.Items[FUserIdx].Password;

  if PasswordDlg.ShowModal = mrOK then
  begin
    FUserList.Items[FUserIdx].Password := PasswordDlg.NewPassword;
    SaveIniFile;
    MessageDlg('Password updated', mtInformation, [mbOK], 0);
  end;
end;

procedure TMainForm.actCloseAppExecute(Sender: TObject);
var
  Tab: TBrowserTabSheet;
begin
  if Math.InRange(actCloseApp.Tag, 0, PageControlApp.PageCount - 1) then
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[actCloseApp.Tag]);
    if (Tab.Kind = ttProgram) and Tab.AutoKill and Tab.ProgramStillRunning then
      Tab.CloseApp;
  end;
end;

procedure TMainForm.actDownloadOpenExecute(Sender: TObject);
begin
  if Assigned(FLastDownload) then
    ShellExecute(Self.WindowHandle, 'open', PChar(FLastDownload^.FullPath), nil,
      nil, SW_SHOWNORMAL)

end;

procedure TMainForm.actDownloadShowInFolderExecute(Sender: TObject);
begin
  if Assigned(FLastDownload) then
    ShellExecute(Self.WindowHandle, 'open', 'explorer.exe',
      PChar('/select, "' + FLastDownload^.FullPath + '"'), nil, SW_SHOWNORMAL);
end;

procedure TMainForm.actRefreshPageExecute(Sender: TObject);
var
  Tab: TBrowserTabSheet;
begin
  if Math.InRange(actCloseApp.Tag, 0, PageControlApp.PageCount - 1) then
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[actCloseApp.Tag]);
    if (Tab.Kind = ttURL) or ((Tab.Kind = ttProgram) and
      not Tab.ProgramStillRunning) then
      Tab.BtnPlay.Click;
  end;
end;

procedure TMainForm.actSetupExecute(Sender: TObject);
begin
  FLogin.FApplyUserPassword := False;
  if FLogin.ShowModal = mrOK then
  begin
    if FSetup.ShowModal = mrOK then
    begin
      FUserList.Assign(FSetup.FUserList);
      SaveIniFile;
      // UpdateEnvironment;
    end
    else
    begin
      if FSetup.btnOK.Enabled then // HasChange
        LoadIniFile;
    end;
  end
  else
  begin
    MessageDlg('Invalid access, please contact to System Administrator',
      mtError, [mbOK], 0);

    Application.Terminate;
  end;
end;

procedure TMainForm.actTabPropertiesExecute(Sender: TObject);
var
  Tab: TBrowserTabSheet;
begin
  Tab := TBrowserTabSheet(PageControlApp.ActivePage);
  FTabProperties.cbAllowOpenDlg.Checked := FUserList.Items[FUserIdx].Tabs.Items
    [Tab.TabIndex].AllowOpenDialog = nbTrue;

  FTabProperties.edDefaultDownloadPath.Text := FUserList.Items[FUserIdx]
    .Tabs.Items[Tab.TabIndex].DownloadPath;
  FTabProperties.edUser.Text := FUserList.Items[FUserIdx].Tabs.Items
    [Tab.TabIndex].TabUser;
  FTabProperties.edPassword.Text := FUserList.Items[FUserIdx].Tabs.Items
    [Tab.TabIndex].TabPassword;

  if FTabProperties.ShowModal = mrOK then
  begin
    if FTabProperties.cbAllowOpenDlg.Checked then
      Tab.AllowOpenDialog := nbTrue
    else
      Tab.AllowOpenDialog := nbFalse;

    Tab.Directory := FTabProperties.edDefaultDownloadPath.Text;
    Tab.TabUser := FTabProperties.edUser.Text;
    Tab.TabPassword := FTabProperties.edPassword.Text;

    FUserList.Items[FUserIdx].Tabs.Items[Tab.TabIndex].AllowOpenDialog :=
      Tab.AllowOpenDialog;
    FUserList.Items[FUserIdx].Tabs.Items[Tab.TabIndex].DownloadPath :=
      Tab.Directory;

    FUserList.Items[FUserIdx].Tabs.Items[Tab.TabIndex].TabUser := Tab.TabUser;
    FUserList.Items[FUserIdx].Tabs.Items[Tab.TabIndex].TabPassword :=
      Tab.TabPassword;

    SaveIniFile;
  end;
end;

procedure TMainForm.AddNewTabURL(TabCaption, url, Directory, TabUser,
  TabPassword: string; LoadAtStartup: boolean;
  AllowOpenDialog: TNullableBoolean { ; bm: TBookmark };
  aCommandList: TList<TCommandInfo>);
var
  Tab: TBrowserTabSheet;
begin
  Tab := TBrowserTabSheet.Create(PageControlApp);
  Tab.Caption := TabCaption;
  Tab.PageControl := PageControlApp;
  Tab.URLProgram := url;
  Tab.Kind := ttURL;
  Tab.LoadAtStartup := LoadAtStartup;
  Tab.AllowOpenDialog := AllowOpenDialog;
  Tab.Directory := Directory;
  Tab.TabUser := TabUser;
  Tab.TabPassword := TabPassword;
  Tab.CommandList := aCommandList;
  Tab.ImageIndex := ICO_Black;
  Tab.CreateBtnPlay;
  Tab.BtnPlay.OptionsImage.Glyph.Assign(cxButton1.OptionsImage.Glyph);
  Tab.BtnPlay.SendToBack;

  Tab.ParentCEFWin := TCEFWindowParent.Create(Tab);
  Tab.ParentCEFWin.Parent := Tab;
  Tab.ParentCEFWin.Color := clWhite;
  Tab.ParentCEFWin.Align := alClient;
  Tab.ParentCEFWin.Visible := LoadAtStartup;

  Tab.Chromium := TChromium.Create(Tab);
  Tab.Chromium.OnAfterCreated := Chromium_OnAfterCreated;
  Tab.Chromium.OnAddressChange := Chromium_OnAddressChange;
  Tab.Chromium.OnClose := Chromium_OnClose;
  Tab.Chromium.OnBeforeClose := Chromium_OnBeforeClose;
  Tab.Chromium.OnBeforePopup := Chromium_OnBeforePopup;
  Tab.Chromium.OnLoadEnd := Chromium_LoadEnd;
  Tab.Chromium.OnLoadError := Chromium_LoadError;
  Tab.Chromium.OnBeforeDownload := Chromium_BeforeDownlad;
  Tab.Chromium.OnDownloadUpdated := Chromium_DownloadUpdated;

  Tab.Chromium.CreateBrowser(Tab.ParentCEFWin, '');

end;

procedure TMainForm.AddNewTabProgram(TabCaption, ProgramName, Params, TabUser,
  TabPassword: string; WaitMS: integer; LoadAtStartup, HideFrames,
  AutoKill: boolean { ; bm: TBookmark }; aCommandList: TList<TCommandInfo>);
var
  Tab: TBrowserTabSheet;
begin
  Tab := TBrowserTabSheet.Create(PageControlApp);
  Tab.Caption := TabCaption;
  Tab.PageControl := PageControlApp;
  Tab.URLProgram := ProgramName;
  Tab.Params := Params;
  Tab.TabUser := TabUser;
  Tab.TabPassword := TabPassword;
  Tab.Kind := ttProgram;
  Tab.WaitMS := WaitMS;
  Tab.LoadAtStartup := LoadAtStartup;
  Tab.HideFrames := HideFrames;
  Tab.AutoKill := AutoKill;
  Tab.CommandList := aCommandList;
  Tab.ImageIndex := ICO_Black;
  Tab.CreateBtnPlay;
  Tab.BtnPlay.OptionsImage.Glyph.Assign(cxButton1.OptionsImage.Glyph);
  Tab.BtnPlay.SendToBack;

  if Tab.LoadAtStartup then
    Tab.RunApp;
end;

function TMainForm.CountWebTabs: integer;
var
  i: integer;
  Tab: TBrowserTabSheet;
begin
  Result := 0;

  for i := 0 to PageControlApp.PageCount - 1 do
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[i]);
    if Tab.Kind = ttURL then
      Inc(Result);

  end;
end;

function TMainForm.CountWebTabsWithChrommium: integer;
var
  i: integer;
  Tab: TBrowserTabSheet;
begin
  Result := 0;

  for i := 0 to PageControlApp.PageCount - 1 do
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[i]);
    if (Tab.Kind = ttURL) and Assigned(Tab.ParentCEFWin) and
      Assigned(Tab.Chromium) then
      Inc(Result);

  end;
end;

function TMainForm.CountAppTabs: integer;
var
  i: integer;
  Tab: TBrowserTabSheet;
begin
  Result := 0;

  for i := 0 to PageControlApp.PageCount - 1 do
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[i]);
    if Tab.Kind = ttProgram then
      Inc(Result);

  end;
end;

function TMainForm.CountAppTabsRunning: integer;
var
  i: integer;
  Tab: TBrowserTabSheet;
begin
  Result := 0;

  for i := 0 to PageControlApp.PageCount - 1 do
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[i]);
    if (Tab.Kind = ttProgram) and Tab.IsLoaded and Tab.ProgramStillRunning then
      Inc(Result);

  end;
end;

procedure TMainForm.CloseTab(aTab: TBrowserTabSheet);
// var
// Tab: TBrowserTabSheet;
begin
  if not Assigned(aTab) then
    exit;
  // Tab := TBrowserTabSheet(PageControlApp.ActivePage);

  if (aTab.Kind = ttURL) and Assigned(aTab.Chromium) then
  begin
    FClosingTab := True;
    try
      aTab.Chromium.StopLoad;
      aTab.Chromium.CloseAllConnections(True);
      aTab.Chromium.CloseBrowser(True);
    except
      on e: Exception do
        showmessage(e.Message);
    end;
  end
  else if aTab.Kind = ttProgram then
  begin
    if aTab.IsLoaded and aTab.ProgramStillRunning then
      aTab.CloseApp;
    aTab.Free;
  end;

  Sleep(500);
  // Application.ProcessMessages;
end;

procedure TMainForm.GoBack;
begin
  if Assigned(PageControlApp.ActivePage) and
    Assigned(TBrowserTabSheet(PageControlApp.ActivePage).Chromium) then

    TBrowserTabSheet(PageControlApp.ActivePage).Chromium.GoBack;
end;

procedure TMainForm.GoForward;
begin
  if Assigned(PageControlApp.ActivePage) and
    Assigned(TBrowserTabSheet(PageControlApp.ActivePage).Chromium) then

    TBrowserTabSheet(PageControlApp.ActivePage).Chromium.GoForward;
end;

procedure TMainForm.Reload;
begin
  if Assigned(PageControlApp.ActivePage) and
    Assigned(TBrowserTabSheet(PageControlApp.ActivePage).Chromium) then

    TBrowserTabSheet(PageControlApp.ActivePage).Chromium.Reload;
end;

procedure TMainForm.Stop;
begin
  if Assigned(PageControlApp.ActivePage) and
    Assigned(TBrowserTabSheet(PageControlApp.ActivePage).Chromium) then

    TBrowserTabSheet(PageControlApp.ActivePage).Chromium.StopLoad;
end;

procedure TMainForm.GoURL(url: string);
begin
  if Assigned(PageControlApp.ActivePage) and
    Assigned(TBrowserTabSheet(PageControlApp.ActivePage).Chromium) then

    TBrowserTabSheet(PageControlApp.ActivePage).Chromium.LoadURL(url);
end;

procedure TMainForm.IdleTimerTimer(Sender: TObject);
var
  inputInfo: TLastInputInfo;

begin

  if FIdleTime = 0 then
    exit;

  FIdleCounter := FIdleCounter + integer(IdleTimer.Interval div 1000);

  inputInfo.cbSize := Sizeof(inputInfo);
  if GetLastInputInfo(inputInfo) then
  begin
    if inputInfo.dwTime <> FLastInput then
    begin
      FLastInput := inputInfo.dwTime;
      FIdleCounter := 0;
    end;
  end;

  if (FIdleCounter >= FIdleTime * 60) then
  begin
    // IdleTimer.Enabled := False;

    if screen.ActiveForm.ClassType = TMainForm then
    begin
      FUserLogin.btnCancel.Caption := 'Exit';
      while FUserLogin.ShowModal <> mrOK do
      begin
        if MessageDlg('Are You Sure To Close Program?',
          TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          Halt;
      end;
      FUserLogin.btnCancel.Caption := 'Cancel';
    end;

    if screen.ActiveForm.ClassType = TFSetup then
    begin
      FLogin.btnCancel.Caption := 'Exit';
      while FLogin.ShowModal <> mrOK do
      begin
        if MessageDlg('Are You Sure To Close Program?',
          TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          Halt;
      end;
      FLogin.btnCancel.Caption := 'Cancel';
    end;
  end

end;

procedure TMainForm.RemoveTabBtnClick(Sender: TObject);
begin
  CloseTab(TBrowserTabSheet(PageControlApp.ActivePage));

end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var canclose: boolean);
var
  i: integer;
  s: string;
  Tab: TBrowserTabSheet;
  task: ITask;
begin
  if MessageDlg('Are You Sure To Close  Program?', TMsgDlgType.mtConfirmation, [tmsgdlgbtn.mbCancel, TMsgDlgBtn.mbClose],0) = mrCancel then
  begin
  CanClose := false;
  exit;
  end;

  if FClosingTab then
    canclose := False

  else
  begin
    if PageControlApp.PageCount = 0 then
      canclose := True

    else
    begin
      if not FClosing then
      begin
        s := '';
        for i := 0 to PageControlApp.PageCount - 1 do
        begin
          Tab := TBrowserTabSheet(PageControlApp.Pages[i]);
          if (Tab.Kind = ttProgram) and not Tab.AutoKill and Tab.ProgramStillRunning
          then

            s := s + Tab.Caption + ', ';
        end;

        if s <> '' then
        begin
          Delete(s, Length(s) - 1, 2);
          FCanClose := False;
          MessageDlg
            (Format('CompuEnterprise cannot be closed because applications [%s] still running. Please close applications first to avoid data loss.',
            [s]), mtWarning, [mbOK], 0);
          exit;
        end;
      end;

      canclose := FCanClose;

      if not(FClosing) then
      begin
        FClosing := True;
        Visible := False;

        ClearDownloadList;
        CloseAllBrowsers;
      end;
    end;
  end;

end;

procedure TMainForm.CloseAllBrowsers;
var
  k: integer;
  Tab: TBrowserTabSheet;
begin
  try
    k := pred(PageControlApp.PageCount);

    while (k >= 0) do
    begin
      Tab := TBrowserTabSheet(PageControlApp.Pages[k]);
      if (Tab.Kind = ttURL) and Assigned(Tab.Chromium) then
      begin
        Tab.Chromium.OnDownloadUpdated := nil;
        Tab.Chromium.OnBeforeDownload := nil;
        CloseTab(Tab);

        // //Tab.Chromium.CloseBrowser(True);
      end
      else if (Tab.Kind = ttProgram) and Tab.ProgramStillRunning then
      begin
        Tab.CloseApp;
      end;

      dec(k);
    end;
  except
    on e: Exception do
      showmessage(e.Message);

  end;
end;

function TMainForm.AreClosedAllBrowsers: boolean;
var
  i: integer;
  Tab: TBrowserTabSheet;
begin
  Result := True;

  for i := 0 to PageControlApp.PageCount - 1 do
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[i]);
    if (Tab.Kind = ttURL) and Assigned(Tab.Chromium) then
    begin
      Result := False;
      exit;
    end;
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FClosingTab := False;
  FCanClose := False;
  FClosing := False;
  FFirstTime := True;
  FLoading := False;
  FChangingTab := False;
  FSetupMode := (ParamCount > 0) and (UpperCase(ParamStr(1)) = 'SETUP');
  LstDownload := TList.Create;
  FLastDownload := nil;
  FUserList := TUserCollection.Create(TUserItem);
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
{$IFDEF TestForegroundWindow}
  Memo1.Visible := True;
{$ENDIF}
  if FFirstTime then
  begin
    // Delete pages created at design time
    while PageControlApp.PageCount > 0 do
      PageControlApp.Pages[0].Free;

    FLogin.FAdminPassword := AdminPassword;
    if not Assigned(GlobalCEFApp) or not GlobalCEFApp.GlobalContextInitialized
    then
      Application.Terminate;

    FSetup.LoadSkins;
    LoadIniFile;
    FUserName := '';
    FUserID := -1;
    FUserIdx := -1;
    actChangePassword.Enabled := True;

    if FSetupMode then
      actSetup.Execute;

    IdleTimer.Enabled := True;

    FUserIdx := 0;
    if FUserList.Count > 1 then
    begin
      if FUserLogin.ShowModal = mrOK then
      begin
        FUserID := FUserLogin.UserID;
        FUserName := FUserLogin.UserName;
        FUserIdx := FUserList.IndexOf(FUserID);
        FIdleTime := FUserList.Items[FUserIdx].IdleTime;
        // IdleTimer.Interval := FIdleTime * 60000;
        // IdleTimer.Enabled := not (FIdleTime = 0);
        actChangePassword.Enabled := True;
      end
      else
      begin
        MessageDlg('Invalid access, please contact to System Administrator',
          mtError, [mbOK], 0);
        Application.Terminate;
      end;
    end;

    if FileExists(GetIniFileName) then
      UpdateEnvironment
    else if not FSetupMode then
    begin
      MessageDlg
        ('Setup file not configured, please contact to System Administrator',
        mtError, [mbOK], 0);
      Application.Terminate;
    end;

    // Added by yona 6/1521
    TimerUpdateDownload.Enabled := True;
    FFirstTime := False;
  end;
end;

procedure TMainForm.GenerateTabs;
var
  i: integer;
begin
  PageControlApp.Properties.HideTabs :=
    (FUserList.Items[FUserIdx].Tabs.Count = 1) and FSetup.cbRemoveBar.Checked;

  for i := 0 to FUserList.Items[FUserIdx].Tabs.Count - 1 do
    if FUserList.Items[FUserIdx].Tabs.Items[i].Kind = ttURL then
      AddNewTabURL(FUserList.Items[FUserIdx].Tabs.Items[i].TabCaption,
        FUserList.Items[FUserIdx].Tabs.Items[i].URLProgram,
        FUserList.Items[FUserIdx].Tabs.Items[i].DownloadPath,
        FUserList.Items[FUserIdx].Tabs.Items[i].TabUser,
        FUserList.Items[FUserIdx].Tabs.Items[i].TabPassword,
        FUserList.Items[FUserIdx].Tabs.Items[i].LoadAtStartup,
        FUserList.Items[FUserIdx].Tabs.Items[i].AllowOpenDialog,
        FUserList.Items[FUserIdx].Tabs.Items[i].CommandList)
    else
      AddNewTabProgram(FUserList.Items[FUserIdx].Tabs.Items[i].TabCaption,
        FUserList.Items[FUserIdx].Tabs.Items[i].URLProgram,
        FUserList.Items[FUserIdx].Tabs.Items[i].Params,
        FUserList.Items[FUserIdx].Tabs.Items[i].TabUser,
        FUserList.Items[FUserIdx].Tabs.Items[i].TabPassword,
        FUserList.Items[FUserIdx].Tabs.Items[i].WaitMS,
        FUserList.Items[FUserIdx].Tabs.Items[i].LoadAtStartup,
        FUserList.Items[FUserIdx].Tabs.Items[i].HideFrames,
        FUserList.Items[FUserIdx].Tabs.Items[i].AutoKill,
        FUserList.Items[FUserIdx].Tabs.Items[i].CommandList);
end;

function TMainForm.ReplaceParams(s, TabUser, TabPassword: string;
  Params: TParamCollection): string;

const
  coUser = '$tabuser';
  coPassword = '$tabpassword';
var
  i: integer;
  Key, Value: string;
begin
  Result := StringReplace(s, coUser, TabUser, [rfReplaceAll, rfIgnoreCase]);
  Result := StringReplace(Result, coPassword, TabPassword,
    [rfReplaceAll, rfIgnoreCase]);

  for i := 0 to Params.Count - 1 do
    Result := StringReplace(Result, '$' + Params.Items[i].Key,
      Params.Items[i].Value, [rfReplaceAll, rfIgnoreCase]);
end;

procedure TMainForm.ClearEnvironment;
begin
  try
    while PageControlApp.PageCount > 0 do
    begin
      CloseTab(TBrowserTabSheet(PageControlApp.ActivePage));
    end;
  except
    on e: Exception do
      showmessage(e.Message);

  end;
end;

procedure TMainForm.UpdateEnvironment;
begin
  ClearEnvironment;
  case FSetup.rgTabPosition.ItemIndex of
    0:
      PageControlApp.Properties.TabPosition := tpTop;
    1:
      PageControlApp.Properties.TabPosition := tpLeft;
    2:
      PageControlApp.Properties.TabPosition := tpRight;
    3:
      PageControlApp.Properties.TabPosition := tpBottom;
  end;

  if not ProgramExpired then
  begin
    GenerateTabs;
    TimerValidateExpirationDate.Enabled := FSetup.edExpirationDate.Tag = 0;
  end;
  Caption := FSetup.edStoreName.Text + ' - ' +
    PageControlApp.ActivePage.Caption;

end;

function TMainForm.ProgramExpired: boolean;
begin
  Result := (FSetup.edExpirationDate.Tag = 0) and
    (FSetup.edExpirationDate.Text <> '') and
    (FSetup.edExpirationDate.Date < Trunc(Now));

  if Result then
  begin
    MessageDlg('Program has expired, please contact to System Administrator',
      mtError, [mbOK], 0);

    ClearEnvironment;
  end;
end;

function TMainForm.GetIniFileName: string;
begin
  Result := Copy(ParamStr(0), 1, Length(ParamStr(0)) - 3) + 'ini';
end;

function Str2Date(s: WideString): TDate;
var
  sm, sd, sy: WideString;
  m, d, Y: word;
begin
  sm := Copy(s, 1, 2);
  sd := Copy(s, 4, 2);
  sy := Copy(s, 7, 4);

  m := StrToInt(sm);
  d := StrToInt(sd);
  Y := StrToInt(sy);

  Result := EncodeDate(Y, m, d);
end;

function GenerateTmpFile: string;
begin
  repeat
    Result := ExtractFilePath(ParamStr(0)) + Format('%d.tmp', [Random(999999)]);
  until not FileExists(Result);
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FUserList.Free;
end;

procedure TMainForm.LoadIniFile;
var
  Ini: TMemoryIniStructure;
  Lst: TStringList;
  FileName: WideString;
  s: WideString;
  i, n, Idx: integer;
  Tab, url: WideString;
  StrKind, Params: WideString;
  Dir: WideString;
  TabUser: WideString;
  TabPassword: WideString;
  WaitMS: integer;
  HideFrames: boolean;
  LoadAtStartup: boolean;
  AutoKill: boolean;
  AllowOpenDlg: WideString;
  Key, Value: WideString;
  User, Password: WideString;
  IdleTime: integer;
  UserID: integer;
  Kind: TTabType;
  u: TUserItem;
  t: TTabItem;
  p: TParamItem;
  nb: TNullableBoolean;
{$IFDEF Encrypt}
  Rijndael: TDCP_rijndael;
{$ENDIF}
begin
  FileName := GetIniFileName;
  if not FileExists(FileName) then
    exit;

  Lst := TStringList.Create;
  Ini := TMemoryIniStructure.Create;
  s := TFile.ReadAllText(FileName);

{$IFDEF Encrypt}
  Rijndael := TDCP_rijndael.Create(Self);
  Rijndael.InitStr(strEncryptKey, TDCP_sha256);
  s := Rijndael.DecryptString(s);
  Rijndael.Free;
{$ENDIF}
  Lst.Text := s;
  Ini.LoadFromStringList(Lst);
  Lst.Free;

  FUserList.Clear;
  FUserList.AddUser(0, '<Unassigned values>', '', 3); // Unassigned info
  FLoading := True;
  s := Ini.ReadString('Application', 'ExpirationDate', '');

  if s = '' then
  begin
    FSetup.edExpirationDate.Text := '';
    FSetup.edExpirationDate.Tag := 1;
  end
  else
  begin
    FSetup.edExpirationDate.Date := Str2Date(s);
    FSetup.edExpirationDate.Tag := 0;
    FSetup.edExpirationDate.Update;
  end;

  FSetup.edStoreName.Text := Ini.ReadString('Application', 'StoreName', '');
  FSetup.edPassword.Text := Ini.ReadString('Application', 'UserPassword', '');
  FSetup.edPasswordValidation.Text := '';
  FSetup.edDefaultDownloadPath.Text := Ini.ReadString('Application',
    'DefaultDownloadPath', '');

  FSetup.cbRemoveBar.Checked := Ini.ReadBool('Application', 'RemoveBar', True);
  FSetup.cbAllowOpenDlg.Checked := Ini.ReadBool('Application',
    'AllowOpenDlg', True);

  s := UpperCase(Ini.ReadString('Look and Feel', 'TabPosition', 'tpTop'));
  if s = 'TOP' then
    FSetup.rgTabPosition.ItemIndex := 0
  else if s = 'LEFT' then
    FSetup.rgTabPosition.ItemIndex := 1
  else if s = 'RIGHT' then
    FSetup.rgTabPosition.ItemIndex := 2
  else if s = 'BOTTOM' then
    FSetup.rgTabPosition.ItemIndex := 3;

  FSetup.cbSkinName.Text := Ini.ReadString('Look and Feel', 'Skin', '');
  FSetup.UpdateSkin;

  try
    FSetup.FNextUserID := Ini.ReadInteger('Users', 'NextUserID', 1);
    n := Ini.ReadInteger('Users', 'Count', 0);
    for i := 1 to n do
    begin
      UserID := Ini.ReadInteger('Users', 'UserID_' + LPad(IntToStr(i),
        3, '0'), 0);
      User := Ini.ReadString('Users', 'UserName_' + LPad(IntToStr(i), 3,
        '0'), '');
      Password := Ini.ReadString('Users', 'Password_' + LPad(IntToStr(i), 3,
        '0'), '');

      IdleTime := Ini.ReadInteger('Users', 'IdleTime_' + LPad(IntToStr(i),
        3, '0'), 0);

      if UserID > 0 then
        FUserList.AddUser(UserID, User, Password, IdleTime);
    end;

    n := Ini.ReadInteger('Program URLs', 'Count', 0);
    for i := 1 to n do
    begin
      UserID := Ini.ReadInteger('Program URLs', 'UserID_' + LPad(IntToStr(i),
        3, '0'), 0);
      Tab := Ini.ReadString('Program URLs', 'TabCaption_' + LPad(IntToStr(i), 3,
        '0'), '');
      StrKind := Ini.ReadString('Program URLs', 'Kind_' + LPad(IntToStr(i), 3,
        '0'), '');

      url := Ini.ReadString('Program URLs', 'URL_Prog_' + LPad(IntToStr(i), 3,
        '0'), '');
      Params := Ini.ReadString('Program URLs', 'Params_' + LPad(IntToStr(i), 3,
        '0'), '');
      WaitMS := Ini.ReadInteger('Program URLs', 'WaitMS_' + LPad(IntToStr(i),
        3, '0'), 0);
      HideFrames := Ini.ReadBool('Program URLs',
        'HideFrames_' + LPad(IntToStr(i), 3, '0'), True);
      LoadAtStartup := Ini.ReadBool('Program URLs',
        'LoadAtStartup_' + LPad(IntToStr(i), 3, '0'), False);
      AutoKill := Ini.ReadBool('Program URLs', 'AutoKill_' + LPad(IntToStr(i),
        3, '0'), False);
      AllowOpenDlg := Ini.ReadString('Program URLs',
        'AllowOpenDlg_' + LPad(IntToStr(i), 3, '0'), '');
      Dir := Ini.ReadString('Program URLs', 'Dir_' + LPad(IntToStr(i), 3,
        '0'), '');
      TabUser := Ini.ReadString('Program URLs', 'TabUser_' + LPad(IntToStr(i),
        3, '0'), '');
      TabPassword := Ini.ReadString('Program URLs',
        'TabPassword_' + LPad(IntToStr(i), 3, '0'), '');

      nb := nbNull;
      { //removed by yona
        if AllowOpenDlg <> '' then
        begin
        //        FSetup.TabURLs.FieldByName('AllowOpenDlg').AsBoolean  := AllowOpenDlg = '1'
        end
        else
      }

      if AllowOpenDlg = '1' then
        nb := nbTrue
      else
        nb := nbFalse;

      Kind := ttNone;
      if StrKind = 'W' then
        Kind := ttURL
      else
        Kind := ttProgram;

{$REGION 'Commands'}
      var
        cmdIdx: integer := 1;
      var
        cmdStr: string;
      var
        cmdkey: string;
      var
        cmdList: TList<TCommandInfo>;

      cmdList := TList<TCommandInfo>.Create;

      while cmdStr <> 'No More' do
      begin

        cmdkey := 'command_' + LPad(IntToStr(i), 3, '0') + '[' +
          cmdIdx.tostring + ']';
        cmdStr := Ini.ReadString('Program URLs', cmdkey, 'No More');
        if cmdStr <> 'No More' then
        begin
          var
            ComInfo: TCommandInfo;
          ComInfo.FromString(cmdStr);
          cmdList.Add(ComInfo);
        end;
        Inc(cmdIdx);
      end;

{$ENDREGION}
      Idx := FUserList.IndexOf(UserID);
      u := FUserList.Items[Idx];
      var
        aTab: TTabItem;
      aTab := u.Tabs.AddTab(Tab, Kind, url, Params, WaitMS, HideFrames,
        LoadAtStartup, AutoKill, Dir, nb, TabUser, TabPassword, cmdList);

    end;

    n := Ini.ReadInteger('Global Parameters', 'Count', 0);
    for i := 1 to n do
    begin
      UserID := Ini.ReadInteger('Global Parameters',
        'UserID_' + LPad(IntToStr(i), 3, '0'), 0);
      Key := Ini.ReadString('Global Parameters', 'Key_' + LPad(IntToStr(i), 3,
        '0'), '');
      Value := Ini.ReadString('Global Parameters', 'Value_' + LPad(IntToStr(i),
        3, '0'), '');

      Idx := FUserList.IndexOf(UserID);
      u := FUserList.Items[Idx];
      u.Params.AddKey(Key, Value);
    end;
  finally
    Ini.Free;

    FLoading := False;
    FSetup.FUserList.Assign(FUserList);
  end;
end;

procedure TMainForm.SaveIniFile;
var
  Ini: TMemoryIniStructure;
  Lst: TStringList;
  FileName: string;
  s: WideString;
  i, j, t, p: integer;
{$IFDEF Encrypt}
  Rijndael: TDCP_rijndael;
{$ENDIF}
begin

  FileName := GetIniFileName;
  Ini := TMemoryIniStructure.Create;

  try
    if (FSetup.edExpirationDate.Text = '') or (FSetup.edExpirationDate.Tag = 1)
    then

      Ini.WriteString('Application', 'ExpirationDate', '')
    else
      Ini.WriteString('Application', 'ExpirationDate',
        FormatDateTime('mm/dd/yyyy', FSetup.edExpirationDate.Date));

    Ini.WriteString('Application', 'StoreName', FSetup.edStoreName.Text);
    Ini.WriteString('Application', 'UserPassword', FSetup.edPassword.Text);
    Ini.WriteString('Application', 'DefaultDownloadPath',
      FSetup.edDefaultDownloadPath.Text);

    Ini.WriteBool('Application', 'RemoveBar', FSetup.cbRemoveBar.Checked);
    Ini.WriteBool('Application', 'AllowOpenDlg', FSetup.cbAllowOpenDlg.Checked);

    s := GetEnumName(TypeInfo(TcxTabPosition),
      integer(PageControlApp.Properties.TabPosition));
    s := Copy(s, 3, Length(s));

    case FSetup.rgTabPosition.ItemIndex of
      0:
        s := 'Top';
      1:
        s := 'Left';
      2:
        s := 'Right';
      3:
        s := 'Bottom';

    end;

    Ini.WriteString('Look and Feel', 'TabPosition', s);
    Ini.WriteString('Look and Feel', 'Skin', FSetup.cbSkinName.Text);

    Ini.WriteInteger('Users', 'Count', FUserList.Count);
    Ini.WriteInteger('Users', 'NextUserID', FSetup.FNextUserID);
    Ini.WriteInteger('Program URLs', 'Count', FUserList.TabCount);
    Ini.WriteInteger('Global Parameters', 'Count', FUserList.ParamCount);
    t := 1;
    p := 1;
    for i := 0 to FUserList.Count - 1 do
    begin
      Ini.WriteInteger('Users', 'UserID_' + LPad(IntToStr(i + 1), 3, '0'),
        FUserList.Items[i].UserID);
      Ini.WriteString('Users', 'UserName_' + LPad(IntToStr(i + 1), 3, '0'),
        FUserList.Items[i].UserName);
      Ini.WriteString('Users', 'Password_' + LPad(IntToStr(i + 1), 3, '0'),
        FUserList.Items[i].Password);
      Ini.WriteInteger('Users', 'IdleTime_' + LPad(IntToStr(i + 1), 3, '0'),
        FUserList.Items[i].IdleTime);

      for j := 0 to FUserList.Items[i].Tabs.Count - 1 do
      begin
        Ini.WriteInteger('Program URLs', 'UserID_' + LPad(IntToStr(t), 3, '0'),
          FUserList.Items[i].UserID);
        Ini.WriteString('Program URLs', 'TabCaption_' + LPad(IntToStr(t), 3,
          '0'), FUserList.Items[i].Tabs.Items[j].TabCaption);
        Ini.WriteString('Program URLs', 'URL_Prog_' + LPad(IntToStr(t), 3, '0'),
          FUserList.Items[i].Tabs.Items[j].URLProgram);
        Ini.WriteString('Program URLs', 'Params_' + LPad(IntToStr(t), 3, '0'),
          FUserList.Items[i].Tabs.Items[j].Params);
        Ini.WriteInteger('Program URLs', 'WaitMS_' + LPad(IntToStr(t), 3, '0'),
          FUserList.Items[i].Tabs.Items[j].WaitMS);
        Ini.WriteBool('Program URLs', 'HideFrames_' + LPad(IntToStr(t), 3, '0'),
          FUserList.Items[i].Tabs.Items[j].HideFrames);
        Ini.WriteBool('Program URLs', 'LoadAtStartup_' + LPad(IntToStr(t), 3,
          '0'), FUserList.Items[i].Tabs.Items[j].LoadAtStartup);
        Ini.WriteBool('Program URLs', 'AutoKill_' + LPad(IntToStr(t), 3, '0'),
          FUserList.Items[i].Tabs.Items[j].AutoKill);
        Ini.WriteString('Program URLs', 'Dir_' + LPad(IntToStr(t), 3, '0'),
          FUserList.Items[i].Tabs.Items[j].DownloadPath);
        Ini.WriteString('Program URLs', 'TabUser_' + LPad(IntToStr(t), 3, '0'),
          FUserList.Items[i].Tabs.Items[j].TabUser);
        Ini.WriteString('Program URLs', 'TabPassword_' + LPad(IntToStr(t), 3,
          '0'), FUserList.Items[i].Tabs.Items[j].TabPassword);

        case FUserList.Items[i].Tabs.Items[j].Kind of
          ttURL:
            Ini.WriteString('Program URLs', 'Kind_' + LPad(IntToStr(t), 3,
              '0'), 'W');
          ttProgram:
            Ini.WriteString('Program URLs', 'Kind_' + LPad(IntToStr(t), 3,
              '0'), 'P');
        end;

        s := '';
        case FUserList.Items[i].Tabs.Items[j].AllowOpenDialog of
          nbFalse:
            s := '0';
          nbTrue:
            s := '1'
        end;

        Ini.WriteString('Program URLs', 'AllowOpenDlg_' + LPad(IntToStr(t),
          3, '0'), s);

{$REGION 'Write Commands'}
        var
          cmdIdx: integer := 1;
        var
          cmdInfo: TCommandInfo;
        for cmdInfo in FUserList.Items[i].Tabs.Items[j].CommandList do
        begin
          try
            Ini.WriteString('Program URLs', 'Command_' + LPad(IntToStr(t), 3,
              '0') + '[' + cmdIdx.tostring + ']', cmdInfo.ToIniString);
          finally

          end;
          Inc(cmdIdx);
        end;
{$ENDREGION}
        Inc(t);
      end;

      for j := 0 to FUserList.Items[i].Params.Count - 1 do
      begin
        Ini.WriteInteger('Global Parameters', 'UserID_' + LPad(IntToStr(p), 3,
          '0'), FUserList.Items[i].UserID);
        Ini.WriteString('Global Parameters', 'Key_' + LPad(IntToStr(p), 3, '0'),
          FUserList.Items[i].Params.Items[j].Key);
        Ini.WriteString('Global Parameters', 'Value_' + LPad(IntToStr(p), 3,
          '0'), FUserList.Items[i].Params.Items[j].Value);

        Inc(p)
      end;
    end;
  finally
    Lst := Ini.SaveToStringList;
    s := Lst.Text;
{$IFDEF Encrypt}
    // Rijndael := TDCP_rijndael.Create(Self);
    // Rijndael.InitStr(strEncryptKey, TDCP_sha256);
    // s := Rijndael.EncryptString(s);
    // Rijndael.Free;
{$ENDIF}
    TFile.WriteAllText(FileName, s);
    Ini.Free;
    Lst.Free;
  end;

end;

procedure TMainForm.DefaultSetup;
begin
  FSetup.edStoreName.Text := '';
  FSetup.edExpirationDate.Date := Now;
  FSetup.edExpirationDate.Update;
  FSetup.edPassword.Text := '';
  FSetup.edPasswordValidation.Text := '';
  FSetup.rgTabPosition.ItemIndex := 0;
  FSetup.cbRemoveBar.Checked := True;
  FSetup.TabURLs.Close;
  FSetup.TabURLs.Open;
  FSetup.TabParams.Close;
  FSetup.TabParams.Open;
end;

procedure TMainForm.ValidateExpirationDate;
begin
  if FSetup.edExpirationDate.Date >= Trunc(Now) then
  begin
    MessageDlg('Program has expired, please contact to System Administrator',
      mtError, [mbOK], 0);

    ClearEnvironment;
  end;
end;

procedure TMainForm.ForwardBtnClick(Sender: TObject);
begin
  GoForward;
end;

procedure TMainForm.GoBtnClick(Sender: TObject);
begin
  GoURL(URLCbx.Text);
end;

procedure TMainForm.ReloadBtnClick(Sender: TObject);
begin
  Reload;
end;

procedure TMainForm.BackBtnClick(Sender: TObject);
begin
  GoBack;
end;

procedure TMainForm.StopBtnClick(Sender: TObject);
begin
  Stop;
end;

procedure TMainForm.TimerUpdateDownloadTimer(Sender: TObject);
var
  i: integer;
  Info: PDownloadInfo;
  s: string;
begin
  if FClosing then
  begin
    TimerUpdateDownload.Enabled := False;
    exit;
  end;

  for i := 0 to LstDownload.Count - 1 do
  begin
    Info := PDownloadInfo(LstDownload[i]);
    if (Info^.State in [dsInProgress, dsTerminated]) then
    begin
      if not Assigned(Info^.Btn) then
      begin
        Info^.Btn := TcxButton.Create(pnlDownloads);
        Info^.Btn.Parent := pnlDownloads;
        Info^.Btn.Visible := False;
        Info^.Btn.Top := 0;
        Info^.Btn.Width := 100;
        Info^.Btn.Left := pnlDownloads.ClientWidth - Info^.Btn.Width;
        Info^.Btn.Align := alRight;
        Info^.Btn.ShowHint := True;
        Info^.Btn.Align := alLeft;
        Info^.Btn.OnClick := DownloadOnClick;
        Info^.Btn.TabStop := False;
        Info^.DropDown := TcxButton.Create(pnlDownloads);
        Info^.DropDown.Parent := pnlDownloads;
        Info^.DropDown.Visible := False;
        Info^.DropDown.Top := 0;
        Info^.DropDown.Width := 24;
        Info^.DropDown.Left := pnlDownloads.ClientWidth - Info^.Btn.Width;
        Info^.DropDown.Align := alRight;
        Info^.DropDown.ShowHint := True;
        Info^.DropDown.Align := alLeft;
        Info^.DropDown.OnClick := DownloadOnDropDown;
        Info^.DropDown.OptionsImage.Images := ImageList1;
        Info^.DropDown.OptionsImage.ImageIndex := 5;
        Info^.DropDown.TabStop := False;
        Info^.Btn.Visible := True;
        Info^.DropDown.Visible := True;
        pnlDownloads.Visible := True;
      end;

      if not Info^.FullPath.IsEmpty then
        s := Info^.FullPath
      else
        s := Info^.SuggestedName;
      Info^.Btn.Hint := s;

      if Info^.State = dsInProgress then
      begin
        Info^.Btn.Caption := LimitedString(ExtractFileName(s), Info^.Btn.Font,
          Info^.Btn.Width - 20) + sLineBreak +

          IntToStr(Math.Max(0, Info^.Pct)) + '%';
      end
      else if Info^.State = dsTerminated then
        Info^.Btn.Caption := LimitedString(ExtractFileName(s), Info^.Btn.Font,
          Info^.Btn.Width - 20)

    end
    else if Info^.State = dsCanceled then
    begin
      if Assigned(Info^.Btn) then
        FreeAndNil(Info^.Btn);
      if Assigned(Info^.DropDown) then
        FreeAndNil(Info^.DropDown);

      LstDownload[i] := nil;
      LstDownload.Delete(i);
      Dispose(Info);
      CheckDownloadPanelVisible;
      exit;
    end;
  end;
end;

procedure TMainForm.TimerValidateExpirationDateTimer(Sender: TObject);
begin
  if FileExists(GetIniFileName) then
  begin
    if ProgramExpired then
      TimerValidateExpirationDate.Enabled := False;
  end;
end;

procedure TMainForm.BrowserCreatedMsg(var aMessage: TMessage);
var
  Tab: TBrowserTabSheet;
  s: string;
begin
  if Math.InRange(aMessage.lParam, 0, PageControlApp.PageCount - 1) then
  begin
    if Assigned(TBrowserTabSheet(PageControlApp.Pages[aMessage.lParam])
      .ParentCEFWin) then
      TBrowserTabSheet(PageControlApp.Pages[aMessage.lParam])
        .ParentCEFWin.UpdateSize;

    if Assigned(TBrowserTabSheet(PageControlApp.Pages[aMessage.lParam]).Chromium)
    then

    begin
      Tab := TBrowserTabSheet(PageControlApp.Pages[aMessage.lParam]);
      if Tab.LoadAtStartup then
      begin
        Tab.ImageIndex := ICO_Yellow;
        s := Tab.ReplaceParams(Tab.URLProgram);
        Tab.Chromium.LoadURL(Tab.URLProgram);
      end;
    end;
  end;
end;

procedure TMainForm.BrowserDestroyWindowParentMsg(var aMessage: TMessage);
begin
  if InRange(aMessage.lParam, 0, PageControlApp.PageCount - 1) and
    Assigned(TBrowserTabSheet(PageControlApp.Pages[aMessage.lParam])
    .ParentCEFWin) then

    FreeAndNil(TBrowserTabSheet(PageControlApp.Pages[aMessage.lParam])
      .FParentCEFWin);
end;

procedure TMainForm.BrowserDestroyTabMsg(var aMessage: TMessage);
begin
  if (aMessage.lParam >= 0) and (aMessage.lParam < PageControlApp.PageCount)
  then

    PageControlApp.Pages[aMessage.lParam].Free;

  FClosingTab := False;
end;

procedure TMainForm.BrowserCheckTaggedTabsMsg(var aMessage: TMessage);
begin
  if (aMessage.lParam >= 0) and (aMessage.lParam < PageControlApp.PageCount)
  then

  begin
    PageControlApp.Pages[aMessage.lParam].Tag := 1;

    if AllTabSheetsAreTagged then
    begin
      FCanClose := True;
      SendMessage(Handle, WM_CLOSE, 0, 0);
    end;
  end;
end;

procedure TMainForm.BrowserUpdateDownload(var aMessage: TMessage);
var
  NewInfo, LstInfo: PDownloadInfo;
begin
  if FClosing then
    exit;

  NewInfo := PDownloadInfo(aMessage.WParam);
  LstInfo := FindByTabAndID(NewInfo^.PageIndex, NewInfo^.ID);

  if not Assigned(LstInfo) then
  begin
    LstDownload.Add(NewInfo);
  end
  else
  begin
    LstInfo^.SuggestedName := NewInfo^.SuggestedName;
    LstInfo^.FullPath := NewInfo^.FullPath;
    LstInfo^.State := NewInfo^.State;
    LstInfo^.Pct := NewInfo^.Pct;

    Dispose(NewInfo);
  end;
end;

procedure TMainForm.btnCloseClick(Sender: TObject);
var
  i: integer;
  Info: PDownloadInfo;
begin
  pnlDownloads.Visible := False;

  for i := LstDownload.Count - 1 downto 0 do
  begin
    Info := PDownloadInfo(LstDownload[i]);
    if Info^.State = dsTerminated then
    begin
      if Assigned(Info^.Btn) then
        FreeAndNil(Info^.Btn);
      if Assigned(Info^.DropDown) then
        FreeAndNil(Info^.DropDown);

      LstDownload[i] := nil;
      LstDownload.Delete(i);
      Dispose(Info);
    end;
  end;
end;

function TMainForm.AllTabSheetsAreTagged: boolean;
var
  i: integer;
  Tab: TBrowserTabSheet;
begin
  Result := True;

  // for i := 0 to PageControlApp.PageCount - 1 do
  i := pred(PageControlApp.PageCount);
  while (i >= 0) and Result do
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[i]);
    if (Tab.Kind = ttURL) and (Tab.Tag <> 1) then
    begin
      Result := False;
      // exit;
    end
    else
      dec(i);

  end;
end;

procedure TMainForm.AppEventsMessage(var Msg: tagMSG; var Handled: boolean);
begin
  case Msg.Message of
    WM_KEYFIRST .. WM_KEYLAST, WM_MOUSEFIRST .. WM_MOUSELAST:
      FIdleCounter := 0;
  end;
end;

procedure TMainForm.Chromium_OnAfterCreated(Sender: TObject;
  const browser: ICefBrowser);

var
  TempPageIndex: integer;
begin
  if GetPageIndex(Sender, TempPageIndex) then
    PostMessage(Handle, CEF_AFTERCREATED, 0, TempPageIndex);
end;

procedure TMainForm.Chromium_OnAddressChange(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame; const url: ustring);

var
  TempPageIndex: integer;
begin
  if not(FClosing) and (PageControlApp.ActivePageIndex >= 0) and

    GetPageIndex(Sender, TempPageIndex) and
    (PageControlApp.ActivePageIndex = TempPageIndex) then
    URLCbx.Text := url;
end;

function TMainForm.GetPageIndex(const aSender: TObject;
  var aPageIndex: integer): boolean;
begin
  Result := False;
  aPageIndex := -1;

  if Assigned(aSender) and (aSender is TComponent) and
    Assigned(TComponent(aSender).Owner) and
    (TComponent(aSender).Owner is TBrowserTabSheet) then
  begin
    aPageIndex := TBrowserTabSheet(TComponent(aSender).Owner).PageIndex;
    Result := True;
  end;
end;

procedure TMainForm.Chromium_OnTitleChange(Sender: TObject;
  const browser: ICefBrowser; const Title: ustring);
var
  TempPageIndex: integer;
begin
  if not(FClosing) and GetPageIndex(Sender, TempPageIndex) then
    PageControlApp.Pages[TempPageIndex].Caption := Title;
end;

procedure TMainForm.Chromium_OnClose(Sender: TObject;
  const browser: ICefBrowser; var aAction: TCefCloseBrowserAction);

var
  TempPageIndex: integer;
begin

  if GetPageIndex(Sender, TempPageIndex) then
  begin
    if FClosing then
    begin

      SendMessage(Handle, CEFBROWSER_DESTROYWNDPARENT, 0, TempPageIndex);
    end;

  end;

end;

procedure TMainForm.Chromium_OnBeforeClose(Sender: TObject;
  const browser: ICefBrowser);

var
  TempPageIndex: integer;
begin
  try
    if GetPageIndex(Sender, TempPageIndex) then
    begin
      if FClosing then
        SendMessage(Handle, CEFBROWSER_CHECKTAGGEDTABS, 0, TempPageIndex)
      else
        SendMessage(Handle, CEFBROWSER_DESTROYTAB, 0, TempPageIndex);
    end;
  finally

  end;
end;

procedure TMainForm.Chromium_OnBeforePopup(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame;
  const targetUrl, targetFrameName: ustring;
  targetDisposition: TCefWindowOpenDisposition; userGesture: boolean;

  const popupFeatures: TCefPopupFeatures; var windowInfo: TCefWindowInfo;
  var client: ICefClient; var settings: TCefBrowserSettings;

  var extra_info: ICefDictionaryValue; var noJavascriptAccess: boolean;

  var Result: boolean);
begin
  // For simplicity, this demo blocks all popup windows and new tabs
  Result := (targetDisposition in [WOD_NEW_FOREGROUND_TAB,
    WOD_NEW_BACKGROUND_TAB, WOD_NEW_POPUP, WOD_NEW_WINDOW]);

end;

procedure TMainForm.Chromium_LoadEnd(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame; httpStatusCode: integer);
var
  TempPageIndex: integer;
  Tab: TBrowserTabSheet;
begin
  if GetPageIndex(Sender, TempPageIndex) then
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[TempPageIndex]);

    if Tab.LoadAtStartup then
      Tab.ImageIndex := ICO_Green;
  end;
end;

procedure TMainForm.Chromium_LoadError(Sender: TObject;
  const browser: ICefBrowser; const frame: ICefFrame; errorCode: integer;
  const errorText, failedUrl: ustring);
var
  Idx: integer;
  Tab: TBrowserTabSheet;
begin
  if GetPageIndex(Sender, Idx) then
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[Idx]);
    if Tab.Kind = ttURL then
    begin
      Tab.ImageIndex := ICO_Red;
      Memo1.Lines.Add(Format('Page: %d, Caption: %s, ErrorCode: %d',
        [Idx, Tab.Caption, errorCode]));
    end;
  end;
end;

procedure TMainForm.Chromium_BeforeDownlad(Sender: TObject;
  const browser: ICefBrowser; const downloadItem: ICefDownloadItem;
  const SuggestedName: ustring; const callback: ICefBeforeDownloadCallback);
var
  Idx: integer;
  Tab: TBrowserTabSheet;
  Dir: string;
  AllowOpenDlg: boolean;
begin

  Tab := TBrowserTabSheet(PageControlApp.ActivePage);
  Dir := Tab.Directory;
  if Dir = '' then
    Dir := FSetup.edDefaultDownloadPath.Text;

  AllowOpenDlg := FSetup.cbAllowOpenDlg.Checked;
  if Tab.AllowOpenDialog <> nbNull then
    AllowOpenDlg := Tab.AllowOpenDialog = nbTrue;

  callback.Cont(Dir + '\' + SuggestedName, AllowOpenDlg);
end;

function TMainForm.FindByTabAndID(TabIndex, ID: integer): PDownloadInfo;
var
  i: integer;
  p: PDownloadInfo;
begin
  Result := nil;

  for i := 0 to LstDownload.Count - 1 do
  begin
    p := PDownloadInfo(LstDownload[i]);
    if (p^.PageIndex = TabIndex) and (p^.ID = ID) then
    begin
      Result := p;

      exit;
    end;
  end;
end;

function TMainForm.FindByButton(Btn: TcxButton): PDownloadInfo;
var
  i: integer;
  p: PDownloadInfo;
begin
  Result := nil;

  for i := 0 to LstDownload.Count - 1 do
  begin
    p := PDownloadInfo(LstDownload[i]);
    if (p^.Btn = Btn) or (p^.DropDown = Btn) then
    begin
      Result := p;

      exit;
    end;
  end;
end;

procedure TMainForm.ClearDownloadList;
var
  p: PDownloadInfo;
begin
  while LstDownload.Count > 0 do
  begin
    p := PDownloadInfo(LstDownload[0]);
    LstDownload[0] := nil;
    LstDownload.Delete(0);

    if Assigned(p^.Btn) then
      p^.Btn.Free;
    if Assigned(p^.DropDown) then
      p^.DropDown.Free;

    Dispose(p);
  end;
end;

function TMainForm.LimitedString(s: string; Fnt: TFont; Size: integer): string;
var
  Bmp: TBitmap;
begin
  Bmp := TBitmap.Create;
  try
    Bmp.Canvas.Font.Assign(Fnt);

    if Bmp.Canvas.TextWidth(s) <= Size then
    begin
      Result := s;

      exit;
    end;

    repeat
      Delete(s, Length(s), 1);
      Result := s + '...';
    until Bmp.Canvas.TextWidth(Result) <= Size;
  finally
    Bmp.Free;
  end;
end;

procedure TMainForm.DownloadOnClick(Sender: TObject);
var
  Info: PDownloadInfo;
begin
  Info := FindByButton(Sender as TcxButton);
  if Assigned(Info) and (Info^.State = dsTerminated) then
    ShellExecute(Self.WindowHandle, 'open', PChar(Info^.FullPath), nil, nil,
      SW_SHOWNORMAL)

end;

procedure TMainForm.DownloadOnDropDown(Sender: TObject);
var
  Info: PDownloadInfo;
  Btn: TcxButton;
  X, Y: integer;
begin
  if not(Sender is TcxButton) then
    exit;

  Btn := Sender as TcxButton;
  Info := FindByButton(Btn);
  if Assigned(Info) then
  begin
    actDownloadOpen.Enabled := Info^.State = dsTerminated;
    actDownloadShowInFolder.Enabled := Info^.State = dsTerminated;
    FLastDownload := Info;
    GetAbsolutePos(Btn, X, Y);
    pmDownload.Popup(X, Y);
  end;
end;

procedure TMainForm.GetAbsolutePos(Ctrl: TControl; var X, Y: integer);

  procedure AbsolutePos(Ctrl: TControl; var X, Y: integer);
  begin
    Inc(X, Ctrl.Left);
    Inc(Y, Ctrl.Top);
    if Ctrl = Self then
      exit;

    AbsolutePos(Ctrl.Parent, X, Y);
  end;

begin
  X := 0;
  Y := 0;
  AbsolutePos(Ctrl, X, Y);
end;

procedure TMainForm.CheckDownloadPanelVisible;
var
  i: integer;
begin
  for i := 0 to LstDownload.Count - 1 do
    if PDownloadInfo(LstDownload[i])^.State in [dsInProgress, dsTerminated] then
      exit;

  pnlDownloads.Visible := False;
end;

procedure TMainForm.Chromium_DownloadUpdated(Sender: TObject;
  const browser: ICefBrowser; const downloadItem: ICefDownloadItem;
  const callback: ICefDownloadItemCallback);

var
  Idx: integer;
  Tab: TBrowserTabSheet;
  Info: PDownloadInfo;
  aMessage: TMessage;
begin
  Info := nil;
  Tab := nil;
  if GetPageIndex(Sender, Idx) then
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[Idx]);
    Tab.ImageIndex := ICO_Green;
    New(Info);
    Info^.Btn := nil;
    Info^.DropDown := nil;
    Info^.ID := downloadItem.ID;
    Info^.PageIndex := Tab.PageIndex;
    Info^.FullPath := downloadItem.FullPath;
    Info^.State := dsNone;
    Info^.Pct := downloadItem.PercentComplete;
    Info^.SuggestedName := downloadItem.SuggestedFileName;
  end;

  if Assigned(Info) then
  begin
    if downloadItem.IsInProgress then
      Info^.State := dsInProgress
    else if downloadItem.IsComplete then
      Info^.State := dsTerminated
    else if downloadItem.IsCanceled then
      Info^.State := dsCanceled;

    PostMessage(Self.Handle, CEFBROWSER_UPDATEDOWNLOAD, integer(Info), 0);
  end;
end;

procedure TMainForm.NotifyMoveOrResizeStarted;
var
  i, j: integer;
  Tab: TBrowserTabSheet;
begin
  if not(showing) or not Assigned(PageControlApp) or FClosing then
    exit;

  i := 0;
  j := PageControlApp.PageCount;

  while (i < j) do
  begin
    if Math.InRange(i, 0, PageControlApp.PageCount - 1) then
    begin
      Tab := TBrowserTabSheet(PageControlApp.Pages[i]);
      if (Tab.Kind = ttURL) and Assigned(Tab.Chromium) then
        Tab.Chromium.NotifyMoveOrResizeStarted;
    end;

    Inc(i);
  end;
end;

procedure TMainForm.WMMove(var aMessage: TWMMove);
begin
  inherited;

  NotifyMoveOrResizeStarted;
end;

procedure TMainForm.WMMoving(var aMessage: TMessage);
begin
  inherited;

  NotifyMoveOrResizeStarted;
end;

procedure TMainForm.WMEnterMenuLoop(var aMessage: TMessage);
begin
  inherited;

  if not(FClosing) and (aMessage.WParam = 0) and Assigned(GlobalCEFApp) then
    GlobalCEFApp.OsmodalLoop := True;
end;

procedure TMainForm.WMExitMenuLoop(var aMessage: TMessage);
begin
  inherited;

  if not(FClosing) and (aMessage.WParam = 0) and Assigned(GlobalCEFApp) then
    GlobalCEFApp.OsmodalLoop := False;
end;

procedure TMainForm.PageControlAppChange(Sender: TObject);
var
  Tab: TBrowserTabSheet;
  WinInfo: TWindowInfo;
  ApWin: ApWinInfo;
  CurrStyle: NativeInt;
  L: TList;
  i: integer;
begin
  if showing and Assigned(PageControlApp.ActivePage) then
  begin
    Tab := TBrowserTabSheet(PageControlApp.ActivePage);
    if Assigned(Tab.Chromium) then
      URLCbx.Text := Tab.Chromium.DocumentURL;

    Caption := FSetup.edStoreName.Text + ' - ' + Tab.Caption;
    if (Tab.FKind = ttProgram) and Tab.ProgramStillRunning then
    begin
      SetForegroundWindow(Tab.AppWnd);
      if not FClosing and (Tab.FQueue.Count > 0) then
      begin

        ApWin := nil;
        L := TList(Tab.FQueue[0]);
        for i := L.Count - 1 downto 0 do
        begin
          ApWin := ApWinInfo(L[i]);
          FillChar(WinInfo, Sizeof(WinInfo), 0);
          WinInfo.cbSize := Sizeof(WinInfo);
          GetWindowInfo(ApWin^.Handle, WinInfo);

          // If properties are different, try to restore
          if ApWin^.dwExStyle <> WinInfo.dwExStyle then
          begin
            // Get current app style
            CurrStyle := Winapi.Windows.GetWindowLong(ApWin^.Handle,
              GWL_EXSTYLE);

            // Verify for missing styles
            if ((ApWin^.dwExStyle and WS_EX_CLIENTEDGE) = WS_EX_CLIENTEDGE) and
              ((CurrStyle and WS_EX_CLIENTEDGE) <> WS_EX_CLIENTEDGE) then
              CurrStyle := CurrStyle or WS_EX_CLIENTEDGE;

            if ((ApWin^.dwExStyle and WS_EX_NOPARENTNOTIFY)
              = WS_EX_NOPARENTNOTIFY) and
              ((CurrStyle and WS_EX_NOPARENTNOTIFY) <> WS_EX_NOPARENTNOTIFY)
            then

              CurrStyle := CurrStyle or WS_EX_NOPARENTNOTIFY;

            if ((ApWin^.dwExStyle and WS_EX_TOOLWINDOW) = WS_EX_TOOLWINDOW) and
              ((CurrStyle and WS_EX_TOOLWINDOW) <> WS_EX_TOOLWINDOW) then
              CurrStyle := CurrStyle or WS_EX_TOOLWINDOW;

            // Restore original style
            Winapi.Windows.SetWindowLong(ApWin^.Handle, GWL_EXSTYLE, CurrStyle);
          end;

          // Restore Window
          ShowWindow(ApWin^.Handle, SW_SHOW);
          SetForegroundWindow(ApWin^.Handle);
          Winapi.Windows.SetFocus(ApWin^.Handle);
        end;
      end;
      Tab.ClearQueue;
    end;
  end;
  FChangingTab := False;
end;

procedure TMainForm.PageControlAppContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: boolean);
var
  Tab: TBrowserTabSheet;
begin
  actCloseApp.Tag := PageControlApp.IndexOfTabAt(MousePos.X, MousePos.Y);
  Handled := not Math.InRange(actCloseApp.Tag, 0, PageControlApp.PageCount - 1);

  if not Handled then
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[actCloseApp.Tag]);
    actRefreshPage.Enabled := (Tab.Kind = ttURL) or
      ((Tab.Kind = ttProgram) and not Tab.ProgramStillRunning);
    actCloseApp.Enabled := (Tab.Kind = ttProgram) and Tab.AutoKill and
      Tab.ProgramStillRunning;

    // actTabProperties.Enabled := Tab.Kind = ttURL;
  end;
end;

procedure TMainForm.PageControlAppPageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: boolean);
var
  Tab: TBrowserTabSheet;
  CurrStyle: NativeInt;
  ApWin: ApWinInfo;
  L: TList;
  i: integer;
begin
  FChangingTab := True;
  if Assigned(PageControlApp.ActivePage) then
  begin
    Tab := TBrowserTabSheet(PageControlApp.ActivePage);
    if (Tab.Kind = ttProgram) and (Tab.ProgramStillRunning) then
    begin
      SendMessage(Tab.AppWnd, WM_ACTIVATE, WA_INACTIVE, 0);
      SendMessage(Tab.AppWnd, WM_ACTIVATEAPP, 0, 0);
      SendMessage(Tab.AppWnd, WM_NCACTIVATE, 0, 0);

      if not FClosing and (Tab.FQueue.Count > 0) then
      begin
        L := TList(Tab.FQueue[0]);
        ApWin := nil;

        for i := 0 to L.Count - 1 do
        begin
          ApWin := ApWinInfo(L[i]);
          if (ApWin^.Handle <> Self.Handle) and
            (ApWin^.Handle <> PageControlApp.Handle) and

            (ApWin^.Handle <> Tab.AppWnd) then
          begin
            // Get current app style
            CurrStyle := Winapi.Windows.GetWindowLong(ApWin^.Handle,
              GWL_EXSTYLE);

            // Try to remove features that don't hide the window
            if ((ApWin^.dwExStyle and WS_EX_CLIENTEDGE) = WS_EX_CLIENTEDGE) then
              CurrStyle := CurrStyle and not WS_EX_CLIENTEDGE;

            if ((ApWin^.dwExStyle and WS_EX_NOPARENTNOTIFY)
              = WS_EX_NOPARENTNOTIFY) then

              CurrStyle := CurrStyle and not WS_EX_NOPARENTNOTIFY;

            if ((ApWin^.dwExStyle and WS_EX_TOOLWINDOW) = WS_EX_TOOLWINDOW) then
              CurrStyle := CurrStyle and not WS_EX_TOOLWINDOW;

            // Apply new window style
            Winapi.Windows.SetWindowLong(ApWin^.Handle, GWL_EXSTYLE, CurrStyle);

            // Hide previous window
            ShowWindow(ApWin^.Handle, SW_HIDE);
          end;
        end;
      end;
    end;
  end;
end;

procedure TMainForm.
  PageControlAppTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click
  (Sender: TObject);

begin
  if Assigned(PageControlApp.ActivePage) then
    TBrowserTabSheet(PageControlApp.ActivePage).BtnPlay.Click;
end;

procedure TMainForm.PopTabCommandOnClick(Sender: TObject);
begin
  TMenuItemCmd(Sender).Execute;
end;

procedure TMainForm.popTabPopup(Sender: TObject);
var
  Tab: TBrowserTabSheet;
begin
  { codesite.EnterMethod('popTabPopup'); }
  for var i: integer := popTab.Items.Count - 1 downto 0 do
  begin
    if popTab.Items[i].Tag = 999 then
      popTab.Items[i].Free;
  end;

  var
    mitem: TMenuItemCmd;

  if Math.InRange(actCloseApp.Tag, 0, PageControlApp.PageCount - 1) then
  begin
    Tab := TBrowserTabSheet(PageControlApp.Pages[actCloseApp.Tag]);
    if (Tab.Kind = ttURL) or ((Tab.Kind = ttProgram) and
      not Tab.ProgramStillRunning) then
    begin
      var
        cmdInfo: TCommandInfo;
      for cmdInfo in Tab.CommandList do
      begin
        mitem := TMenuItemCmd.Create(Self);
        mitem.CommandInfo := cmdInfo;
        mitem.Caption := cmdInfo.Caption;
        mitem.Tag := 999;
        mitem.OnClick := PopTabCommandOnClick;
        popTab.Items.Add(mitem);
      end;
    end;

  end;

  { var mitem: TMenuItem;
    var cmdInfo: TCommandInfo;
    for cmdInfo in TTabItem(tab).CommandList do
    begin
    mitem := Tmenuitem.Create(Self);
    mitem.Caption := cmdInfo.Caption;
    popTab.Items.Add(mitem);
    end; }

  { codesite.ExitMethod('popTabPopup'); }
{$ENDREGION}
end;

procedure TMainForm.CEFInitializedMsg(var aMessage: TMessage);
begin
  if not(ButtonPnl.Enabled) then
  begin
    ButtonPnl.Enabled := True;
    Caption := 'Tab Browser';
    cursor := crDefault;
    if (PageControlApp.PageCount = 0) then
      AddTabBtn.Click;

  end;
end;

{ TMenuItemCmd }

procedure TMenuItemCmd.Execute;
var
  ext: String;
begin
  ext := lowercase(TPath.GetExtension(CommandInfo.exe));

  // bat file
 // if CompareText(ext, '.bat') = 0 then
    RunCmd;

end;

procedure TMenuItemCmd.RunCmd;
var
  iRes: integer;
  sMsg: String;
  execMode: Integer;
  dir: String;
  Tab: TBrowserTabSheet;
  args: String;

begin
  try
    Tab := TBrowserTabSheet(Mainform.PageControlApp.Pages[MainForm.actCloseApp.Tag]);
    Args := tab.ReplaceParams(CommandInfo.Params);

    if CommandInfo.exe = '' then
    begin
      showmessage('No Program Associated with Button');
      exit;
    end;

    dir := TPath.GetDirectoryName(CommandInfo.exe);

    iRes := -1;
    sMsg := '';
    iRes := ShellExecute(0, 'open', PChar(CommandInfo.exe ), Pchar(Args), pchar(dir), CommandInfo.Mode);

    case iRes of
      0:
        sMsg := 'The operating system is out of memory or resources.';
      2:
        sMsg := 'The specified file was not found';
      3:
        sMsg := 'The specified path was not found.';
      5:
        sMsg := 'Windows 95 only: The operating system denied access to the specified file';
      8:
        sMsg := 'Windows 95 only: There was not enough memory to complete the operation.';
      10:
        sMsg := 'Wrong Windows version';
      11:
        sMsg := 'The .EXE file is invalid (non-Win32 .EXE or error in .EXE image)';
      12:
        sMsg := 'Application was designed for a different operating system';
      13:
        sMsg := 'Application was designed for MS-DOS 4.0';
      15:
        sMsg := 'Attempt to load a real-mode program';
      16:
        sMsg := 'Attempt to load a second instance of an application with non-readonly data segments.';
      19:
        sMsg := 'Attempt to load a compressed application file.';
      20:
        sMsg := 'Dynamic-link library (DLL) file failure.';
      26:
        sMsg := 'A sharing violation occurred.';
      27:
        sMsg := 'The filename association is incomplete or invalid.';
      28:
        sMsg := 'The DDE transaction could not be completed because the request timed out.';
      29:
        sMsg := 'The DDE transaction failed.';
      30:
        sMsg := 'The DDE transaction could not be completed because other DDE transactions were being processed.';
      31:
        sMsg := 'There is no application associated with the given extension.';
      32:
        sMsg := 'Windows 95 only: The specified dynamic-link library was not found.';
    end;
    if sMsg <> EmptyStr then
      MessageDlg(sMsg, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  except
    on e: Exception do
      MessageDlg(e.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0);
  end;

end;

end.
