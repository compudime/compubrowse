unit uTabParameters;
interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxControls, cxContainer, cxEdit, cxCheckBox,
  cxTextEdit, cxLabel, JvBaseDlg, JvBrowseFolder, uTypes
  {, dxSkinBasic, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkroom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinOffice2019Black, dxSkinOffice2019Colorful,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinValentine, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue
  }
  ;
type
  TFTabProperties = class(TForm)
    btnCancel: TcxButton;
    btnOK: TcxButton;
    labDownloadPath: TcxLabel;
    edDefaultDownloadPath: TcxTextEdit;
    btnDefaultDownloadPath: TcxButton;
    cbAllowOpenDlg: TcxCheckBox;
    FolderDlg: TJvBrowseForFolderDialog;
    cxLabel1: TcxLabel;
    edUser: TcxTextEdit;
    labPassword: TcxLabel;
    edPassword: TcxTextEdit;
    labConfirmation: TcxLabel;
    edConfirmation: TcxTextEdit;
    labPasswordNotMatch: TcxLabel;
    procedure btnDefaultDownloadPathClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FKind: integer;
    procedure SetKind (Value: integer);
  public
    { Public declarations }
    property TabType: integer read FKind write SetKind;
  end;
var
  FTabProperties: TFTabProperties;
implementation
uses uMainForm;
{$R *.dfm}
procedure TFTabProperties.btnDefaultDownloadPathClick(Sender: TObject);
begin
  FolderDlg.Directory := edDefaultDownloadPath.Text;
  if FolderDlg.Execute then
    edDefaultDownloadPath.Text := FolderDlg.Directory;
end;


procedure TFTabProperties.FormShow(Sender: TObject);
begin
  labPasswordNotMatch.Visible := False;
  edConfirmation.Text         := edPassword.Text;
end;
procedure TFTabProperties.SetKind (Value: integer);
begin
  if FKind <> Value then
  begin
    FKind := Value;
    labDownloadPath.Visible        := TTabType (FKind) = ttURL;
    edDefaultDownloadPath.Visible  := TTabType (FKind) = ttURL;
    btnDefaultDownloadPath.Visible := TTabType (FKind) = ttURL;
  end;
end;

end.
