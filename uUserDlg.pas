unit uUserDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit,
  {$IFDEF UseSkins}
  dxSkinsCore, dxSkinBasic,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkroom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinOffice2019Black, dxSkinOffice2019Colorful,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue,
 {$ENDIF}
  cxTextEdit, cxLabel, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.NumberBox, Vcl.ExtCtrls, uPassword, uResetPasswordDlg;

type
  TUserDlgAction = (udaAdd, udaEdit, udaCloneUser);

  TUserDlg = class(TForm)
    cxLabel1: TcxLabel;
    edUserName: TcxTextEdit;
    cxLabel2: TcxLabel;
    nbIdleTime: TNumberBox;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    Panel1: TPanel;
    FramePassword1: TFramePassword;
    btnResetPassword: TcxButton;
    procedure btnOKClick(Sender: TObject);
    procedure btnResetPasswordClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    FUserDlgAction: TUserDlgAction;
    FOldPassword: String;
    FNewPassword: String;
    procedure SetUserAction(const Value: TUserDlgAction);
    { Private declarations }
  public
    constructor Create(AOwner: TComponent;
      aUserAction: TUserDlgAction = udaAdd); overload;
    property UserAction: TUserDlgAction read FUserDlgAction write SetUserAction;
    property OldPassword: String read FOldPassword write FOldPassword;
    property NewPassword: String read FNewPassword write FNewPassword;
  end;

var
  UserDlg: TUserDlg;

implementation

{$R *.dfm}
{ TUserDlg }

procedure TUserDlg.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TUserDlg.btnOKClick(Sender: TObject);
var
  newPw: String;
begin
  ModalResult := mrNone;
  case FUserDlgAction of
    udaAdd:
      begin
        if FramePassword1.Validate(OldPassword, FNewPassword) then
        begin
          ModalResult := mrOK;
        end;
      end;
    udaEdit:
      begin
        ModalResult := mrOK;
      end;
    udaCloneUser:
      begin
        if FramePassword1.Validate('', FNewPassword) then
        begin
          ModalResult := mrOK;
        end;
      end;
  end;
end;

procedure TUserDlg.btnResetPasswordClick(Sender: TObject);
var
  ResetPwDlg: TResetPasswordDlg;
begin
  ResetPwDlg := TResetPasswordDlg.Create(Self);
  ResetPwDlg.OldPassword := OldPassword;
  case FUserDlgAction of
    // udaAdd:
    // begin
    // ResetPwDlg.FramePassword1.pnlOldPassword.Visible := false;
    // end;
    udaEdit:
      begin
        ResetPwDlg.FramePassword1.pnlOldPassword.Visible := True;
      end;
    udaCloneUser:
      begin
      end;
  end;

  if ResetPwDlg.ShowModal = mrOK then
  begin
    NewPassword := ResetPwDlg.NewPassword;
  end;

end;

constructor TUserDlg.Create(AOwner: TComponent; aUserAction: TUserDlgAction);
begin
  inherited Create(AOwner);
  UserAction := aUserAction;
end;

procedure TUserDlg.SetUserAction(const Value: TUserDlgAction);
begin
  FUserDlgAction := Value;
  case FUserDlgAction of
    udaAdd:
      begin
        FramePassword1.pnlOldPassword.Visible := false;
        FramePassword1.Visible := True;
        btnResetPassword.Visible := false;
        Height := 250;
      end;
    udaEdit:
      begin
        FramePassword1.Visible := false;
        btnResetPassword.Visible := True;
        Height := 190;
      end;
    udaCloneUser:
      begin
        FramePassword1.pnlOldPassword.Visible := false;
        FramePassword1.Visible := True;
        btnResetPassword.Visible := false;
        Height := 250;
      end;
  end;
end;

end.
