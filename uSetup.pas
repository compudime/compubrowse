unit uSetup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  System.Generics.Collections, System.TypInfo, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, dxmdaset, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxHyperLinkEdit, Vcl.Menus, cxContainer,
  Vcl.ComCtrls, dxCore, cxDateUtils, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxTextEdit, cxLabel, Vcl.StdCtrls, cxButtons, cxDBNavigator, dxSkinsForm,
  cxGroupBox, cxRadioGroup, cxCheckBox, System.Actions, Vcl.ActnList, cxSpinEdit,
  cxButtonEdit, JvBaseDlg, JvBrowseFolder, dxBarBuiltInMenu, cxPC, cxDBEdit,
  SynEdit, SynDBEdit, SynCompletionProposal, uAddEditUser, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, uTypes, System.ImageList, Vcl.ImgList, dxScrollbarAnnotations,
   {$IFDEF UseSkins}
  dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinBasic, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinOffice2019Black, dxSkinOffice2019Colorful,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinValentine, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue,
  {$ENDIF}
  Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls,
  Vcl.ExtCtrls, cxGridCardView, cxGridDBCardView, cxGridCustomLayoutView,
  dxLayoutContainer, cxGridViewLayoutContainer, cxGridLayoutView,
  cxGridDBLayoutView, cxMRUEdit, Vcl.AppEvnts, dxNumericWheelPicker,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.StorageBin, FireDAC.Comp.DataSet, FireDAC.Comp.Client, uUserDlg;

type
  TUserCollection  = class;
  TTabCollection   = class;
  TUserItem        = class;
  TParamItem       = class;
  TParamCollection = class;

  TTableOperation = (toNone, toEdit, toInsert, toDelete);

  TParamItem = class(TCollectionItem)
  private
    FKey:   string;
    FValue: string;
  public
    constructor Create(Collection: TCollection); override;
    property Key: string read FKey write FKey;
    property Value: string read FValue write FValue;
  end;

  TParamCollection = class (TCollection)
  private
    FUserItem: TUserItem;
    function GetItem(Index: Integer): TParamItem;
    procedure SetItem(Index: Integer; Value: TParamItem);
  public
    function IndexOf (Key: string): integer;
    function AddKey (Key, Value: string): TParamItem;
    property Items[Index: Integer]: TParamItem read GetItem write SetItem; default;
    property UserItem: TUserItem read FUserItem write FUserItem;
  end;

  TTabItem = class (TCollectionItem)
  private
    FTabs:            TTabCollection;
    FTabCaption:      string;
    FKind:            TTabType;
    FURLProgram:      string;
    FParams:          string;
    FWaitMS:          integer;
    FHideFrames:      boolean;
    FLoadAtStartup:   boolean;
    FAutoKill:        boolean;
    FDownloadPath:    string;
    FAllowOpenDialog: TNullableBoolean;
    FTabUser:         string;
    FTabPassword:     string;
    FCommandList: TList<TCommandInfo>;

//   function GetCommandList: TList<TCommandInfo>;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property TabCaption: string read FTabCaption write FTabCaption;
    property Kind: TTabType read FKind write FKind;
    property URLProgram: string read FURLProgram write FURLProgram;
    property Params: string read FParams write FParams;
    property WaitMS: integer read FWaitMS write FWaitMS;
    property HideFrames: boolean read FHideFrames write FHideFrames;
    property LoadAtStartup: boolean read FLoadAtStartup write FLoadAtStartup;
    property AutoKill: boolean read FAutoKill write FAutoKill;
    property DownloadPath: string read FDownloadPath write FDownloadPath;
    property AllowOpenDialog: TNullableBoolean read FAllowOpenDialog write FAllowOpenDialog;
    property TabUser: string read FTabUser write FTabUser;
    property TabPassword: string read FTabPassword write FTabPassword;
    property CommandList: TList<TCommandInfo> read FCommandList write FCommandList;
  end;

  TTabCollection = class (TCollection)
  private
    FUserItem: TUserItem;
    function GetItem(Index: Integer): TTabItem;
    procedure SetItem(Index: Integer; Value: TTabItem);
  public
    function AddTab (TabCaption: string; Kind: TTabType; URLProgram, Params: string; WaitMS: integer;
                     HideFrames, LoadAtStartup, AutoKill: boolean; DownloadPath: string;
                     AllowOpenDialog: TNullableBoolean; TabUser, TabPassword: string; aCommandList: TList<TCommandInfo>): TTabItem;
    procedure Exchange (Idx1, Idx2: integer);
    property Items[Index: Integer]: TTabItem read GetItem write SetItem; default;
    property UserItem: TUserItem read FUserItem write FUserItem;
  end;

  TUserItem = class (TCollectionItem)
  private
    FUsers:    TUserCollection;
    FTabs:     TTabCollection;
    FParams:   TParamCollection;
    FUserID:   integer;
    FUserName: string;
    FPassword: string;
    FIdleTime: Integer;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    function CloneUser (NewUserID: integer; NewUserName, NewPassword: string; NewIdleTime: Integer): TUserItem;
    property UserID: integer read FUserID write FUserID;
    property UserName: string read FUserName write FUserName;
    property Password: string read FPassword write FPassword;
    property IdleTime: Integer read FIdleTime write FIdleTime;
    property Tabs: TTabCollection read FTabs write FTabs;
    property Params: TParamCollection read FParams write FParams;
  end;

  TUserCollection = class (TCollection)
  private
    function GetItem(Index: Integer): TUserItem;
    procedure SetItem(Index: Integer; Value: TUserItem);
  public
    function IndexOf (UserID: integer): integer; overload;
    function IndexOf (UserName: string): integer; overload;
    function AddUser (UserID: integer; UserName, Password: string; aIdleTime: integer): TUserItem;
    function TabCount: integer;
    function ParamCount: integer;
    procedure Assign (Sender: TUserCollection);
    property Items[Index: Integer]: TUserItem read GetItem write SetItem; default;
  end;

  TFSetup = class(TForm)
    PageControl: TcxPageControl;
    TabGeneralSettings: TcxTabSheet;
    TabGlobalParameters: TcxTabSheet;
    cxLabel1: TcxLabel;
    edStoreName: TcxTextEdit;
    cxLabel2: TcxLabel;
    edExpirationDate: TcxDateEdit;
    cxLabel3: TcxLabel;
    edPassword: TcxTextEdit;
    cxLabel4: TcxLabel;
    labPasswordNotMatch: TcxLabel;
    rgTabPosition: TcxRadioGroup;
    cbRemoveBar: TcxCheckBox;
    cxLabel6: TcxLabel;
    cbSkinName: TcxComboBox;
    cxLabel7: TcxLabel;
    edPasswordValidation: TcxTextEdit;
    edDefaultDownloadPath: TcxTextEdit;
    btnDefaultDownloadPath: TcxButton;
    cbAllowOpenDlg: TcxCheckBox;
    TabURLs: TdxMemData;
    TabURLsTabCaption: TStringField;
    TabURLsKind: TStringField;
    TabURLsURL_Prog: TStringField;
    TabURLsParams: TStringField;
    TabURLsWaitMS: TWordField;
    TabURLsHideFrames: TBooleanField;
    TabURLsLoadAtStartup: TBooleanField;
    TabURLsAutoKill: TBooleanField;
    TabURLsDownloadPath: TStringField;
    TabURLsAllowOpenDlg: TBooleanField;
    TabURLsTabUser: TStringField;
    TabURLsTabPassword: TStringField;
    dsURLs: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    RowEven: TcxStyle;
    RowOdd: TcxStyle;
    Header: TcxStyle;
    Selection: TcxStyle;
    OpenDlg: TOpenDialog;
    FolderDlg: TJvBrowseForFolderDialog;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    TabParams: TdxMemData;
    GridParametersDBTableView1: TcxGridDBTableView;
    GridParametersLevel1: TcxGridLevel;
    GridParameters: TcxGrid;
    dsParams: TDataSource;
    ParamsNavigator: TcxDBNavigator;
    TabParamsKey: TStringField;
    TabParamsValue: TStringField;
    GridParametersDBTableView1RecId: TcxGridDBColumn;
    GridParametersDBTableView1Key: TcxGridDBColumn;
    GridParametersDBTableView1Value: TcxGridDBColumn;
    TabTabConfiguration: TcxTabSheet;
    GridPrograms: TcxGrid;
    GridProgramsDBTableView1: TcxGridDBTableView;
    GridProgramsDBTableView1RecId: TcxGridDBColumn;
    GridProgramsDBTableView1TabCaption: TcxGridDBColumn;
    GridProgramsDBTableView1Kind: TcxGridDBColumn;
    GridProgramsDBTableView1URL_Prog: TcxGridDBColumn;
    GridProgramsDBTableView1Params: TcxGridDBColumn;
    GridProgramsDBTableView1WaitMS: TcxGridDBColumn;
    GridProgramsDBTableView1HideFrames: TcxGridDBColumn;
    GridProgramsDBTableView1LoadAtStartup: TcxGridDBColumn;
    GridProgramsDBTableView1AutoKill: TcxGridDBColumn;
    GridProgramsDBTableView1AllowOpenDlg: TcxGridDBColumn;
    GridProgramsDBTableView1DownloadPath: TcxGridDBColumn;
    GridProgramsDBTableView1TabUser: TcxGridDBColumn;
    GridProgramsDBTableView1TabPassword: TcxGridDBColumn;
    GridProgramsLevel1: TcxGridLevel;
    btnDown: TcxButton;
    btnUp: TcxButton;
    cxLabel5: TcxLabel;
    URLNavigator: TcxDBNavigator;
    edParams: TcxTextEdit;
    btnSetNull: TcxButton;
    cxLabel8: TcxLabel;
    dbedTabCaption: TcxDBTextEdit;
    dbrgKind: TcxDBRadioGroup;
    cxLabel9: TcxLabel;
    dbedURL_Program: TDBSynEdit;
    cxLabel10: TcxLabel;
    dbedProgParams: TDBSynEdit;
    cxLabel12: TcxLabel;
    dbcbHideFrames: TcxDBCheckBox;
    dbcbLoadAtStartup: TcxDBCheckBox;
    dbcbAutokill: TcxDBCheckBox;
    cxLabel13: TcxLabel;
    dbedTabUser: TcxDBTextEdit;
    cxLabel14: TcxLabel;
    cbedTabPassword: TcxDBTextEdit;
    dbcbAllowOpenDlg: TcxDBCheckBox;
    cxLabel15: TcxLabel;
    dbedDownloadPath: TcxDBButtonEdit;
    dbseWaitMS: TcxDBSpinEdit;
    labParams: TcxLabel;
    SynCompPropURLProg: TSynCompletionProposal;
    SynCompPropProgParams: TSynCompletionProposal;
    cxButton1: TcxButton;
    cxLabel11: TcxLabel;
    GridUsersDBTableView1: TcxGridDBTableView;
    GridUsersLevel1: TcxGridLevel;
    GridUsers: TcxGrid;
    cxDBNavigator1: TcxDBNavigator;
    TabUsers: TdxMemData;
    dsUsers: TDataSource;
    TabUsersUserName: TStringField;
    TabUsersPassword: TStringField;
    GridUsersDBTableView1RecId: TcxGridDBColumn;
    GridUsersDBTableView1UserName: TcxGridDBColumn;
    GridUsersDBTableView1Password: TcxGridDBColumn;
    TabUsersUserID: TIntegerField;
    GridUsersDBTableView1UserID: TcxGridDBColumn;
    btnAddUser: TcxButton;
    btnResetPassword: TcxButton;
    btnDeleteUser: TcxButton;
    TabURLsUserID: TIntegerField;
    TabParamsUserID: TIntegerField;
    cxLabel16: TcxLabel;
    dblcbURLsUserID: TcxDBLookupComboBox;
    GridProgramsDBTableView1UserID: TcxGridDBColumn;
    GridParametersDBTableView1UserID: TcxGridDBColumn;
    cxLabel17: TcxLabel;
    cbSelectedUser: TcxComboBox;
    ActionList1: TActionList;
    actTabMoveUp: TAction;
    actTabMoveDown: TAction;
    ImageList1: TImageList;
    btnCloneUser: TcxButton;
    TabCommands: TdxMemData;
    dsCommands: TDataSource;
    TabURLsurlID: TStringField;
    TabCommandsUrlID: TStringField;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    cxDBTextEdit3: TcxDBTextEdit;
    cxLabel18: TcxLabel;
    cxLabel19: TcxLabel;
    cxLabel20: TcxLabel;
    cxButton2: TcxButton;
    cxLabel21: TcxLabel;
    TabCommandsCaption: TStringField;
    TabCommandsExe: TStringField;
    TabCommandsParams: TStringField;
    Commands: TLabel;
    CmdNavigator: TcxDBNavigator;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1Caption: TcxGridDBColumn;
    TabUsersIdleTime: TIntegerField;
    GridUsersDBTableView1IdleTime: TcxGridDBColumn;
    TabCommandsMode: TIntegerField;
    cbShowMode: TDBLookupComboBox;
    tabCmdShow: TFDMemTable;
    tabCmdShowid: TIntegerField;
    tabCmdShowoption: TStringField;
    dsCmdShow: TDataSource;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure ControlChanged(Sender: TObject);
    procedure TabURLsAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure edPasswordPropertiesChange(Sender: TObject);
    procedure edPasswordValidationPropertiesChange(Sender: TObject);
    procedure edExpirationDatePropertiesChange(Sender: TObject);
    procedure cbSkinNamePropertiesChange(Sender: TObject);
    procedure TabURLsAfterDelete(DataSet: TDataSet);
    procedure GridDBTableView1URL_ProgPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure TabURLsNewRecord(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure btnDefaultDownloadPathClick(Sender: TObject);
    procedure GridDBTableView1DownloadPathPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure dsURLsDataChange(Sender: TObject; Field: TField);
    procedure btnSetNullClick(Sender: TObject);
    procedure dbedURL_ProgramKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxDBButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure dbedURL_ProgramChange(Sender: TObject);
    procedure dbedProgParamsChange(Sender: TObject);
    procedure TabUsersAfterDelete(DataSet: TDataSet);
    procedure TabUsersAftefrPost(DataSet: TDataSet);
    procedure TabUsersBeforePost(DataSet: TDataSet);
    procedure dsUsersStateChange(Sender: TObject);
    procedure btnAddUserClick(Sender: TObject);
    procedure btnResetPasswordClick(Sender: TObject);
    procedure btnDeleteUserClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure cbSelectedUserPropertiesChange(Sender: TObject);
    procedure actTabMoveUpExecute(Sender: TObject);
    procedure actTabMoveDownExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure PageControlPageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure TabParamsNewRecord(DataSet: TDataSet);
    procedure TabURLsBeforePost(DataSet: TDataSet);
    procedure TabURLsBeforeDelete(DataSet: TDataSet);
    procedure TabParamsBeforePost(DataSet: TDataSet);
    procedure TabParamsAfterPost(DataSet: TDataSet);
    procedure TabParamsBeforeDelete(DataSet: TDataSet);
    procedure TabParamsAfterDelete(DataSet: TDataSet);
    procedure TabUsersBeforeDelete(DataSet: TDataSet);
    procedure btnCloneUserClick(Sender: TObject);
    procedure TabURLsAfterScroll(DataSet: TDataSet);
    procedure TabCommandsFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure cxButton2Click(Sender: TObject);
    procedure TabCommandsBeforePost(DataSet: TDataSet);
    procedure TabCommandsAfterEdit(DataSet: TDataSet);
    procedure dsCommandsStateChange(Sender: TObject);
    procedure dsURLsStateChange(Sender: TObject);
    procedure TabCommandsAfterDelete(DataSet: TDataSet);
    procedure TabCommandsAfterInsert(DataSet: TDataSet);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
  private
    { Private declarations }
    //FSkinFile:      string;
    FSkinsFound:    boolean;
    FSkinOK:        boolean;
    Loading:        boolean;
    UpdatingTables: boolean;
    FLastTabOp:     TTableOperation;
    FLastTabIdx:    integer;
    FLastParamOp:   TTableOperation;
    FLastParamIdx:  integer;
    FLastUserOp:    TTableOperation;
    FLastUserIdx:   integer;



    function SelectedUserID: integer;
    procedure UpdateCompletionProposals;
    procedure UpdateParamsPreview;
    procedure SelectProgram;
    procedure SelectDownloadPath;
    function GetNextUserID: integer;
    procedure UpdateTables;
    procedure PopulateUserSelection;
  public

    FNextUserID: integer;
    FUserList:   TUserCollection;
    procedure UpdateSkin;
    procedure LoadSkins (DisplayMessage: boolean = True);
    function UserExists (UserName: string): boolean;
  end;

var
  FSetup: TFSetup;

implementation
uses uMainForm, uFUserLogin;

{$R *.dfm}

function EnvSysVarList: TStringList;
var
  Variable: Boolean;
  Str: PChar;
  Res, VarName: string;
  p: integer;
begin
  result   := TStringList.Create;
  Str      := GetEnvironmentStrings;
  Res      := '';
  Variable := False;
  while True do
  begin
    if Str^ = #0 then
    begin
      if Variable then
      begin
        P       := Pos ('=', Res);
        VarName := Copy (Res, 1, P - 1);
        result.Add(VarName);
      end;
      Variable := True;
      Inc(Str);
      Res := '';
      if Str^ = #0 then
        Break
      else
        Res := Res + str^;
    end
    else
      if Variable then
        Res := Res + Str^;
    Inc(str);
  end;
end;

{$region 'TParamItem'}
constructor TParamItem.Create(Collection: TCollection);
begin
  inherited Create (Collection);

  FKey   := '';
  FValue := '';
end;
{$endregion}

{$region 'TParamCollection'}
function TParamCollection.GetItem(Index: Integer): TParamItem;
begin
  Result := TParamItem (inherited GetItem(Index));
end;

procedure TParamCollection.SetItem(Index: Integer; Value: TParamItem);
begin
  inherited SetItem(Index, Value);
end;

function TParamCollection.IndexOf (Key: string): integer;
var
  i: integer;
begin
  result := -1;
  Key    := UpperCase (Key);

  for i := 0 to Count - 1 do
    if UpperCase (Items [i].Key) = Key then
    begin
      result := i;
      exit;
    end;
end;

function TParamCollection.AddKey (Key, Value: string): TParamItem;
begin
  result := nil;

  if IndexOf (Key) <> -1 then exit;

  result        := TParamItem (Add);
  result.FKey   := Key;
  result.FValue := Value;
end;
{$endregion}

{$region 'TTabItem'}
constructor TTabItem.Create(Collection: TCollection);
begin
  inherited Create (Collection);

  FTabs             := TTabCollection (Collection);
  FTabCaption       := '';
  FKind             := ttNone;
  FURLProgram       := '';
  FParams           := '';
  FWaitMS           := 0;
  FHideFrames       := True;
  FLoadAtStartup    := False;
  FAutoKill         := False;
  FDownloadPath     := '';
  FAllowOpenDialog  := nbNull;
  FTabUser          := '';
  FTabPassword      := '';

end;
{$region}

{$region 'TTabCollection'}
function TTabCollection.GetItem(Index: Integer): TTabItem;
begin
  Result := TTabItem (inherited GetItem(Index));
end;

procedure TTabCollection.SetItem(Index: Integer; Value: TTabItem);
begin
  inherited SetItem(Index, Value);
end;

function TTabCollection.AddTab (TabCaption: string; Kind: TTabType; URLProgram, Params: string; WaitMS: integer;
                                HideFrames, LoadAtStartup, AutoKill: boolean; DownloadPath: string;
                                AllowOpenDialog: TNullableBoolean; TabUser, TabPassword: string; aCommandList: TList<TCommandInfo>): TTabItem;
begin
  result                 := TTabItem (Add);
  result.TabCaption      := TabCaption;
  result.Kind            := KInd;
  result.URLProgram      := URLProgram;
  result.Params          := Params;
  result.WaitMS          := WaitMS;
  result.HideFrames      := HideFrames;
  result.LoadAtStartup   := LoadAtStartup;
  result.AutoKill        := AutoKill;
  result.DownloadPath    := DownloadPath;
  result.AllowOpenDialog := AllowOpenDialog;
  result.TabUser         := TabUser;
  result.TabPassword     := TabPassword;

  result.FCommandList.Free;


  if Assigned(aCommandList) then
  begin
    result.FCommandList := TList<TCommandInfo>.create;
    var info: TCommandInfo;
    for var i: integer := 0 to acommandList.Count - 1 do
//    for info in aCommandList do
    begin
      result.FCommandList.Add(acommandList[i])
    end;
  end;
end;

procedure TTabCollection.Exchange (Idx1, Idx2: integer);
var
  TmpTabCaption:      string;
  TmpKind:            TTabType;
  TmpURLProgram:      string;
  TmpParams:          string;
  TmpWaitMS:          integer;
  TmpHideFrames:      boolean;
  TmpLoadAtStartup:   boolean;
  TmpAutoKill:        boolean;
  TmpDownloadPath:    string;
  TmpAllowOpenDialog: TNullableBoolean;
  TmpTabUser:         string;
  TmpTabPassword:     string;
begin
  TmpTabCaption                 := Items [Idx1].TabCaption;
  TmpKind                       := Items [Idx1].Kind;
  TmpURLProgram                 := Items [Idx1].URLProgram;
  TmpParams                     := Items [Idx1].Params;
  TmpWaitMS                     := Items [Idx1].WaitMS;
  TmpHideFrames                 := Items [Idx1].HideFrames;
  TmpLoadAtStartup              := Items [Idx1].LoadAtStartup;
  TmpAutoKill                   := Items [Idx1].AutoKill;
  TmpDownloadPath               := Items [Idx1].DownloadPath;
  TmpAllowOpenDialog            := Items [Idx1].AllowOpenDialog;
  TmpTabUser                    := Items [Idx1].TabUser;
  TmpTabPassword                := Items [Idx1].TabPassword;

  Items [Idx1].TabCaption       := Items [Idx2].TabCaption;
  Items [Idx1].Kind             := Items [Idx2].Kind;
  Items [Idx1].URLProgram       := Items [Idx2].URLProgram;
  Items [Idx1].Params           := Items [Idx2].Params;
  Items [Idx1].WaitMS           := Items [Idx2].WaitMS;
  Items [Idx1].HideFrames       := Items [Idx2].HideFrames;
  Items [Idx1].LoadAtStartup    := Items [Idx2].LoadAtStartup;
  Items [Idx1].AutoKill         := Items [Idx2].AutoKill;
  Items [Idx1].DownloadPath     := Items [Idx2].DownloadPath;
  Items [Idx1].AllowOpenDialog  := Items [Idx2].AllowOpenDialog;
  Items [Idx1].TabUser          := Items [Idx2].TabUser;
  Items [Idx1].TabPassword      := Items [Idx2].TabPassword;

  Items [Idx2].TabCaption       := TmpTabCaption;
  Items [Idx2].Kind             := TmpKind;
  Items [Idx2].URLProgram       := TmpURLProgram;
  Items [Idx2].Params           := TmpParams;
  Items [Idx2].WaitMS           := TmpWaitMS;
  Items [Idx2].HideFrames       := TmpHideFrames;
  Items [Idx2].LoadAtStartup    := TmpLoadAtStartup;
  Items [Idx2].AutoKill         := TmpAutoKill;
  Items [Idx2].DownloadPath     := TmpDownloadPath;
  Items [Idx2].AllowOpenDialog  := TmpAllowOpenDialog;
  Items [Idx2].TabUser          := TmpTabUser;
  Items [Idx2].TabPassword      := TmpTabPassword;
end;

{$endregion}

{$region 'TUserItem'}
constructor TUserItem.Create(Collection: TCollection);
begin
  inherited Create (Collection);

  FUsers            := TUserCollection (Collection);
  FUserID           := -1;
  FUserName         := '';
  FPassword         := '';
  FTabs             := TTabCollection.Create(TTabItem);
  FTabs.FUserItem   := Self;
  FParams           := TParamCollection.Create(TParamItem);
  FParams.FUserItem := Self;
end;

destructor TUserItem.Destroy;
begin
  FreeAndNil (FTabs);
  FreeAndNil (FParams);

  inherited Destroy;
end;

function TUserItem.CloneUser (NewUserID: integer; NewUserName, NewPassword: string; NewIdleTime: Integer): TUserItem;
var
  i: integer;
begin
  result := FUsers.AddUser(NewUserID, NewUserName, NewPassword, NewIdleTime);

  for i := 0 to Tabs.Count - 1 do
    result.Tabs.AddTab(Tabs.Items [i].TabCaption,
                       Tabs.Items [i].Kind,
                       Tabs.Items [i].URLProgram,
                       Tabs.Items [i].Params,
                       Tabs.Items [i].WaitMS,
                       Tabs.Items [i].HideFrames,
                       Tabs.Items [i].LoadAtStartup,
                       Tabs.Items [i].AutoKill,
                       Tabs.Items [i].DownloadPath,
                       Tabs.Items [i].AllowOpenDialog,
                       Tabs.Items [i].TabUser,
                       Tabs.Items [i].TabPassword,
                       Tabs.items [i].CommandList);

  for i := 0 to Params.Count - 1 do
    result.Params.AddKey(Params.Items [i].Key,
                         Params.Items [i].Value);
end;


{$endregion}

{$region 'TUserCollection'}
function TUserCollection.GetItem(Index: Integer): TUserItem;
begin

 // if index >= 0 then
   try
  Result := TUserItem (inherited GetItem(Index))
   finally

   end;
 // else
 //   Result := nil;

end;

procedure TUserCollection.SetItem(Index: Integer; Value: TUserItem);
begin
  inherited SetItem(Index, Value);
end;

function TUserCollection.IndexOf (UserID: integer): integer;
var
  i: integer;
begin
  result := -1;

  for i := 0 to Count - 1 do
    if Items [i].UserID = UserID then
    begin
      result := i;
      exit;
    end;
end;

function TUserCollection.IndexOf (UserName: string): integer;
var
  i: integer;
begin
  result   := -1;
  UserName := UpperCase (UserName);

  for i := 0 to Count - 1 do
    if UpperCase (Items [i].UserName) = UserName then
    begin
      result := i;
      exit;
    end;
end;

function TUserCollection.AddUser (UserID: integer; UserName, Password: string; aIdleTime: integer): TUserItem;
begin
  result := nil;

  if (IndexOf (UserID) = -1) and (IndexOf (UserName) = -1) then
  begin
    result           := TUserItem (Add);
    result.FUserID   := UserID;
    result.FUserName := UserName;
    result.FPassword := Password;
    result.IdleTime := aIdleTime;
  end;
end;

function TUserCollection.TabCount: integer;
var
  i: integer;
begin
  result := 0;

  for i := 0 to Count - 1 do
    Inc (result, Items [i].Tabs.Count);
end;

function TUserCollection.ParamCount: integer;
var
  i: integer;
begin
  result := 0;

  for i := 0 to Count - 1 do
    Inc (result, Items [i].Params.Count);
end;

procedure TUserCollection.Assign (Sender: TUserCollection);
var
  i, j: integer;
  u: TUserItem;
  t: TTabItem;
  p: TParamItem;
begin
  Clear;

  for i := 0 to Sender.Count - 1 do
  begin
    u := AddUser(Sender.Items [i].UserID,
                 Sender.Items [i].UserName,
                 Sender.Items[i].Password, Sender.Items[i].IdleTime);


    for j := 0 to Sender.Items [i].Tabs.Count - 1 do
      t := u.Tabs.AddTab(Sender.Items [i].Tabs.Items [j].TabCaption,
                         Sender.Items [i].Tabs.Items [j].Kind,
                         Sender.Items [i].Tabs.Items [j].URLProgram,
                         Sender.Items [i].Tabs.Items [j].Params,
                         Sender.Items [i].Tabs.Items [j].WaitMS,
                         Sender.Items [i].Tabs.Items [j].HideFrames,
                         Sender.Items [i].Tabs.Items [j].LoadAtStartup,
                         Sender.Items [i].Tabs.Items [j].AutoKill,
                         Sender.Items [i].Tabs.Items [j].DownloadPath,
                         Sender.Items [i].Tabs.Items [j].AllowOpenDialog,
                         Sender.Items [i].Tabs.Items [j].TabUser,
                         Sender.Items [i].Tabs.Items [j].TabPassword,
                         Sender.Items [i].Tabs.Items [j].CommandList);

    for j := 0 to Sender.Items [i].Params.Count - 1 do
      p := u.Params.AddKey(Sender.Items [i].Params.Items [j].Key,
                           Sender.Items [i].Params.Items [j].Value);
  end;
end;

{$endregion}

{$region 'TFSetup'}
function TFSetup.GetNextUserID: integer;
begin
  result := FNextUserID;
  Inc (FNextUserID);
end;

function TFSetup.SelectedUserID: integer;
begin
  result := -1;

  if cbSelectedUser.ItemIndex >= 0 then
    result := Integer (cbSelectedUser.Properties.Items.Objects [cbSelectedUser.ItemIndex]);
end;

procedure TFSetup.PopulateUserSelection;
var
  i: integer;
begin
  Loading := True;
  cbSelectedUser.Properties.Items.Clear;
  cbSelectedUser.Properties.Items.Add('<All users>');
  for i := 0 to FUserList.Count - 1 do
    cbSelectedUser.Properties.Items.AddObject(FUserList.Items [i].UserName, TObject (FUserList.Items [i].UserID));
  cbSelectedUser.ItemIndex := 0;
  Loading := False;
end;


procedure TFSetup.UpdateTables;
var
  i, j: integer;
begin
  if Loading then exit;

  UpdatingTables := True;
  TabUsers.DisableControls;
  TabURLs.DisableControls;
  TabParams.DisableControls;
  try
    TabUsers.Close;
    TabUsers.Open;
    TabURLs.Close;
    TabURLs.Open;
    TabParams.Close;
    TabParams.Open;
    TabCommands.Close;
    TabCommands.Open;

    TabCommands.Filter := 'urlid = ' + QuotedStr(TabUrls.FieldByName('urlid').AsString);
    TabCommands.Filtered := true;
    TabUsers.ReadOnly  := False;
    TabURLs.ReadOnly   := False;
    TabParams.ReadOnly := False;
    for i := 0 to FUserList.Count - 1 do
    begin
      if FUserList.Items [i].UserID <> 0 then
      begin
        TabUsers.Append;
        TabUsers.FieldByName('UserID').AsInteger  := FUserList.Items [i].UserID;
        TabUsers.FieldByName('UserName').AsString := FUserList.Items [i].UserName;
        TabUsers.FieldByName('Password').AsString := FUserList.Items [i].Password;
        TabUsers.FieldByName('IdleTime').AsInteger := FUserList.Items [i].IdleTime;
        TabUsers.Post;
      end;

      if (cbSelectedUser.ItemIndex = 0) or
         (SelectedUserID = FUserList.Items [i].UserID) then
      begin
        for j := 0 to FUserList.Items [i].FTabs.Count - 1 do
        begin
          TabURLs.Append;
          if FUserList.Items [i].UserID <> 0 then
            TabURLs.FieldByName('UserID').AsInteger  := FUserList.Items [i].UserID;

          TabURLs.FieldByName('TabCaption').AsString     := FUserList.Items [i].Tabs.Items [j].TabCaption;
          TabURLs.FieldByName('URL_Prog').AsString       := FUserList.Items [i].Tabs.Items [j].URLProgram;
          TabURLs.FieldByName('Params').AsString         := FUserList.Items [i].Tabs.Items [j].Params;
          TabURLs.FieldByName('WaitMS').AsInteger        := FUserList.Items [i].Tabs.Items [j].WaitMS;
          TabURLs.FieldByName('HideFrames').AsBoolean    := FUserList.Items [i].Tabs.Items [j].HideFrames;
          TabURLs.FieldByName('LoadAtStartup').AsBoolean := FUserList.Items [i].Tabs.Items [j].LoadAtStartup;
          TabURLs.FieldByName('HideFrames').AsBoolean    := FUserList.Items [i].Tabs.Items [j].HideFrames;
          TabURLs.FieldByName('AutoKill').AsBoolean      := FUserList.Items [i].Tabs.Items [j].AutoKill;
          TabURLs.FieldByName('DownloadPath').AsString   := FUserList.Items [i].Tabs.Items [j].DownloadPath;
          TabURLs.FieldByName('TabUser').AsString        := FUserList.Items [i].Tabs.Items [j].TabUser;
          TabURLs.FieldByName('TabPassword').AsString    := FUserList.Items [i].Tabs.Items [j].TabPassword;
          TabURLs.FieldByName('URLID').AsString    := Tguid.NewGuid.ToString;

          case FUserList.Items [i].Tabs.Items [j].Kind of
            ttURL:     TabURLs.FieldByName('Kind').AsString := 'W';
            ttProgram: TabURLs.FieldByName('Kind').AsString := 'P';
          end;

          case FUserList.Items [i].Tabs.Items [j].AllowOpenDialog of
            nbFalse: TabURLs.FieldByName('AllowOpenDlg').AsBoolean := False;
            nbTrue:  TabURLs.FieldByName('AllowOpenDlg').AsBoolean := True;
          end;

          TabURLs.Post;

          {$region 'fill commands table'}
          try
            var cmdInfo: TCommandInfo;
            for cmdInfo in FUserList.Items [i].Tabs.Items [j].CommandList do
            begin
              TabCommands.Insert;
              TabCommands.FieldByName('urlid').asstring := TabURLs.FieldByName('urlid').AsString;
              TabCommands.FieldByName('Caption').asstring :=  cmdInfo.Caption;
              TabCommands.FieldByName('Mode').Asinteger :=  cmdInfo.Mode;
              TabCommands.FieldByName('exe').asstring :=  cmdInfo.exe;
              TabCommands.FieldByName('Params').asstring :=  cmdInfo.Params;
              TabCommands.post;
            end;

          except
            raise Exception.Create('Error Message');
          end;

          {$endregion}
        end;

        for j := 0 to FUserList.Items [i].Params.Count - 1 do
        begin
          TabParams.Append;
          TabParams.FieldByName('UserID').AsInteger := FUserList.Items [i].UserID;
          TabParams.FieldByName('Key').AsString     := FUserList.Items [i].Params.Items [j].Key;
          TabParams.FieldByName('Value').AsString   := FUserList.Items [i].Params.Items [j].Value;
          TabParams.Post;
        end;
      end;
    end;

    TabUsers.First;
    TabURLs.First;
    TabParams.First;
  finally
    TabUsers.EnableControls;
    TabURLs.EnableControls;
    TabParams.EnableControls;
    UpdatingTables := False;
    TabURLs.Last;
    TabURLs.First;
  end;
end;

procedure TFSetup.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
var
  AllBrowse, CanMoveTab, ApplyReadOnly: boolean;
begin
  AllBrowse                           := (TabUsers.State = dsBrowse) and (TabURLs.State = dsBrowse) and (TabParams.State = dsBrowse);
  CanMoveTab                          := (TabUsers.RecordCount = 0) or (cbSelectedUser.ItemIndex > 0);
  ApplyReadOnly                       := (TabUsers.RecordCount > 0) and (cbSelectedUser.ItemIndex = 0);
  actTabMoveUp.Enabled                := AllBrowse and CanMoveTab and (GridProgramsDBTableView1.DataController.FocusedRecordIndex > 0);
  actTabMoveDown.Enabled              := AllBrowse and CanMoveTab and (GridProgramsDBTableView1.DataController.FocusedRecordIndex < (GridProgramsDBTableView1.DataController.RecordCount - 1));
  cbSelectedUser.Properties.ReadOnly  := (TabURLs.State in [dsInsert, dsEdit]) or (TabParams.State in [dsInsert, dsEdit]);
  dblcbURLsUserID.Properties.ReadOnly := (TabUsers.RecordCount > 0) and (cbSelectedUser.ItemIndex > 1);
  dblcbURLsUserID.Properties.ReadOnly   := ApplyReadOnly;
  dbedTabCaption.Properties.ReadOnly    := ApplyReadOnly;
  dbrgKind.Properties.ReadOnly          := ApplyReadOnly;
  dbedURL_Program.ReadOnly              := ApplyReadOnly;
  dbedProgParams.ReadOnly               := ApplyReadOnly;
  dbseWaitMS.Properties.ReadOnly        := ApplyReadOnly;
  dbcbHideFrames.Properties.ReadOnly    := ApplyReadOnly;
  dbcbLoadAtStartup.Properties.ReadOnly := ApplyReadOnly;
  dbcbAutokill.Properties.ReadOnly      := ApplyReadOnly;
  dbedTabUser.Properties.ReadOnly       := ApplyReadOnly;
  cbedTabPassword.Properties.ReadOnly   := ApplyReadOnly;
  dbcbAllowOpenDlg.Properties.ReadOnly  := ApplyReadOnly;
  dbedDownloadPath.Properties.ReadOnly  := ApplyReadOnly;

  TcxLookupComboBoxProperties (GridProgramsDBTableView1UserID.Properties).ReadOnly   := ApplyReadOnly; // or ((TabUsers.RecordCount > 0) and (cbSelectedUser.ItemIndex > 1));
  TcxLookupComboBoxProperties (GridParametersDBTableView1UserID.Properties).ReadOnly := ApplyReadOnly; // or ((TabUsers.RecordCount > 0) and (cbSelectedUser.ItemIndex > 1));
  TcxTextEditProperties (GridProgramsDBTableView1TabCaption.Properties).ReadOnly     := ApplyReadOnly;
  TcxRadioGroupProperties (GridProgramsDBTableView1Kind.Properties).ReadOnly         := ApplyReadOnly;
  TcxButtonEditProperties (GridProgramsDBTableView1URL_Prog.Properties).ReadOnly     := ApplyReadOnly;
  TcxTextEditProperties (GridProgramsDBTableView1Params.Properties).ReadOnly         := ApplyReadOnly;
  TcxSpinEditProperties (GridProgramsDBTableView1WaitMS.Properties).ReadOnly         := ApplyReadOnly;
  TcxCheckBoxProperties (GridProgramsDBTableView1HideFrames.Properties).ReadOnly     := ApplyReadOnly;
  TcxCheckBoxProperties (GridProgramsDBTableView1LoadAtStartup.Properties).ReadOnly  := ApplyReadOnly;
  TcxCheckBoxProperties (GridProgramsDBTableView1AutoKill.Properties).ReadOnly       := ApplyReadOnly;
  TcxCheckBoxProperties (GridProgramsDBTableView1AllowOpenDlg.Properties).ReadOnly   := ApplyReadOnly;
  TcxButtonEditProperties (GridProgramsDBTableView1DownloadPath.Properties).ReadOnly := ApplyReadOnly;
  TcxTextEditProperties (GridProgramsDBTableView1TabUser.Properties).ReadOnly        := ApplyReadOnly;
  TcxTextEditProperties (GridProgramsDBTableView1TabPassword.Properties).ReadOnly    := ApplyReadOnly;
  TcxTextEditProperties (GridParametersDBTableView1Key.Properties).ReadOnly          := ApplyReadOnly;
  TcxTextEditProperties (GridParametersDBTableView1Value.Properties).ReadOnly        := ApplyReadOnly;

  GridProgramsDBTableView1.OptionsData.Appending   := not ApplyReadOnly;
  GridParametersDBTableView1.OptionsData.Appending := not ApplyReadOnly;
  URLNavigator.Buttons.Append.Enabled              := not ApplyReadOnly;
  URLNavigator.Buttons.Edit.Enabled                := not ApplyReadOnly;
  URLNavigator.Buttons.Delete.Enabled              := not ApplyReadOnly;
  ParamsNavigator.Buttons.Append.Enabled           := not ApplyReadOnly;
  ParamsNavigator.Buttons.Edit.Enabled             := not ApplyReadOnly;
  ParamsNavigator.Buttons.Delete.Enabled           := not ApplyReadOnly;

  if ApplyReadOnly then
  begin
    dbedURL_Program.Color := clBtnFace;
    dbedProgParams.Color  := clBtnFace;
  end
  else
  begin
    dbedURL_Program.Color := clWindow;
    dbedProgParams.Color  := clWindow;
  end;
end;

procedure TFSetup.actTabMoveDownExecute(Sender: TObject);
var
  current, next: TBookmark;
  currCaption, currURL, currKind, currParams, currDownloadPath, currTabUser, currTabPassword: string;
  nextCaption, nextURL, nextKind, nextParams, nextDownloadPath, nextTabUser, nextTabPassword: string;
  currIdx, i,  si, ui: integer;
  currUserID, currWaitMS: integer;
  nextUserID, nextWaitMS: integer;
  currHasUserID, currHideFrames, currLoadAtStartup, currAutoKill: boolean;
  nextHasUserID, nextHideFrames, nextLoadAtStartup, nextAutoKill: boolean;
  currAllowOpenDlg: TNullableBoolean;
  nextAllowOpenDlg: TNullableBoolean;
  Kind: TTabType;
begin
  TabURLs.DisableControls;
  UpdatingTables := True;

  try
    //1. Get current data
    current           := TabURLs.GetBookmark;
    currIdx           := TabURLs.RecNo;
    CurrHasUserID     := not TabURLs.FieldByName('UserID').IsNull;
    CurrUserID        := TabURLs.FieldByName('UserID').AsInteger;
    CurrCaption       := TabURLs.FieldByName('TabCaption').AsString;
    CurrKind          := TabURLs.FieldByName('Kind').AsString;
    CurrURL           := TabURLs.FieldByName('URL_Prog').AsString;
    CurrParams        := TabURLs.FieldByName('Params').AsString;
    CurrWaitMS        := TabURLs.FieldByName('WaitMS').AsInteger;
    currHideFrames    := TabURLs.FieldByName('HideFrames').AsBoolean;
    currLoadAtStartup := TabURLs.FieldByName('LoadAtStartup').AsBoolean;
    currAutoKill      := TabURLs.FieldByName('AutoKill').AsBoolean;
    currDownloadPath  := TabURLs.FieldByName('DownloadPath').AsString;
    currTabUser       := TabURLs.FieldByName('TabUser').AsString;
    currTabPassword   := TabURLs.FieldByName('TabPassword').AsString;
    currAllowOpenDlg  := nbNull;
    if not TabURLs.FieldByName('AllowOpenDlg').IsNull then
    begin
      if TabURLs.FieldByName('AllowOpenDlg').AsBoolean then
        currAllowOpenDlg := nbTrue
      else
        currAllowOpenDlg := nbFalse;
    end;

    //2. Move to the next record
    TabURLs.Next;

    //3. Get next record data
    next              := TabURLs.GetBookmark;
    nextHasUserID     := not TabURLs.FieldByName('UserID').IsNull;
    nextUserID        := TabURLs.FieldByName('UserID').AsInteger;
    nextCaption       := TabURLs.FieldByName('TabCaption').AsString;
    nextKind          := TabURLs.FieldByName('Kind').AsString;
    nextURL           := TabURLs.FieldByName('URL_Prog').AsString;
    nextParams        := TabURLs.FieldByName('Params').AsString;
    nextWaitMS        := TabURLs.FieldByName('WaitMS').AsInteger;
    nextHideFrames    := TabURLs.FieldByName('HideFrames').AsBoolean;
    nextLoadAtStartup := TabURLs.FieldByName('LoadAtStartup').AsBoolean;
    nextAutoKill      := TabURLs.FieldByName('AutoKill').AsBoolean;
    nextDownloadPath  := TabURLs.FieldByName('DownloadPath').AsString;
    nextTabUser       := TabURLs.FieldByName('TabUser').AsString;
    nextTabPassword   := TabURLs.FieldByName('TabPassword').AsString;
    nextAllowOpenDlg  := nbNull;
    if not TabURLs.FieldByName('AllowOpenDlg').IsNull then
    begin
      if TabURLs.FieldByName('AllowOpenDlg').AsBoolean then
        nextAllowOpenDlg := nbTrue
      else
        nextAllowOpenDlg := nbFalse;
    end;

    //4. Update previous record with current record data
    TabURLs.Edit;
    if CurrHasUserID then
      TabURLs.FieldByName('UserID').AsInteger := currUserID
    else
      TabURLs.FieldByName('UserID').Clear;

    TabURLs.FieldByName('TabCaption').AsString     := CurrCaption;
    TabURLs.FieldByName('Kind').AsString           := CurrKind;
    TabURLs.FieldByName('URL_Prog').AsString       := CurrURL;
    TabURLs.FieldByName('Params').AsString         := CurrParams;
    TabURLs.FieldByName('WaitMS').AsInteger        := CurrWaitMS;
    TabURLs.FieldByName('HideFrames').AsBoolean    := currHideFrames;
    TabURLs.FieldByName('LoadAtStartup').AsBoolean := currLoadAtStartup;
    TabURLs.FieldByName('AutoKill').AsBoolean      := currAutoKill;
    TabURLs.FieldByName('DownloadPath').AsString   := currDownloadPath;
    TabURLs.FieldByName('TabUser').AsString        := currTabUser;
    TabURLs.FieldByName('TabPassword').AsString    := currTabPassword;
    case currAllowOpenDlg of
      nbNull:  TabURLs.FieldByName('AllowOpenDlg').Clear;
      nbFalse,
      nbTrue:  TabURLs.FieldByName('AllowOpenDlg').AsBoolean := currAllowOpenDlg = nbTrue;
    end;
    TabURLs.Post;

    //5. Back to current record
    TabURLs.GotoBookmark(current);

    //6. Update current record with next record data
    TabURLs.Edit;
    if nextHasUserID then
      TabURLs.FieldByName('UserID').AsInteger := nextUserID
    else
      TabURLs.FieldByName('UserID').Clear;

    TabURLs.FieldByName('TabCaption').AsString     := nextCaption;
    TabURLs.FieldByName('Kind').AsString           := nextKind;
    TabURLs.FieldByName('URL_Prog').AsString       := nextURL;
    TabURLs.FieldByName('Params').AsString         := nextParams;
    TabURLs.FieldByName('WaitMS').AsInteger        := nextWaitMS;
    TabURLs.FieldByName('HideFrames').AsBoolean    := nextHideFrames;
    TabURLs.FieldByName('LoadAtStartup').AsBoolean := nextLoadAtStartup;
    TabURLs.FieldByName('AutoKill').AsBoolean      := nextAutoKill;
    TabURLs.FieldByName('DownloadPath').AsString   := nextDownloadPath;
    TabURLs.FieldByName('TabUser').AsString        := nextTabUser;
    TabURLs.FieldByName('TabPassword').AsString    := nextTabPassword;
    case nextAllowOpenDlg of
      nbNull:  TabURLs.FieldByName('AllowOpenDlg').Clear;
      nbFalse,
      nbTrue:  TabURLs.FieldByName('AllowOpenDlg').AsBoolean := nextAllowOpenDlg = nbTrue;
    end;

    TabURLs.Post;

    //7. Set the next record as new current record
    TabURLs.GotoBookmark(next);

    //8. Set the new current values
    si := SelectedUserID;
    ui := FUserList.IndexOf (si);
    Dec (currIdx);

    Kind := ttNone;
    if nextKind = 'W' then
      Kind := ttURL
    else
      Kind := ttProgram;

    FUserList.Items [ui].Tabs [currIdx].TabCaption      := nextCaption;
    FUserList.Items [ui].Tabs [currIdx].Kind            := Kind;
    FUserList.Items [ui].Tabs [currIdx].URLProgram      := nextURL;
    FUserList.Items [ui].Tabs [currIdx].Params          := nextParams;
    FUserList.Items [ui].Tabs [currIdx].WaitMS          := nextWaitMS;
    FUserList.Items [ui].Tabs [currIdx].HideFrames      := nextHideFrames;
    FUserList.Items [ui].Tabs [currIdx].LoadAtStartup   := nextLoadAtStartup;
    FUserList.Items [ui].Tabs [currIdx].AutoKill        := nextAutoKill;
    FUserList.Items [ui].Tabs [currIdx].DownloadPath    := nextDownloadPath;
    FUserList.Items [ui].Tabs [currIdx].TabUser         := nextTabUser;
    FUserList.Items [ui].Tabs [currIdx].TabPassword     := nextTabPassword;
    FUserList.Items [ui].Tabs [currIdx].AllowOpenDialog := nextAllowOpenDlg;

    //9. Set the new previous values
    Kind := ttNone;
    if currKind = 'W' then
      Kind := ttURL
    else
      Kind := ttProgram;

    FUserList.Items [ui].Tabs [currIdx + 1].TabCaption      := currCaption;
    FUserList.Items [ui].Tabs [currIdx + 1].Kind            := Kind;
    FUserList.Items [ui].Tabs [currIdx + 1].URLProgram      := currURL;
    FUserList.Items [ui].Tabs [currIdx + 1].Params          := currParams;
    FUserList.Items [ui].Tabs [currIdx + 1].WaitMS          := currWaitMS;
    FUserList.Items [ui].Tabs [currIdx + 1].HideFrames      := currHideFrames;
    FUserList.Items [ui].Tabs [currIdx + 1].LoadAtStartup   := currLoadAtStartup;
    FUserList.Items [ui].Tabs [currIdx + 1].AutoKill        := currAutoKill;
    FUserList.Items [ui].Tabs [currIdx + 1].DownloadPath    := currDownloadPath;
    FUserList.Items [ui].Tabs [currIdx + 1].TabUser         := currTabUser;
    FUserList.Items [ui].Tabs [currIdx + 1].TabPassword     := currTabPassword;
    FUserList.Items [ui].Tabs [currIdx + 1].AllowOpenDialog := currAllowOpenDlg;
  finally
    TabURLs.FreeBookmark(next);
    TabURLs.FreeBookmark(current);
    TabURLs.EnableControls;
    GridPrograms.SetFocus;
    UpdatingTables := False;
    btnOK.Enabled  := True;
  end;
end;

procedure TFSetup.actTabMoveUpExecute(Sender: TObject);
var
  current, prev: TBookmark;
  currCaption, currURL, currKind, currParams, currDownloadPath, currTabUser, currTabPassword: string;
  prevCaption, prevURL, prevKind, prevParams, prevDownloadPath, prevTabUser, prevTabPassword: string;
  currIdx, i, ui: integer;
  currUserID, currWaitMS: integer;
  prevUserID, prevWaitMS: integer;
  currHasUserID, currHideFrames, currLoadAtStartup, currAutoKill: boolean;
  prevHasUserID, prevHideFrames, prevLoadAtStartup, prevAutoKill: boolean;
  currAllowOpenDlg: TNullableBoolean;
  prevAllowOpenDlg: TNullableBoolean;
  Kind: TTabType;
begin
  GridPrograms.SetFocus;
  TabURLs.DisableControls;
  UpdatingTables := True;

  try
    //1. Get current data
    current           := TabURLs.GetBookmark;
    currHasUserID     := not TabURLs.FieldByName('UserID').IsNull;
    CurrUserID        := TabURLs.FieldByName('UserID').AsInteger;
    CurrCaption       := TabURLs.FieldByName('TabCaption').AsString;
    CurrKind          := TabURLs.FieldByName('Kind').AsString;
    CurrURL           := TabURLs.FieldByName('URL_Prog').AsString;
    CurrParams        := TabURLs.FieldByName('Params').AsString;
    CurrWaitMS        := TabURLs.FieldByName('WaitMS').AsInteger;
    currHideFrames    := TabURLs.FieldByName('HideFrames').AsBoolean;
    currLoadAtStartup := TabURLs.FieldByName('LoadAtStartup').AsBoolean;
    currAutoKill      := TabURLs.FieldByName('AutoKill').AsBoolean;
    currDownloadPath  := TabURLs.FieldByName('DownloadPath').AsString;
    currTabUser       := TabURLs.FieldByName('TabUser').AsString;
    currTabPassword   := TabURLs.FieldByName('TabPassword').AsString;
    currAllowOpenDlg  := nbNull;
    if not TabURLs.FieldByName('AllowOpenDlg').IsNull then
    begin
      if TabURLs.FieldByName('AllowOpenDlg').AsBoolean then
        currAllowOpenDlg := nbTrue
      else
        currAllowOpenDlg := nbFalse;
    end;

    //2. Move to the previous record
    currIdx := TabURLs.RecNo;
    TabURLs.First;
    while TabURLs.RecNo <> (currIdx - 1) do
      TabURLs.Next;

    //3. Get previous record data
    prev              := TabURLs.GetBookmark;
    prevHasUserID     := not TabURLs.FieldByName('UserID').IsNull;
    prevUserID        := TabURLs.FieldByName('UserID').AsInteger;
    prevCaption       := TabURLs.FieldByName('TabCaption').AsString;
    prevKind          := TabURLs.FieldByName('Kind').AsString;
    prevURL           := TabURLs.FieldByName('URL_Prog').AsString;
    prevParams        := TabURLs.FieldByName('Params').AsString;
    prevWaitMS        := TabURLs.FieldByName('WaitMS').AsInteger;
    prevHideFrames    := TabURLs.FieldByName('HideFrames').AsBoolean;
    prevLoadAtStartup := TabURLs.FieldByName('LoadAtStartup').AsBoolean;
    prevAutoKill      := TabURLs.FieldByName('AutoKill').AsBoolean;
    prevDownloadPath  := TabURLs.FieldByName('DownloadPath').AsString;
    prevTabUser       := TabURLs.FieldByName('TabUser').AsString;
    prevTabPassword   := TabURLs.FieldByName('TabPassword').AsString;
    prevAllowOpenDlg  := nbNull;
    if not TabURLs.FieldByName('AllowOpenDlg').IsNull then
    begin
      if TabURLs.FieldByName('AllowOpenDlg').AsBoolean then
        prevAllowOpenDlg := nbTrue
      else
        prevAllowOpenDlg := nbFalse;
    end;

    //4. Update previous record with current record data
    TabURLs.Edit;
    if currHasUserID then
      TabURLs.FieldByName('UserID').AsInteger := currUserID
    else
      TabURLs.FieldByName('UserID').Clear;

    TabURLs.FieldByName('TabCaption').AsString     := CurrCaption;
    TabURLs.FieldByName('Kind').AsString           := CurrKind;
    TabURLs.FieldByName('URL_Prog').AsString       := CurrURL;
    TabURLs.FieldByName('Params').AsString         := CurrParams;
    TabURLs.FieldByName('WaitMS').AsInteger        := CurrWaitMS;
    TabURLs.FieldByName('HideFrames').AsBoolean    := currHideFrames;
    TabURLs.FieldByName('LoadAtStartup').AsBoolean := currLoadAtStartup;
    TabURLs.FieldByName('AutoKill').AsBoolean      := currAutoKill;
    TabURLs.FieldByName('DownloadPath').AsString   := currDownloadPath;
    TabURLs.FieldByName('TabUser').AsString        := currTabUser;
    TabURLs.FieldByName('TabPassword').AsString    := currTabPassword;
    case currAllowOpenDlg of
      nbNull:  TabURLs.FieldByName('AllowOpenDlg').Clear;
      nbFalse,
      nbTrue:  TabURLs.FieldByName('AllowOpenDlg').AsBoolean := currAllowOpenDlg = nbTrue;
    end;
    TabURLs.Post;

    //5. Back to current record
    TabURLs.GotoBookmark(current);

    //6. Update current record with previous record data
    TabURLs.Edit;
    if prevHasUserID then
      TabURLs.FieldByName('UserID').AsInteger := prevUserID
    else
      TabURLs.FieldByName('UserID').Clear;

    TabURLs.FieldByName('TabCaption').AsString     := prevCaption;
    TabURLs.FieldByName('Kind').AsString           := prevKind;
    TabURLs.FieldByName('URL_Prog').AsString       := prevURL;
    TabURLs.FieldByName('Params').AsString         := prevParams;
    TabURLs.FieldByName('WaitMS').AsInteger        := prevWaitMS;
    TabURLs.FieldByName('HideFrames').AsBoolean    := prevHideFrames;
    TabURLs.FieldByName('LoadAtStartup').AsBoolean := prevLoadAtStartup;
    TabURLs.FieldByName('AutoKill').AsBoolean      := prevAutoKill;
    TabURLs.FieldByName('DownloadPath').AsString   := prevDownloadPath;
    TabURLs.FieldByName('TabUser').AsString        := prevTabUser;
    TabURLs.FieldByName('TabPassword').AsString    := prevTabPassword;
    case prevAllowOpenDlg of
      nbNull:  TabURLs.FieldByName('AllowOpenDlg').Clear;
      nbFalse,
      nbTrue:  TabURLs.FieldByName('AllowOpenDlg').AsBoolean := prevAllowOpenDlg = nbTrue;
    end;
    TabURLs.Post;

    //7. Set the previous record as new current record
    TabURLs.GotoBookmark(prev);

    //8. Set the new current values
    ui := FUserList.IndexOf (SelectedUserID);
    Dec (currIdx);

    Kind := ttNone;
    if prevKind = 'W' then
      Kind := ttURL
    else
      Kind := ttProgram;

    FUserList.Items [ui].Tabs [currIdx].TabCaption      := prevCaption;
    FUserList.Items [ui].Tabs [currIdx].Kind            := Kind;
    FUserList.Items [ui].Tabs [currIdx].URLProgram      := prevURL;
    FUserList.Items [ui].Tabs [currIdx].Params          := prevParams;
    FUserList.Items [ui].Tabs [currIdx].WaitMS          := prevWaitMS;
    FUserList.Items [ui].Tabs [currIdx].HideFrames      := prevHideFrames;
    FUserList.Items [ui].Tabs [currIdx].LoadAtStartup   := prevLoadAtStartup;
    FUserList.Items [ui].Tabs [currIdx].AutoKill        := prevAutoKill;
    FUserList.Items [ui].Tabs [currIdx].DownloadPath    := prevDownloadPath;
    FUserList.Items [ui].Tabs [currIdx].TabUser         := prevTabUser;
    FUserList.Items [ui].Tabs [currIdx].TabPassword     := prevTabPassword;
    FUserList.Items [ui].Tabs [currIdx].AllowOpenDialog := prevAllowOpenDlg;

    //9. Set the new previous values
    Kind := ttNone;
    if currKind = 'W' then
      Kind := ttURL
    else
      Kind := ttProgram;

    FUserList.Items [ui].Tabs [currIdx - 1].TabCaption      := currCaption;
    FUserList.Items [ui].Tabs [currIdx - 1].Kind            := Kind;
    FUserList.Items [ui].Tabs [currIdx - 1].URLProgram      := currURL;
    FUserList.Items [ui].Tabs [currIdx - 1].Params          := currParams;
    FUserList.Items [ui].Tabs [currIdx - 1].WaitMS          := currWaitMS;
    FUserList.Items [ui].Tabs [currIdx - 1].HideFrames      := currHideFrames;
    FUserList.Items [ui].Tabs [currIdx - 1].LoadAtStartup   := currLoadAtStartup;
    FUserList.Items [ui].Tabs [currIdx - 1].AutoKill        := currAutoKill;
    FUserList.Items [ui].Tabs [currIdx - 1].DownloadPath    := currDownloadPath;
    FUserList.Items [ui].Tabs [currIdx - 1].TabUser         := currTabUser;
    FUserList.Items [ui].Tabs [currIdx - 1].TabPassword     := currTabPassword;
    FUserList.Items [ui].Tabs [currIdx - 1].AllowOpenDialog := currAllowOpenDlg;
  finally
    TabURLs.FreeBookmark(prev);
    TabURLs.FreeBookmark(current);
    TabURLs.EnableControls;
    GridPrograms.SetFocus;
    UpdatingTables := False;
    btnOK.Enabled  := True;
  end;
end;


procedure TFSetup.cxButton5Click(Sender: TObject);
var
  UserDlg: TUserDlg;
  sourceUserID, sourceUserIdx, targetUserID: integer;
begin
  UserDlg := TUserDlg.Create(Self, udaCloneUser);

  UserDlg.OldPassword := TabUsers.FieldByName('Password').AsString;
  UserDlg.NewPassword := TabUsers.FieldByName('Password').AsString;
  UserDlg.edUserName.Text := TabUsers.FieldByName('UserName').AsString;
  UserDlg.nbIdleTime.ValueInt := TabUsers.FieldByName('IdleTime').AsInteger;
  sourceUserID              := TabUsers.FieldByName('UserID').AsInteger;
  sourceUserIdx             := FUserList.IndexOf(sourceUserID);
  try
  if UserDlg.ShowModal = mrOK then
    begin

    targetUserID := GetNextUserID;
    FUserList.Items [sourceUserIdx].CloneUser(targetUserID, UserDlg.edUserName.Text, UserDlg.NewPassword, UserDlg.nbIdleTime.ValueInt);
    PopulateUserSelection;
    UpdateTables;

    MessageDlg (Format ('User %s created', [UserDlg.edUserName.Text]), mtInformation, [mbOK], 0);
    end;
  finally
    UserDlg.Free;
  end;
{
var
  sourceUserID, sourceUserIdx, targetUserID: integer;
begin
  FAddEditUser.OriginalUser := TabUsers.FieldByName('UserName').AsString;
  FAddEditUser.UserAction   := uaCloneUser;
  sourceUserID              := TabUsers.FieldByName('UserID').AsInteger;
  FAddEditUser.nbIdleTime.value   := TabUsers.FieldByName('IdleTime').AsInteger;
  sourceUserIdx             := FUserList.IndexOf(sourceUserID);
  if FAddEditUser.ShowModal = mrOk then
  begin
    targetUserID := GetNextUserID;
    FUserList.Items [sourceUserIdx].CloneUser(targetUserID, FAddEditUser.edUserName.Text, FAddEditUser.edPassword.Text, FAddEditUser.nbIdleTime.ValueInt);
    PopulateUserSelection;
    UpdateTables;
    btnOK.Enabled := True;

    MessageDlg (Format ('User %s created', [FAddEditUser.edUserName.Text]), mtInformation, [mbOK], 0);
  end;}

end;


procedure TFSetup.btnCloneUserClick(Sender: TObject);
var
  sourceUserID, sourceUserIdx, targetUserID: integer;
begin
  FAddEditUser.OriginalUser := TabUsers.FieldByName('UserName').AsString;
  FAddEditUser.UserAction   := uaCloneUser;
  sourceUserID              := TabUsers.FieldByName('UserID').AsInteger;
  FAddEditUser.nbIdleTime.value   := TabUsers.FieldByName('IdleTime').AsInteger;
  sourceUserIdx             := FUserList.IndexOf(sourceUserID);
  if FAddEditUser.ShowModal = mrOk then
  begin
    targetUserID := GetNextUserID;
    FUserList.Items [sourceUserIdx].CloneUser(targetUserID, FAddEditUser.edUserName.Text, FAddEditUser.edPassword.Text, FAddEditUser.nbIdleTime.ValueInt);
    PopulateUserSelection;
    UpdateTables;
    btnOK.Enabled := True;

    MessageDlg (Format ('User %s created', [FAddEditUser.edUserName.Text]), mtInformation, [mbOK], 0);
  end;
end;

procedure TFSetup.cxButton3Click(Sender: TObject);
var
  UserDlg: TUserDlg;
begin
  UserDlg := TUserDlg.Create(Self, udaAdd);
  try
  if UserDlg.ShowModal = mrOK then
    begin
    TabUsers.Append;
    TabUsers.FieldByName('UserName').AsString := UserDlg.edUserName.Text;
    TabUsers.FieldByName('Password').AsString := UserDlg.NewPassword;
    TabUsers.FieldByName('IdleTime').AsInteger := UserDlg.nbIdleTime.ValueInt;
    TabUsers.Post;

    MessageDlg (Format ('User %s created', [UserDlg.edUserName.Text]), mtInformation, [mbOK], 0);
    end;
  finally
    UserDlg.Free;
  end;
end;

procedure TFSetup.cxButton4Click(Sender: TObject);
var
  UserDlg: TUserDlg;
begin
  UserDlg := TUserDlg.Create(Self, udaEdit);
  UserDlg.OldPassword := TabUsers.FieldByName('Password').AsString;
  UserDlg.NewPassword := TabUsers.FieldByName('Password').AsString;
  UserDlg.edUserName.Text := TabUsers.FieldByName('UserName').AsString;
  UserDlg.nbIdleTime.ValueInt := TabUsers.FieldByName('IdleTime').AsInteger;
  try
  if UserDlg.ShowModal = mrOK then
    begin
    TabUsers.Edit;
    TabUsers.FieldByName('UserName').AsString := UserDlg.edUserName.Text;
    TabUsers.FieldByName('Password').AsString := UserDlg.NewPassword;
    TabUsers.FieldByName('IdleTime').AsInteger := UserDlg.nbIdleTime.ValueInt;
    TabUsers.Post;

    MessageDlg (Format ('User %s Updated', [UserDlg.edUserName.Text]), mtInformation, [mbOK], 0);
    end;
  finally
    UserDlg.Free;
  end;

end;



procedure TFSetup.btnAddUserClick(Sender: TObject);
begin
  FAddEditUser.UserAction := uaAdd;
  if FAddEditUser.ShowModal = mrOk then
  begin
    TabUsers.Append;
    TabUsers.FieldByName('UserName').AsString := FAddEditUser.edUserName.Text;
    TabUsers.FieldByName('Password').AsString := FAddEditUser.edPassword.Text;
    TabUsers.FieldByName('IdleTime').AsInteger := FAddEditUser.nbIdleTime.ValueInt;
    TabUsers.Post;

    MessageDlg (Format ('User %s created', [FAddEditUser.edUserName.Text]), mtInformation, [mbOK], 0);
  end;
end;

procedure TFSetup.btnCancelClick(Sender: TObject);
begin
  TabURLs.Cancel;
  TabParams.Cancel;
  MainForm.UserID := -1;
  ModalResult     := mrCancel;
end;



procedure TFSetup.btnResetPasswordClick(Sender: TObject);
begin
   FAddEditUser.UserAction      := uaResetPassword;
   FAddEditUser.edUserName.Text := TabUsers.FieldByName('UserName').AsString;
   if FAddEditUser.ShowModal = mrOk then
   begin
     TabUsers.Edit;
     TabUsers.FieldByName('Password').AsString := FAddEditUser.edPassword.Text;
     TabUsers.Post;

     MessageDlg ('Password updated', mtInformation, [mbOK], 0);
   end;
end;

procedure TFSetup.btnDefaultDownloadPathClick(Sender: TObject);
begin
  FolderDlg.Directory := edDefaultDownloadPath.Text;
  if FolderDlg.Execute then
    edDefaultDownloadPath.Text := FolderDlg.Directory;
end;

procedure TFSetup.btnDeleteUserClick(Sender: TObject);
var
  i, UserID, ui: integer;
begin
  if MessageDlg (Format ('Are you sure to delete the [%s] user, and its tabs and paramters?', [TabUsers.FieldByName ('UserName').AsString]), mtConfirmation, mbYesNo, 0) = mrYes then
  begin
    UserID := TabUsers.FieldByName ('UserID').AsInteger;
    ui     := FUserList.IndexOf(UserID);
    FUserList.Delete(ui);
    PopulateUserSelection;
    UpdateTables;
    MessageDlg ('User deleted', mtInformation, [mbOK], 0);
    btnOK.Enabled := True;
  end;
end;

procedure TFSetup.btnOKClick(Sender: TObject);
begin
  if TabURLs.State in dsEditModes then
    begin
     MessageDlg('Some data changed!, Please save or cancel it first', mtWarning, [mbOK], 0);
     exit;
    end;

  if (edPassword.Tag = 1) and (edPassword.Text <> edPasswordValidation.Text) then
  begin
    MessageDlg ('User password and validation not match', mtWarning, [mbOK], 0);
    edPassword.SetFocus;
    exit;
  end;

  MainForm.UserID := -1;
  ModalResult     := mrOK;
end;

procedure TFSetup.btnSetNullClick(Sender: TObject);
begin
  if (TabURLs.State = dsBrowse) and (TabURLs.RecordCount > 0) then
  begin
    TabUrls.Edit;
    TabUrls.FieldByName('AllowOpenDlg').Clear;
    TabUrls.Post;
    GridPrograms.SetFocus;
  end;
end;

procedure TFSetup.cbSelectedUserPropertiesChange(Sender: TObject);

begin
  UpdateTables;



 var
    enbl: Boolean;
  enbl := cbSelectedUser.ItemIndex > 0;


  for var i: integer := 0 to Panel3.ControlCount -1 do
    begin

    if IsPublishedProp(Panel3.Controls[i], 'ReadOnly') then
    begin
       SetPropValue(Panel3.Controls[i], 'ReadOnly', not enbl);
    end;

    if IsPublishedProp(Panel3.Controls[i], 'Enabled') then
         SetPropValue(Panel3.Controls[i], 'Enabled', enbl);

    if panel3.Controls[i] is TcxTextEdit then
      (panel3.Controls[i] as TcxTextEdit).Properties.ReadOnly := not enbl;

    if panel3.Controls[i] is TDBLookupControl then
      (panel3.Controls[i] as TDBLookupControl).Enabled := enbl;

    end;



end;

procedure TFSetup.cbSkinNamePropertiesChange(Sender: TObject);
begin
  if MainForm.Loading then exit;

  btnOK.Enabled := True;
  UpdateSkin;
end;

procedure TFSetup.UpdateSkin;
begin
  {$ifdef Useskins}
  if FSkinsFound and FSkinOK and (cbSkinName.Text <> '') then
  begin
    try
      dxSkinsUserSkinLoadFromFile(FSkinFile, cbSkinName.Text);
    except
      begin
        FSkinOK := False;
        if MainForm.SetupMode then
          MessageDlg ('Invalid AllSkins.skinres version, it is necessary to use the skinres obtained from the version of devexpress that the application was compiled',
                      mtWarning, [mbOK], 0)
        else
          MessageDlg ('A problem has occurred with the skins file, please contact the system administrator', mtWarning, [mbOK], 0)
      end;
    end;
  end;
  {$endif}
end;


procedure TFSetup.ControlChanged(Sender: TObject);
begin
  if MainForm.Loading then exit;

  btnOK.Enabled := True;
end;

procedure TFSetup.cxButton1Click(Sender: TObject);
begin
  SelectProgram;
end;

procedure TFSetup.cxButton2Click(Sender: TObject);
begin
  OpenDlg.InitialDir := ExtractFilePath (ParamStr (0));
    OpenDlg.FileName := TabCommands.FieldByName('exe').AsString;
    if OpenDlg.Execute (Self.Handle) then
    begin
      if TabCommands.State = dsBrowse then
        TabCommands.Edit;

      TabCommands.FieldByName('exe').AsString := OpenDlg.FileName;
    end;

end;



procedure TFSetup.cxDBButtonEdit1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  SelectDownloadPath;
end;

procedure TFSetup.dbedProgParamsChange(Sender: TObject);
begin
  UpdateParamsPreview;
end;

procedure TFSetup.dbedURL_ProgramChange(Sender: TObject);
begin
  UpdateParamsPreview;
end;

procedure TFSetup.dbedURL_ProgramKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then Key := 0;
end;

procedure TFSetup.UpdateParamsPreview;
var
  ui: integer;
begin
  if MainForm.Loading or not Showing or UpdatingTables then exit;

  if not Showing or not TabURLs.Active or (TabURLs.RecordCount = 0) then
  begin
    MainForm.UserID := -1;
    ui              := 0;
  end
  else
  begin
    MainForm.UserID := TabURLs.FieldByName('UserID').AsInteger;
    ui              := FUserList.IndexOf (TabURLs.FieldByName('UserID').AsInteger);
  end;

  //UpdateParams;
  UpdateCompletionProposals;

  if  (TabURLs.RecordCount > 0) and (TabURLs.State in [dsBrowse, dsInsert, dsEdit]) then
  begin
    btnSetNull.Enabled := not TabUrls.FieldByName('AllowOpenDlg').IsNull;

    labParams.Caption := '';
    edParams.Text     := '';

    if      TabURLs.FieldByName ('Kind').AsString = 'W' then
    begin
      labParams.Caption := 'URL preview';
      edParams.Text := MainForm.ReplaceParams(dbedURL_Program.Text,
                                              TabURLs.FieldByName('TabUser').AsString,
                                              TabURLs.FieldByName('TabPassword').AsString,
                                              FUserList.Items [ui].Params);
    end
    else if TabURLs.FieldByName ('Kind').AsString = 'P' then
    begin
      labParams.Caption := 'Program parameters preview';
      edParams.Text := MainForm.ReplaceParams(dbedProgParams.Text,
                                              TabURLs.FieldByName('TabUser').AsString,
                                              TabURLs.FieldByName('TabPassword').AsString,
                                              FUserList.Items [ui].Params);
    end
  end;
end;

procedure TFSetup.dsCommandsStateChange(Sender: TObject);
begin
  if TabCommands.State in dsEditModes then
    TabURLs.Edit;
  btnOK.Enabled := not((TabUsers.State in dsEditModes) or
    (TabCommands.State in dsEditModes) or (TabUsers.State in dsEditModes));
end;

procedure TFSetup.dsURLsDataChange(Sender: TObject; Field: TField);
begin
  if Showing and TabURLs.Active and (TabURLs.RecordCount > 0) then
    MainForm.UserID := TabURLs.FieldByName('UserID').AsInteger;

  dbedProgParams.ReadOnly := not TabURLs.Active or (TabURLs.FieldByName('Kind').AsString <> 'P');
  UpdateParamsPreview;
end;

procedure TFSetup.dsURLsStateChange(Sender: TObject);
begin
  btnOK.Enabled := not((TabUsers.State in dsEditModes) or
    (TabCommands.State in dsEditModes) or (TabUsers.State in dsEditModes));
end;

procedure TFSetup.dsUsersStateChange(Sender: TObject);
begin
  btnAddUser.Enabled       := TabUsers.Active and (TabUsers.State = dsBrowse);
  btnResetPassword.Enabled := TabUsers.Active and (TabUsers.State = dsBrowse) and (TabUsers.RecordCount > 0);
  btnDeleteUser.Enabled    := TabUsers.Active and (TabUsers.State = dsBrowse) and (TabUsers.RecordCount > 0);
  btnCloneUser.Enabled     := TabUsers.Active and (TabUsers.State = dsBrowse) and (TabUsers.RecordCount > 0);

  btnOK.Enabled := not((TabUsers.State in dsEditModes) or
    (TabCommands.State in dsEditModes) or (TabUsers.State in dsEditModes));
end;

procedure TFSetup.edExpirationDatePropertiesChange(Sender: TObject);
begin
  if MainForm.Loading then exit;

  btnOK.Enabled        := True;
  edExpirationDate.Tag := 0;
end;

procedure TFSetup.edPasswordPropertiesChange(Sender: TObject);
begin
  if MainForm.Loading then exit;

  btnOK.Enabled               := True;
  labPasswordNotMatch.Visible := edPassword.Text <> edPasswordValidation.Text;
  edPassword.Tag              := 1;
end;

procedure TFSetup.edPasswordValidationPropertiesChange(Sender: TObject);
begin
  if MainForm.Loading then exit;

  labPasswordNotMatch.Visible := edPassword.Text <> edPasswordValidation.Text;
end;

procedure TFSetup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MainForm.UserID := -1;
end;

procedure TFSetup.FormCreate(Sender: TObject);
begin
  tabCmdShow.Close;
  tabCmdShow.Open;
  tabCmdShow.EmptyDataSet;
  tabCmdShow.InsertRecord([0,'HIDE']);
  tabCmdShow.InsertRecord([1,'SHOW NORMAL']);
  tabCmdShow.InsertRecord([2,'SHOW MINIMIZED']);
  tabCmdShow.InsertRecord([3,'SHOW MAXIMIZED']);
  tabCmdShow.InsertRecord([4,'SHOW (NOT ACTIVATE)']);
  tabCmdShow.InsertRecord([5,'SHOW']);
  tabCmdShow.InsertRecord([6,'MINIMIZE']);
  tabCmdShow.InsertRecord([7,'SHOW MINIMIZED (NOT ACTIVE)']);
  tabCmdShow.InsertRecord([8,'SHOW (NOT ACTIVE)']);
  tabCmdShow.InsertRecord([9,'RESTORE']);
  tabCmdShow.InsertRecord([10,'SHOW DEFAULT']);
  tabCmdShow.InsertRecord([11,'FORCE MINIMIZE']);


  FUserList      := TUserCollection.Create(TUserItem);
  FSkinOK        := True;
  FNextUserID    := 0;
  Loading        := False;
  UpdatingTables := False;
  FLastTabOp     := toNone;
  FLastTabIdx    := -1;
  FLastParamOp   := toNone;
  FLastParamIdx  := -1;
  FLastUserOp    := toNone;
  FLastUserIdx   := -1;
  TabURLs.Open;
  TabParams.Open;
  TabCommands.Open;
end;

procedure TFSetup.FormDestroy(Sender: TObject);
begin
  FUserList.Free;
end;

procedure TFSetup.FormShow(Sender: TObject);
begin
  PageControl.ActivePage := TabGeneralSettings;
  btnOK.Enabled          := False;
  edPassword.Tag         := 0;

  PopulateUserSelection;
  UpdateTables;

  LoadSkins (False);
  UpdateParamsPreview;
end;

procedure TFSetup.LoadSkins (DisplayMessage: boolean = True);
begin
  {$ifdef Useskins}
  FSkinFile   := ExtractFilePath(ParamStr (0)) + 'AllSkins.skinres';
  FSkinsFound := FileExists(FSkinFile);

  if FSkinsFound then
  begin
    try
      if FSkinOK then
        dxSkinsUserSkinPopulateSkinNames(FSkinFile, cbSkinName.Properties.Items, True);
    except
      begin
        FSkinOK := False;
        if MainForm.SetupMode then
          MessageDlg ('Invalid AllSkins.skinres version, it is necessary to use the skinres obtained from the version of devexpress that the application was compiled',
                      mtWarning, [mbOK], 0)
        else
          MessageDlg ('A problem has occurred with the skins file, please contact the system administrator', mtWarning, [mbOK], 0)
      end;
    end;
  end
  else
  begin
    cbSkinName.Enabled := False;

    if DisplayMessage then
      MessageDlg ('Skins file [AllSkins.skinres] not found', mtWarning, [mbOK], 0);
  end;
  {$else}
  FSkinsFound := False;
  {$endif}
end;

procedure TFSetup.PageControlPageChanging(Sender: TObject; NewPage: TcxTabSheet;
  var AllowChange: Boolean);
begin
  AllowChange := not (TabURLs.State in [dsInsert, dsEdit]) and not (TabParams.State in [dsInsert, dsEdit]);
end;

procedure TFSetup.SelectDownloadPath;
begin
  if (TabURLs.State in [dsEdit, dsInsert, dsBrowse]) and (TabURLs.FieldByName ('Kind').AsString = 'W') then
  begin
    FolderDlg.Directory := TabURLs.FieldByName('DownloadPath').AsString;
    if FolderDlg.Execute then
    begin
      if TabURLs.State = dsBrowse then
        TabURLs.Edit;

      TabURLs.FieldByName('DownloadPath').AsString := FolderDlg.Directory;
    end;
  end;
end;

procedure TFSetup.GridDBTableView1DownloadPathPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SelectDownloadPath;
end;

procedure TFSetup.SelectProgram;
begin
  OpenDlg.InitialDir := ExtractFilePath (ParamStr (0));
  if (TabURLs.State in [dsEdit, dsInsert, dsBrowse]) and (TabURLs.FieldByName ('Kind').AsString = 'P') then
  begin
    OpenDlg.FileName := TabURLs.FieldByName('URL_Prog').AsString;
    if OpenDlg.Execute (Self.Handle) then
    begin
      if TabURLs.State = dsBrowse then
        TabURLs.Edit;

      TabURLs.FieldByName('URL_Prog').AsString := ExtractFileName (OpenDlg.FileName);
    end;
  end;
end;

procedure TFSetup.GridDBTableView1URL_ProgPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  SelectProgram;
end;



procedure TFSetup.TabParamsAfterDelete(DataSet: TDataSet);
var
  ui: integer;
begin
  if MainForm.Loading then exit;

  if not UpdatingTables and (FLastParamOp = toDelete) and (FLastParamIdx > -1) then
  begin
    if TabParams.FieldByName('UserID').IsNull then
      ui := 0
    else
      ui := FUserList.IndexOf(TabParams.FieldByName('UserID').AsInteger);

    FUserList.Items [ui].Params.Delete(FLastParamIdx);
  end;

  //UpdateParams;
  UpdateCompletionProposals;

  if not UpdatingTables then
    btnOK.Enabled := True;
end;

procedure TFSetup.TabParamsAfterPost(DataSet: TDataSet);
var
  ui, currIdx: integer;
begin
  if MainForm.Loading or UpdatingTables then exit;

  if TabParams.FieldByName('UserID').IsNull then
    ui := 0
  else
    ui := FUserList.IndexOf(TabParams.FieldByName('UserID').AsInteger);
  currIdx := TabParams.RecNo - 1;

  if SelectedUserID = TabParams.FieldByName('UserID').AsInteger then
  begin
    case FLastParamOp of
      toEdit: begin
        FUserList.Items [ui].Params.Items [currIdx].FKey   := TabParams.FieldByName('Key').AsString;
        FUserList.Items [ui].Params.Items [currIdx].FValue := TabParams.FieldByName('Value').AsString;
      end;
      toInsert: begin
        FUserList.Items [ui].Params.AddKey(TabParams.FieldByName('Key').AsString,
                                           TabParams.FieldByName('Value').AsString);
      end;
    end;
  end
  else
  begin
    ui := FUserList.IndexOf(SelectedUserID);
    if FLastParamOp = toEdit then
      FUserList.Items [ui].Params.Delete(currIdx);

    if TabParams.FieldByName('UserID').IsNull then
      ui := 0
    else
      ui := FUserList.IndexOf(TabParams.FieldByName('UserID').AsInteger);

    FUserList.Items [ui].Params.AddKey(TabParams.FieldByName('Key').AsString,
                                       TabParams.FieldByName('Value').AsString);
    UpdateTables;
  end;
  FLastParamOp  := toNone;
  FLastParamIdx := -1;

  UpdateCompletionProposals;

  if not UpdatingTables then
    btnOK.Enabled := True;
end;

procedure TFSetup.TabParamsBeforeDelete(DataSet: TDataSet);
begin
  FLastParamOp  := toDelete;
  FLastParamIdx := TabParams.RecNo - 1;
end;

procedure TFSetup.TabParamsBeforePost(DataSet: TDataSet);
begin
  FLastParamOp := toNone;

  if UpdatingTables then exit;

  case TabParams.State of
    dsInsert: FLastParamOp := toInsert;
    dsEdit:   FLastParamOp := toEdit;
  end;
end;

procedure TFSetup.TabParamsNewRecord(DataSet: TDataSet);
begin
  if MainForm.Loading or UpdatingTables or (cbSelectedUser.ItemIndex <= 1) then exit;

  TabParams.FieldByName('UserID').AsInteger := SelectedUserID;
end;

procedure TFSetup.TabURLsAfterDelete(DataSet: TDataSet);
var
  ui: integer;
begin
  if MainForm.Loading then exit;

  if not UpdatingTables and (FLastTabOp = toDelete) and (FLastTabIdx > -1) then
  begin
    if TabURLs.FieldByName('UserID').IsNull then
      ui := 0
    else
      ui := FUserList.IndexOf(TabURLs.FieldByName('UserID').AsInteger);

    FUserList.Items [ui].Tabs.Delete(FLastTabIdx);
  end;

  UpdateCompletionProposals;

  if not UpdatingTables then
    btnOK.Enabled := True;
end;

procedure TFSetup.TabURLsAfterPost(DataSet: TDataSet);
var
  ui, currIdx: integer;
  Kind: TTabType;
  currAllowOpenDlg: TNullableBoolean;
begin
  if MainForm.Loading or UpdatingTables then exit;

  if TabURLs.FieldByName('UserID').IsNull then
    ui := 0
  else
    ui := FUserList.IndexOf(TabURLs.FieldByName('UserID').AsInteger);
  currIdx := TabURLs.RecNo - 1;

  Kind := ttNone;
  if TabURLs.FieldByName('Kind').AsString = 'W' then
    Kind := ttURL
  else
    Kind := ttProgram;

  currAllowOpenDlg  := nbNull;
  if not TabURLs.FieldByName('AllowOpenDlg').IsNull then
  begin

    if TabURLs.FieldByName('AllowOpenDlg').AsBoolean then
      currAllowOpenDlg := nbTrue
    else
      currAllowOpenDlg := nbFalse;
  end;


  var CmdList: TList<TCommandInfo>;
  CmdList := TList<TCommandInfo>.Create;

  //TabCommands.Open;
  TabCommands.First;
  var
    info:TCommandInfo;
  while not TabCommands.eof do
    begin
    info.UrlID := TabURLs.FieldByName('UrlID').AsString;
    info.Caption := TabCommands.FieldByName('Caption').AsString;
    info.Mode := TabCommands.FieldByName('Mode').AsInteger;
    info.exe := TabCommands.FieldByName('exe').AsString;
    info.Params := TabCommands.FieldByName('Params').AsString;
    CmdList.Add(info);

    TabCommands.Next;
    end;

  if SelectedUserID = TabURLs.FieldByName('UserID').AsInteger then
  begin
    case FLastTabOp of
      toEdit: begin
        FUserList.Items [ui].Tabs.Items [currIdx].FTabCaption      := TabURLs.FieldByName('TabCaption').AsString;
        FUserList.Items [ui].Tabs.Items [currIdx].FKind            := Kind;
        FUserList.Items [ui].Tabs.Items [currIdx].FURLProgram      := TabURLs.FieldByName('URL_Prog').AsString;
        FUserList.Items [ui].Tabs.Items [currIdx].FParams          := TabURLs.FieldByName('Params').AsString;
        FUserList.Items [ui].Tabs.Items [currIdx].FWaitMS          := TabURLs.FieldByName('WaitMS').AsInteger;
        FUserList.Items [ui].Tabs.Items [currIdx].FHideFrames      := TabURLs.FieldByName('HideFrames').AsBoolean;
        FUserList.Items [ui].Tabs.Items [currIdx].FLoadAtStartup   := TabURLs.FieldByName('LoadAtStartup').AsBoolean;
        FUserList.Items [ui].Tabs.Items [currIdx].FAutoKill        := TabURLs.FieldByName('AutoKill').AsBoolean;
        FUserList.Items [ui].Tabs.Items [currIdx].FDownloadPath    := TabURLs.FieldByName('DownloadPath').AsString;
        FUserList.Items [ui].Tabs.Items [currIdx].FAllowOpenDialog := currAllowOpenDlg;
        FUserList.Items [ui].Tabs.Items [currIdx].FTabUser         := TabURLs.FieldByName('TabUser').AsString;
        FUserList.Items [ui].Tabs.Items [currIdx].FTabCaption      := TabURLs.FieldByName('TabCaption').AsString;
        FUserList.Items [ui].Tabs.Items [currIdx].CommandList      := CmdList;
      end;
      toInsert: begin

        FUserList.Items [ui].Tabs.AddTab(TabURLs.FieldByName('TabCaption').AsString,
                                         Kind,
                                         TabURLs.FieldByName('URL_Prog').AsString,
                                         TabURLs.FieldByName('Params').AsString,
                                         TabURLs.FieldByName('WaitMS').AsInteger,
                                         TabURLs.FieldByName('HideFrames').AsBoolean,
                                         TabURLs.FieldByName('LoadAtStartup').AsBoolean,
                                         TabURLs.FieldByName('AutoKill').AsBoolean,
                                         TabURLs.FieldByName('DownloadPath').AsString,
                                         currAllowOpenDlg,
                                         TabURLs.FieldByName('TabUser').AsString,
                                         TabURLs.FieldByName('TabCaption').AsString,
                                         CmdList);
      end;
    end;
  end
  else
  begin
    ui := FUserList.IndexOf(SelectedUserID);
    if FLastTabOp = toEdit then
      FUserList.Items [ui].Tabs.Delete(currIdx);

    if TabURLs.FieldByName('UserID').IsNull then
      ui := 0
    else
      ui := FUserList.IndexOf(TabURLs.FieldByName('UserID').AsInteger);

   { var CmdList: TList<TCommandInfo>;
    CmdList := TList<TCommandInfo>.Create;
    TabCommands.Open;
    TabCommands.First;
    while not TabCommands.eof do
      begin
      var
        info: TCommandInfo;
        info.UrlID := TabCommands['UrlID'];
        info.Caption := TabCommands['Caption'];
        info.Mode := TabCommands['Mode'];
        info.exe := TabCommands['exe'];
        info.Params := TabCommands['Params'];
      CmdList.add(info);
      TabCommands.Next;
      end;  }


    FUserList.Items [ui].Tabs.AddTab(TabURLs.FieldByName('TabCaption').AsString,
                                     Kind,
                                     TabURLs.FieldByName('URL_Prog').AsString,
                                     TabURLs.FieldByName('Params').AsString,
                                     TabURLs.FieldByName('WaitMS').AsInteger,
                                     TabURLs.FieldByName('HideFrames').AsBoolean,
                                     TabURLs.FieldByName('LoadAtStartup').AsBoolean,
                                     TabURLs.FieldByName('AutoKill').AsBoolean,
                                     TabURLs.FieldByName('DownloadPath').AsString,
                                     currAllowOpenDlg,
                                     TabURLs.FieldByName('TabUser').AsString,
                                     TabURLs.FieldByName('TabCaption').AsString,
                                     CmdList);

    UpdateTables;
  end;

//  CmdList.free;
  FLastTabOp  := toNone;
  FLastTabIdx := -1;

  UpdateCompletionProposals;
  btnOK.Enabled := True;
end;

procedure TFSetup.TabCommandsAfterDelete(DataSet: TDataSet);
begin
  TabURLs.edit;
  TabURLs.Post;
end;

procedure TFSetup.TabCommandsAfterEdit(DataSet: TDataSet);
begin
  btnOK.Enabled := true;
end;

procedure TFSetup.TabCommandsAfterInsert(DataSet: TDataSet);
begin

  TabCommands.FieldByName('Mode').AsInteger := 1 // Show_Normal ;
end;

procedure TFSetup.TabCommandsBeforePost(DataSet: TDataSet);
begin
  TabCommands.FieldByName('UrlId').AsString := TabURLs.FieldByName('UrlId').AsString ;
end;

procedure TFSetup.TabCommandsFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  accept := TabCommands.FieldByName('UrlId').AsString = TabURLs.FieldByName('UrlId').AsString ;
end;

procedure TFSetup.TabURLsAfterScroll(DataSet: TDataSet);
var
  ui, currIdx: integer;
begin
  TabCommands.Filtered := false;
  TabCommands.Filter := 'UrlId = ' + QuotedStr(TabURLs.FieldByName('UrlId').AsString);
  TabCommands.Filtered := true;

end;

procedure TFSetup.TabURLsBeforeDelete(DataSet: TDataSet);
begin
  FLastTabOp  := toDelete;
  FLastTabIdx := TabURLs.RecNo - 1;
end;

procedure TFSetup.TabURLsBeforePost(DataSet: TDataSet);
begin
  FLastTabOp := toNone;

  if UpdatingTables then exit;

  case TabURLs.State of
    dsInsert: FLastTabOp := toInsert;
    dsEdit:   FLastTabOp := toEdit;
  end;
end;

procedure TFSetup.TabURLsNewRecord(DataSet: TDataSet);
begin
  if MainForm.Loading or UpdatingTables then exit;

  TabURLs.FieldByName ('WaitMS').AsInteger        := 500;
  TabURLs.FieldByName ('HideFrames').AsBoolean    := True;
  TabURLs.FieldByName ('LoadAtStartup').AsBoolean := False;
  TabURLs.FieldByName ('AutoKill').AsBoolean      := False;

  if cbSelectedUser.ItemIndex > 1 then
    TabURLs.FieldByName('UserID').AsInteger := SelectedUserID;
end;

procedure TFSetup.TabUsersAfterDelete(DataSet: TDataSet);
var
  ui: integer;
begin
  if MainForm.Loading then exit;

  if not UpdatingTables and (FLastUserOp = toDelete) and (FLastUserIdx > -1) then
  begin
    if TabUsers.FieldByName('UserID').IsNull then
      ui := 0
    else
      ui := FUserList.IndexOf(TabUsers.FieldByName('UserID').AsInteger);

    FUserList.Items [ui].Tabs.Delete(FLastUserIdx);
    PopulateUserSelection;
  end;

  if not UpdatingTables then
    btnOK.Enabled := True;
end;

procedure TFSetup.TabUsersAftefrPost(DataSet: TDataSet);
var
  ui, currIdx: integer;
begin
  if MainForm.Loading or UpdatingTables then exit;

  if TabUsers.FieldByName('UserID').IsNull then
    ui := 0
  else
    ui := FUserList.IndexOf(TabUsers.FieldByName('UserID').AsInteger);
  currIdx := TabUsers.RecNo - 1;

  case FLastUserOp of
    toEdit: begin
      FUserList.Items [ui].FUserName := TabUsers.FieldByName('UserName').AsString;
      FUserList.Items [ui].FPassword := TabUsers.FieldByName('Password').AsString;
      FUserList.Items [ui].FIdleTime := TabUsers.FieldByName('IdleTime').AsInteger;
    end;
    toInsert: begin
      FUserList.AddUser (TabUsers.FieldByName('UserID').AsInteger,
                         TabUsers.FieldByName('UserName').AsString,
                         TabUsers.FieldByName('Password').AsString,
                         TabUsers.FieldByName('IdleTime').AsInteger);
      PopulateUserSelection;
    end;
  end;
  FLastUserOp   := toNone;
  FLastUserIdx  := -1;
  btnOK.Enabled := True;
end;

procedure TFSetup.TabUsersBeforeDelete(DataSet: TDataSet);
begin
  FLastUserOp  := toDelete;
  FLastUserIdx := TabUsers.RecNo - 1;
end;

procedure TFSetup.TabUsersBeforePost(DataSet: TDataSet);
begin
  if not MainForm.Loading and not UpdatingTables and (TabUsers.State = dsInsert) then
    TabUsers.FieldByName('UserID').AsInteger := GetNextUserID;

  FLastUserOp := toNone;

  if UpdatingTables then exit;

  case TabUsers.State of
    dsInsert: FLastUserOp := toInsert;
    dsEdit:   FLastUserOp := toEdit;
  end;
end;

procedure TFSetup.UpdateCompletionProposals;
var
  L: TStringList;
  i, ui: integer;
  bm: TBookmark;
begin
  L := EnvSysVarList;

  try
    for i := 0 to L.Count - 1 do
      L [i] := '%' + L [i] + '%';

    if TabURLs.FieldByName ('UserID').IsNull then
      ui := 0
    else
      ui := FUserList.IndexOf (TabURLs.FieldByName('UserID').AsInteger);

    for i := 0 to FUserList.Items [ui].Params.Count - 1 do
      L.Add('$' + FUserList.Items [ui].Params.Items [i].Key);

    SynCompPropURLProg.ItemList.Assign(L);
    SynCompPropProgParams.ItemList.Assign(L);
  finally
    L.Free;
  end;
end;

function TFSetup.UserExists (UserName: string): boolean;
var
  bm: TBookmark;
begin
  result   := False;
  UserName := UpperCase (UserName);
  bm       := nil;

  TabUsers.DisableControls;
  try
    TabUsers.Last;
    if TabUsers.RecordCount > 0 then
    begin
      TabUsers.First;
      while not TabUsers.Eof do
      begin
        if UpperCase (TabUsers.FieldByName ('UserName').AsString) = UserName then
        begin
          result := True;
          exit;
        end;

        TabUsers.Next;
      end;
    end;
  finally
    if Assigned (bm) then
    begin
      TabUsers.GotoBookmark(bm);
      TabUsers.FreeBookmark(bm);
    end;

    TabUsers.EnableControls;
  end;
end;
{$endregion}

destructor TTabItem.Destroy;
begin
  FcommandList.Free;
  inherited;
end;

//function TTabItem.GetCommandList: TList<TCommandInfo>;
//begin
//  if not Assigned(FCommandList) then
//    FCommandList := TList<TCommandInfo>.Create;
//  result :=  FCommandList;
//end;



end.
