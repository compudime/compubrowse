object FramePassword: TFramePassword
  Left = 0
  Top = 0
  Width = 370
  Height = 117
  TabOrder = 0
  object pnlOldPassword: TPanel
    Left = 0
    Top = 0
    Width = 185
    Height = 117
    Align = alLeft
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 0
    object edPreviousPassword: TcxTextEdit
      Left = 10
      Top = 19
      Properties.EchoMode = eemPassword
      TabOrder = 0
      OnKeyPress = edPasswordValidationKeyPress
      Width = 169
    end
    object labPreviousPassword: TcxLabel
      Left = 10
      Top = 3
      Caption = 'Previous password'
      Transparent = True
    end
    object lblIncorrectPassword: TcxLabel
      Left = 10
      Top = 39
      AutoSize = False
      Caption = 'Incorrect password'
      Style.TextColor = clRed
      Transparent = True
      Height = 17
      Width = 143
    end
  end
  object Panel2: TPanel
    Left = 185
    Top = 0
    Width = 185
    Height = 117
    Align = alClient
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 1
    object cxLabel3: TcxLabel
      Left = 6
      Top = 51
      Caption = 'Password validation'
    end
    object edPassword: TcxTextEdit
      Left = 6
      Top = 19
      Properties.EchoMode = eemPassword
      TabOrder = 1
      OnKeyPress = edPasswordValidationKeyPress
      Width = 169
    end
    object edPasswordValidation: TcxTextEdit
      Left = 6
      Top = 67
      Properties.EchoMode = eemPassword
      TabOrder = 2
      OnKeyPress = edPasswordValidationKeyPress
      Width = 169
    end
    object labErrorMsg: TcxLabel
      Left = 6
      Top = 87
      AutoSize = False
      Caption = 'Invalid previous password'
      Style.TextColor = clRed
      Transparent = True
      Height = 17
      Width = 143
    end
    object labPassword: TcxLabel
      Left = 6
      Top = 3
      Caption = 'Password'
      Transparent = True
    end
  end
end
