unit uResetPasswordDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit,
 {$IFDEF UseSkins}
  dxSkinsCore, dxSkinBasic,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkroom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinOffice2019Black, dxSkinOffice2019Colorful,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue,
 {$ENDIF}
  cxTextEdit, cxLabel, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.ExtCtrls, uPassword, dxGDIPlusClasses, cxImage;

type
  TResetPasswordDlg = class(TForm)
    Panel1: TPanel;
    btnCancel: TcxButton;
    btnOK: TcxButton;
    FramePassword1: TFramePassword;
    Panel2: TPanel;
    cxImage1: TcxImage;
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FramePassword1edPasswordKeyPress(Sender: TObject; var Key: Char);
  private
    FNewPassword: String;
    FOldPassword: String;

  public
    Property NewPassword: String read FNewPassword write FNewPassword;
    property OldPassword: String read FOldPassword write FOldPassword;
  end;

var
  ResetPasswordDlg: TResetPasswordDlg;

implementation

{$R *.dfm}

procedure TResetPasswordDlg.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TResetPasswordDlg.btnOKClick(Sender: TObject);
begin
  ModalResult := mrNone;
  if FramePassword1.Validate(OldPassword, FNewPassword) then
  begin
    ModalResult := mrOK;
  end;
end;



procedure TResetPasswordDlg.FramePassword1edPasswordKeyPress(Sender: TObject;
  var Key: Char);
begin
  btnOK.Enabled := not (FramePassword1.edPassword.text = emptyStr);
  FramePassword1.edPasswordValidationKeyPress(Sender, Key);

end;

end.
