unit uMemoryIniStructure;

interface
uses System.SysUtils, System.Variants, System.Classes;

type
  TIniValues = class;
  TIniSection = class;
  TIniSections = class;
  TIniValue = class (TCollectionItem)
  private
    FValues: TIniValues;
    FKey:    string;
    FValue:  string;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property Key: string read FKey write FKey;
    property Value: string read FValue write FValue;
  end;

  TIniValues = class (TCollection)
  private
    FSection: TIniSection;
    function GetItem(Index: Integer): TIniValue;
    procedure SetItem(Index: Integer; Value: TIniValue);
  public
    destructor Destroy; override;
    function IndexOf (Key: string): integer;
    property Items[Index: Integer]: TIniValue read GetItem write SetItem; default;
    property Section: TIniSection read FSection write FSection;
  end;

  TIniSection = class (TCollectionItem)
  private
    FIniSections: TIniSections;
    FSectionName: string;
    FValues:      TIniValues;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property IniSections: TIniSections read FIniSections;
    property SectionName: string read FSectionName write FSectionName;
    property Values: TIniValues read FValues write FValues;
  end;

  TMemoryIniStructure = class;
  TIniSections = class (TCollection)
  private
    FIniStructure: TMemoryIniStructure;
    function GetItem(Index: Integer): TIniSection;
    procedure SetItem(Index: Integer; Value: TIniSection);
  public
    destructor Destroy; override;
    function IndexOf (Section: string): integer;
    property Items[Index: Integer]: TIniSection read GetItem write SetItem; default;
    property IniStructure: TMemoryIniStructure read FIniStructure write FIniStructure;
  end;

  TMemoryIniStructure = class
  private
    FSections: TIniSections;
    function GetOrCreateSectionKey (Section, Key: string): TIniValue;
    function GetSectionKey (Section, Key: string): TIniValue;
  public
    constructor Create;
    destructor Destroy; override;
    function SaveToStringList: TStringList;
    function LoadFromStringList (Ini: TStringList): boolean;

    procedure WriteString (Section, Ident, Value: string);
    procedure WriteInteger (Section, Ident: string; Value: integer);
    procedure WriteBool (Section, Ident: string; Value: boolean);

    function ReadString (Section, Ident, Default: string): string;
    function ReadInteger (Section, Ident: string; Default: integer): integer;
    function ReadBool (Section, Ident: string; Default: boolean): boolean;

    property Sections: TIniSections read FSections write FSections;
  end;

implementation

{$region 'TIniValue'}
constructor TIniValue.Create(Collection: TCollection);
begin
  inherited Create (Collection);

  FValues := TIniValues (Collection);
  FKey    := '';
  FValue  := '';
end;

destructor TIniValue.Destroy;
begin
  FKey   := '';
  FValue := '';

  inherited Destroy;
end;
{$endregion}

{$region 'TIniValues'}
destructor TIniValues.Destroy;
begin
  inherited Destroy;
end;

function TIniValues.GetItem(Index: Integer): TIniValue;
begin
  Result := TIniValue (inherited GetItem(Index));
end;

procedure TIniValues.SetItem(Index: Integer; Value: TIniValue);
begin
  inherited SetItem(Index, Value);
end;

function TIniValues.IndexOf (Key: string): integer;
var
  i: integer;
begin
  result  := -1;
  Key := UpperCase(Key);
  for i := 0 to Count - 1 do
    if UpperCase (Items [i].Key) = Key then
    begin
      result := i;
      exit;
    end;
end;
{$endregion}

{$region 'TIniSection'}
constructor TIniSection.Create(Collection: TCollection);
begin
  inherited Create (Collection);

  FIniSections     := TIniSections (Collection);
  FSectionName     := '';
  FValues          := TIniValues.Create (TIniValue);
  FValues.FSection := Self;
end;

destructor TIniSection.Destroy;
begin
  FSectionName := '';
  FValues.Free;

  inherited Destroy;
end;
{$endregion}

{$region 'TIniSections'}
destructor TIniSections.Destroy;
begin
  inherited Destroy;
end;

function TIniSections.GetItem(Index: Integer): TIniSection;
begin
  Result := TIniSection (inherited GetItem(Index));
end;

procedure TIniSections.SetItem(Index: Integer; Value: TIniSection);
begin
  inherited SetItem(Index, Value);
end;

function TIniSections.IndexOf (Section: string): integer;
var
  i: integer;
begin
  result  := -1;
  Section := UpperCase(Section);
  for i := 0 to Count - 1 do
    if UpperCase (Items [i].SectionName) = Section then
    begin
      result := i;
      exit;
    end;
end;
{$endregion}

{$region 'TMemoryIniStructure'}
constructor TMemoryIniStructure.Create;
begin
  inherited Create;

  FSections := TIniSections.Create(TIniSection);
end;

destructor TMemoryIniStructure.Destroy;
begin
  FSections.Free;

  inherited Destroy;
end;

function TMemoryIniStructure.SaveToStringList: TStringList;
var
  i, j: integer;
begin
  result := TStringList.Create;

  for i := 0 to Sections.Count - 1 do
  begin
    result.Add('[' + Sections [i].SectionName + ']');

    for j := 0 to Sections [i].Values.Count - 1 do
      result.Add(Format ('%s=%s', [Sections [i].Values.Items [j].Key, Sections [i].Values.Items [j].Value]));

    if i < (Sections.Count - 1) then
      result.Add('');
  end;
end;

function TMemoryIniStructure.LoadFromStringList (Ini: TStringList): boolean;
var
  i, p: integer;
  s: WideString;
  Section: TIniSection;
  Value: TIniValue;
begin
  Sections.Clear;
  Section := nil;
  Value   := nil;

  for i := 0 to Ini.Count - 1 do
  begin
    s := Trim (Ini [i]);

    if s <> '' then
    begin
      if (s [1] = '[') and (s [Length (s)] = ']') then
      begin
        s                   := Trim (Copy (s, 2, Length (s) - 2));
        Section             := TIniSection (Sections.Add);
        Section.SectionName := s;
      end
      else if Assigned (Section) then
      begin
        p := Pos ('=', s);

        if p > 0 then
        begin
          Value       := TIniValue (Section.Values.Add);
          Value.Key   := Trim (Copy (s, 1, p - 1));
          Value.Value := Trim (Copy (s, p + 1, Length (s)));
        end;
      end;
    end;
  end;
end;

function TMemoryIniStructure.GetOrCreateSectionKey (Section, Key: string): TIniValue;
var
  idxSection, IdxIdent: integer;
  TmpSection: TIniSection;
  TmpValue: TIniValue;
begin
  idxSection := Sections.IndexOf(Section);
  if idxSection = -1 then
  begin
    TmpSection             := TIniSection (Sections.Add);
    TmpSection.SectionName := Section;
  end
  else
    TmpSection := Sections.Items [idxSection];

  idxIdent := TmpSection.Values.IndexOf(Key);
  if idxIdent = -1 then
  begin
    result := TIniValue (TmpSection.Values.Add);
    result.Key := Key;
  end
  else
    result := TmpSection.Values.Items [idxIdent];
end;

function TMemoryIniStructure.GetSectionKey (Section, Key: string): TIniValue;
var
  idxSection, IdxIdent: integer;
  TmpSection: TIniSection;
  TmpValue: TIniValue;
begin
  result := nil;

  idxSection := Sections.IndexOf(Section);
  if idxSection = -1 then exit;

  TmpSection := Sections.Items [idxSection];
  idxIdent := TmpSection.Values.IndexOf(Key);

  if idxIdent = -1 then exit;
  result := TmpSection.Values.Items [idxIdent];
end;

procedure TMemoryIniStructure.WriteString (Section, Ident, Value: string);
begin
  GetOrCreateSectionKey (Section, Ident).Value := Value;
end;

procedure TMemoryIniStructure.WriteInteger (Section, Ident: string; Value: integer);
begin
  GetOrCreateSectionKey (Section, Ident).Value := IntToStr (Value);
end;

procedure TMemoryIniStructure.WriteBool (Section, Ident: string; Value: boolean);
begin
  GetOrCreateSectionKey (Section, Ident).Value := IntToStr (Ord (Value));
end;

function TMemoryIniStructure.ReadString (Section, Ident, Default: string): string;
var
  IniValue: TIniValue;
begin
  result   := Default;
  IniValue := GetSectionKey(Section, Ident);

  if Assigned (IniValue) then
    result := IniValue.Value;
end;

function TMemoryIniStructure.ReadInteger (Section, Ident: string; Default: integer): integer;
var
  IniValue: TIniValue;
begin
  result   := Default;
  IniValue := GetSectionKey(Section, Ident);

  if Assigned (IniValue) then
    result := StrToInt (IniValue.Value);
end;

function TMemoryIniStructure.ReadBool (Section, Ident: string; Default: boolean): boolean;
var
  IniValue: TIniValue;
begin
  result   := Default;
  IniValue := GetSectionKey(Section, Ident);

  if Assigned (IniValue) then
    result := IniValue.Value = '1';
end;
{$endregion}

end.
