unit uAddEditUser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, cxLabel,
  {$IFDEF UseSkins}
  dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinBasic, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkroom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinOffice2019Black, dxSkinOffice2019Colorful,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringtime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinTheBezier, dxSkinValentine, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue,
  {$ENDIF}
  Vcl.NumberBox;

type

  TUserAction = (uaAdd, uaResetPassword, uaChangePassword, uaCloneUser);

  TFAddEditUser = class(TForm)
    btnOK: TcxButton;
    btnCancel: TcxButton;
    cxLabel1: TcxLabel;
    edUserName: TcxTextEdit;
    labPreviousPassword: TcxLabel;
    edPreviousPassword: TcxTextEdit;
    labPassword: TcxLabel;
    edPassword: TcxTextEdit;
    cxLabel3: TcxLabel;
    edPasswordValidation: TcxTextEdit;
    labErrorMsg: TcxLabel;
    cxLabel2: TcxLabel;
    nbIdleTime: TNumberBox;
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edPasswordPropertiesChange(Sender: TObject);
    procedure edPreviousPasswordPropertiesChange(Sender: TObject);
  private
    { Private declarations }
    FUserAction: TUserAction;
    procedure SetUserAction(Value: TUserAction);
    procedure UpdateOk;
  public
    { Public declarations }
    PreviousPassword: string;
    OriginalUser: string;
    property UserAction: TUserAction read FUserAction write SetUserAction;
  end;

var
  FAddEditUser: TFAddEditUser;

implementation

uses uMainForm, uSetup;
{$R *.dfm}

procedure TFAddEditUser.UpdateOk;
begin
  btnOK.Enabled := (edUserName.Text <> '') and
    (edPassword.Text = edPasswordValidation.Text) and
    ((FUserAction in [uaAdd, uaCloneUser]) or
    (PreviousPassword = edPreviousPassword.Text));
end;

procedure TFAddEditUser.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFAddEditUser.btnOKClick(Sender: TObject);
begin
  if (FUserAction in [uaAdd, uaCloneUser]) and FSetup.UserExists(edUserName.Text)
  then
  begin
    MessageDlg(Format('User name [%s] already exists', [edUserName.Text]),
      mtWarning, [mbOK], 0);
    exit;
  end;
  ModalResult := mrOk;
end;

procedure TFAddEditUser.edPasswordPropertiesChange(Sender: TObject);
begin
  if not Visible then
    exit;
  labErrorMsg.Visible := edPassword.Text <> edPasswordValidation.Text;
  if labErrorMsg.Visible then
    labErrorMsg.Caption := 'Password does not match';
  UpdateOk;
end;

procedure TFAddEditUser.edPreviousPasswordPropertiesChange(Sender: TObject);
begin
  labErrorMsg.Visible := PreviousPassword <> edPreviousPassword.Text;
  if labErrorMsg.Visible then
    labErrorMsg.Caption := 'Invalid previous password';
  UpdateOk;
end;

procedure TFAddEditUser.FormCreate(Sender: TObject);
begin
  UserAction := uaAdd;
end;

procedure TFAddEditUser.FormShow(Sender: TObject);
begin
  labErrorMsg.Caption := '';
  edPreviousPassword.Text := '';
  edPassword.Text := '';
  edPasswordValidation.Text := '';
  case FUserAction of
    uaAdd, uaCloneUser:
      edUserName.SetFocus;
    uaResetPassword:
      edPassword.SetFocus;
    uaChangePassword:
      edPreviousPassword.SetFocus;
  end;
end;

procedure TFAddEditUser.SetUserAction(Value: TUserAction);
begin
  if FUserAction <> Value then
  begin
    FUserAction := Value;
    edUserName.Text := '';
    PreviousPassword := '';
    labPreviousPassword.Visible := FUserAction = uaChangePassword;
    edPreviousPassword.Visible := FUserAction = uaChangePassword;
    edUserName.Properties.ReadOnly := FUserAction
      in [uaChangePassword, uaResetPassword];
    btnOK.Enabled := False;
    case FUserAction of
      uaAdd:
        begin
          Caption := 'Add user';
          labPassword.Caption := 'Password';
        end;
      uaCloneUser:
        begin
          Caption := 'Clone user ' + OriginalUser;
          labPassword.Caption := 'Password';
        end;
      uaResetPassword:
        begin
          Caption := 'Reset password';
          labPassword.Caption := 'Password';
        end;
      uaChangePassword:
        begin
          Caption := 'Change password';
          labPassword.Caption := 'New password';
        end;
    end;
  end;
end;

end.
