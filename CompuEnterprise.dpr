program CompuEnterprise;

{$I cef.inc}

uses
{$IFDEF DELPHI16_UP}
  Vcl.Forms,
{$ELSE}
  Forms,
  Windows,
{$ENDIF }
  CodeSiteLogging,
  WinApi.Windows,
  uCEFApplication,
  uMainForm in 'uMainForm.pas' {MainForm} ,
  uSetup in 'uSetup.pas' {FSetup} ,
  uLogin in 'uLogin.pas' {FLogin} ,
  uProcesses in 'uProcesses.pas',
  uMemoryIniStructure in 'uMemoryIniStructure.pas',
  uTabParameters in 'uTabParameters.pas' {FTabProperties} ,
  uAddEditUser in 'uAddEditUser.pas' {FAddEditUser} ,
  uFUserLogin in 'uFUserLogin.pas' {FUserLogin} ,
  uTypes in 'uTypes.pas',
  uUserDlg in 'uUserDlg.pas' {UserDlg} ,
  uResetPasswordDlg in 'uResetPasswordDlg.pas' {ResetPasswordDlg} ,
  uPassword in 'uPassword.pas' {FramePassword: TFrame};

{$R *.res}
{ $SetPEFlags IMAGE_FILE_LARGE_ADDRESS_AWARE }

begin
  // GlobalCEFApp creation and initialization moved to a different unit to fix the memory leak described in the bug #89
  // https://github.com/salvadordf/CEF4Delphi/issues/89

  // CodeSite.Enabled := True;
  // CodeSite.Clear;

  CreateGlobalCEFApp;
  GlobalCEFApp.LocalesRequired := 'en-US';
  if GlobalCEFApp.StartMainProcess then
  begin
    Application.Initialize;
{$IFDEF DELPHI11_UP}
    Application.MainFormOnTaskbar := True;
{$ENDIF}
    Application.CreateForm(TMainForm, MainForm);
    Application.CreateForm(TFSetup, FSetup);
    Application.CreateForm(TFLogin, FLogin);
    Application.CreateForm(TFTabProperties, FTabProperties);
    Application.CreateForm(TFAddEditUser, FAddEditUser);
    Application.CreateForm(TFUserLogin, FUserLogin);
    Application.Run;
  end;

  DestroyGlobalCEFApp;

end.
